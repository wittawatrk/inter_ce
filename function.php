<?php
require_once('phpmail/class.phpmailer.php');
date_default_timezone_set("Asia/Bangkok");
class DB_local{
	protected $conn_obj_localhost = NULL;
	public function connect_localhost(){
		try{
			$conn = new PDO("sqlsrv:server=192.168.66.22; Database=SynergyExternal_cloud_2018_new","userreport","PreciseI$");
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
		}catch(PDOException $e){	
			return NULL;
		}
	}
}
class DB_other extends DB_local{
	private function get_source_array($cmp){
		$query_source = "select c.company_db,d.mapping_ip,d.mapping_username,d.mapping_password from company c inner join datasource_mapping d on c.datasource_mapping_id = d.mapping_id where company_code='".$cmp."'";
		try{
			$conn_data = $this->connect_localhost();
			$stmt_query = $conn_data->prepare($query_source); 
			$stmt_query->execute();
			$arr_q=$stmt_query->fetch( PDO::FETCH_ASSOC );
			return $arr_q;
		}catch(PDOException $e){	
			return NULL;
		}
	} 
	public function connect_domain_company($cmp){
		$source=$this->get_source_array($cmp);
		try{
			$conn = new PDO("sqlsrv:server=".$source['mapping_ip']."; Database = ".$source['company_db'], $source['mapping_username'], $source['mapping_password']);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return array($conn,$source['mapping_ip']);
		}catch(PDOException $e){	
			return NULL;
		}
	}
}
class Query_domain extends DB_other{
	private $conn_domain_company = NULL;
	public function __construct($cmpOutside){
		$this->conn_domain_company = $this->connect_domain_company($cmpOutside);
	}
	public function query_table($query_table){
		try{
			if($this->conn_domain_company[1]=="localhost"){$query_table=str_replace("[192.168.66.22].","",$query_table);}
			$conn_data = $this->conn_domain_company[0];	
			$stmt_query=$conn_data->prepare($query_table);
			$stmt_query->execute();
			$array_return = array();
			while($arr_q=$stmt_query->fetch( PDO::FETCH_ASSOC )){
				array_push($array_return, $arr_q);
			}
			return $array_return;
		}catch(PDOException $e){
			return $e->getMessage();
		}
	}
}
class Query_local extends DB_local{
	public $conn_local = NULL;
	public function __construct(){
		$this->conn_local = $this->connect_localhost();
	}
	public function query_table($query_table){
		try{
			$conn_data = $this->conn_local;	
			$stmt_query=$conn_data->prepare($query_table);
			$stmt_query->execute();
			$array_return = array();
			while($arr_q=$stmt_query->fetch( PDO::FETCH_ASSOC )){
				array_push($array_return, $arr_q);
			}
			return $array_return;
		}catch(PDOException $e){
			return $e->getMessage();
		}
	}
	public function query_insert($query,$params){
		try{
			$conn_data = $this->conn_local;	
			$stmt_query=$conn_data->prepare($query);
			$stmt_query->execute($params);
			$array_return = array();
			while($arr_q=$stmt_query->fetch( PDO::FETCH_ASSOC )){
				array_push($array_return, $arr_q);
			}
			return $array_return;
		}catch(PDOException $e){
			return $e->getMessage();
		}
	}
}

class general_function{
	public function remove_comma($int_or_str){
		return str_replace(",","",$int_or_str);
	}
	public function get_menu($active_page){
		$index_active = ($active_page=='index.php')?'style="background-color:#eee;"':'';
		$other_report_ar_balance_list = ($active_page=='other_report_ar_balance_list.php')?'class="active"':'';
		$other_report_cmp_in_rev = ($active_page=='other_report_cmp_in_rev.php')?'class="active"':'';
		$other_report_cmp_with_rev = ($active_page=='other_report_cmp_with_rev.php')?'class="active"':'';
		$other_report_rev_with_rev = ($active_page=='other_report_rev_with_rev.php')?'class="active"':'';
		$other_report_apd_pem_pem1 = ($active_page=='other_report_apd_pem_pem1.php')?'class="active"':'';
		$view_revenue_compare_for_pc = ($active_page=='view_report.php')?'class="active"':'';
		$View_Compare_Core_Company = ($active_page=='other_report_get_by_revenue.php')?'class="active"':'';
		$setting_bs = ($active_page=='setting_bs.php')?'class="active"':'';
		$setting_gl = ($active_page=='setting_gl.php')?'class="active"':'';
		$setting_customer = ($active_page=='setting_customer.php')?'class="active"':'';
		$upload_edit = ($active_page=='upload_edit.php')?'class="active"':'';
		$upload_erp = ($active_page=='upload_erp.php')?'class="active"':'';
		$summary_by_gl=($active_page=='summary_by_gl_code.php')?'class="active"':'';
		$str_html = '<ul class="nav navbar-nav">
		<li '.$index_active.'>
			<a href="index.php">View</a>
		</li>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Bootstrap Related Resources" aria-expanded="false">Upload <b class="caret"></b></a>
			<ul class="dropdown-menu dropdown-menu-right">
                        <li '.$upload_erp.'>
                            <a href="upload_erp.php"><i class="icon-list"></i> Upload From ERP</a>
                        </li>
                        <li '.$upload_edit.'>
                            <a href="upload_edit.php"><i class="icon-list"></i> Editable</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown"  >
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Bootstrap Related Resources" aria-expanded="false">Others Report <b class="caret"></b></a>
				<ul class="dropdown-menu dropdown-menu-right">
			 
			  			  <li '.$view_revenue_compare_for_pc.'>
						<a href="view_report.php"><i class="icon-list"></i>Consolidate Report for PC</a>
				</li>	
						<li '.$View_Compare_Core_Company.'>		
						<a href="other_report_get_by_revenue.php"><i class="icon-list"></i>Consolidate Report for RS</a>
						</li>
						<li '.$summary_by_gl.'>
						<a href="summary_by_gl_code.php"><i class="icon-list"></i>Summary by GL </a>
						</li>
                </ul>
                </li>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Bootstrap Related Resources" aria-expanded="false">Settings <b class="caret"></b></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li '.$setting_gl.'>
                            <a href="setting_gl.php"><i class="icon-list"></i> Setting GL</a>
                        </li>
                        <li '.$setting_customer.'>
                            <a href="setting_customer.php"><i class="icon-list"></i> Setting Customer/Supplier</a>
                        </li>
                        <li '.$setting_bs.'>
                            <a href="setting_bs.php"><i class="icon-list"></i> Setting Starting Balance</a>
                        </li>
                    </ul>
                </li>
				</ul>';
				return $str_html;
	}
	
}
class get_date_class{
	public function get_date_today(){
		$arr = getdate(); 
		return $arr['year']."-".str_pad($arr['mon'],2,"0",STR_PAD_LEFT)."-".str_pad($arr['mday'],2,"0",STR_PAD_LEFT)." ".str_pad($arr['hours'],2,"0",STR_PAD_LEFT).":".str_pad($arr['minutes'],2,"0",STR_PAD_LEFT).":".str_pad($arr['seconds'],2,"0",STR_PAD_LEFT);
	}
	public function change_date_from_db_to_show_date($datetime){
		if($datetime!=""){
		return date("d/m/Y",strtotime($datetime));
		}
		else{return "";}
	}
	public function change_date_from_db_to_show_datetime($datetime){
		if($datetime!=""){
		return date("d/m/Y h:i:s",strtotime($datetime));
		}
		else{return "";}
	}
}
/*
class get_td_left_right{
	private $conn_local = NULL;
	private $year = NULL;
	private $month = NULL;
	private $com_id_left = NULL;
	private $com_id_right = NULL;
	private $gl_id = NULL;
	public function __construct($conn_local,$month,$year){ //,$cmp_id_left,$cmp_id_right,$gl_id
		$this->year = $year; 
		$this->month = $month;
		$this->com_id_left = $cmp_id_left;
		$this->com_id_right = $cmp_id_right;
		$this->gl_id = $gl_id;

		//$this->conn_local = $this->connect_localhost();
	}
}*/


$class_date = new get_date_class();
$class_general = new general_function();

////////////////////

?>