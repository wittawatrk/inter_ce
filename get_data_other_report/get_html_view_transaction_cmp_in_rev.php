<?php
require_once '../function.php';
  $month_start = $_POST['month_start'];
  $month_end = $_POST['month_end'];
  $year = $_POST['year'];
	$date_start = $year."-".str_pad($month_start,2,"0",STR_PAD_LEFT)."-01";
	$date_last =  $year."-".str_pad($month_end,2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($year."-".$month_end."-01")),2,"0",STR_PAD_LEFT);
  $gl_id = $_POST['gl_id'];

function get_query($year,$month_start,$month_end,$gl_id,$head_com_id,$this_com_id)  {
	return "  select isl.atid,iz.*
    from ie_summary_lastest isl
    inner join ie_zoom iz on isl.atid = iz.ie_summary_id
    where isl.company_id_user='".$head_com_id."' 
    and isl.company_id_mapping='".$this_com_id."' 
    and isl.ie_grouping_gl_id='".$gl_id."'
    and isl.year = '".$year."'
    and isl.month between '".$month_start."' and '".$month_end."' ";
}
$class_q_local = new Query_local();
$str_q = get_query($year,$month_start,$month_end,$gl_id,$_POST['head_com'],$_POST['this_com']);
$arr_q = $class_q_local->query_table($str_q);
if(is_array($arr_q)&&sizeof($arr_q)>0){
?>
	<table style="font-size:12px;" border="1" id="table_trans_inner_view_by_com"  width="100%">
	<thead style="background-color:#fafafa;border-bottom:2px solid;">
		<tr>
			<td>glcode</td>
      <td>entry_no</td>
      <td>ourref</td>
      <td>yourref</td>
      <td>des</td>
      <td>itemcode</td>
      <td>item_des</td>
      <td>Amount</td>
      <td>debnr</td>
      <td>deb_name</td>
      <td>crdnr</td>
      <td>crd_name</td>
      <td>so</td>
		</tr>
	</thead>
	<tbody>
	<?php
  $i=0; $sum_amount = 0;
	while($i<sizeof($arr_q)){
    $arr_in_trans = $arr_q[$i];
    echo '<tr>';
    echo '<td>'.$arr_in_trans["glcode"].'</td>';
    echo '<td>'.$arr_in_trans["entry_no"].'</td>';
    echo '<td>'.$arr_in_trans["ourref"].'</td>';
    echo '<td>'.$arr_in_trans["yourref"].'</td>';
    echo '<td>'.$arr_in_trans["des"].'</td>';
    echo '<td>'.$arr_in_trans["itemcode"].'</td>';
    echo '<td>'.$arr_in_trans["item_des"].'</td>';
      if(($arr_in_trans["qty"]!=NULL&&$arr_in_trans["qty"]!="")){
        echo '<td>'.number_format($arr_in_trans["qty"],2).'</td>';
      }else{
        echo '<td></td>';
      }
    echo '<td>'.$arr_in_trans["debnr"].'</td>';
    echo '<td>'.$arr_in_trans["deb_name"].'</td>';
    echo '<td>'.$arr_in_trans["crdnr"].'</td>';
    echo '<td>'.$arr_in_trans["crd_name"].'</td>';
    echo '<td>'.$arr_in_trans["so"].'</td>';
    echo '</tr>';
    $sum_amount+=$arr_in_trans["qty"];
    $i++;
  }
  echo '<tr><td colspan="13">Sum : '.number_format($sum_amount,2).'</td></tr>';
	?>
	</tbody>
	</table>
<?php
}else{echo "ไม่มีข้อมูล";}
?>
<br>
<?php
	//echo $str_q;
?>
<style type="text/css">
#table_trans_inner_view_by_com{
    border:1px grey solid;
}
#table_trans_inner_view_by_com thead td{
    padding: 12px;
    background-color: #F5F5F5;
    /*font-weight: bold;*/
}
#table_trans_inner_view_by_com tbody td{
    padding: 8px;
    cursor:pointer;
    border-right:1px grey solid;
}
#table_trans_inner_view_by_com tfoot td{
    padding: 8px;
    border-right:1px grey solid;
}
</style>