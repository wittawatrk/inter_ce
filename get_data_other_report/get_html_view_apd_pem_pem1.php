<?php
require_once '../function.php';
//$_POST['user_company_code']="CI";
//$_POST['user_company_id']="3";
//$_POST['year']="2016";
//$_POST['month']="1";
$date_start = $_POST['year']."-".str_pad($_POST['month_start'],2,"0",STR_PAD_LEFT)."-01";
$date_last =  $_POST['year']."-".str_pad($_POST['month_end'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($_POST['year']."-".$_POST['month_end']."-01")),2,"0",STR_PAD_LEFT);
$year = $_POST['year'];
$month_start = $_POST['month_start'];
$month_end = $_POST['month_end'];
$text_combine_cmp = 'PEM+PEM1';
$cmp_id_first = "1";
$cmp_id_second = "10";

function get_q_value($type_gl,$gl_id,$row_com_id,$col_com_id,$year,$month_start,$month_end,$date_start,$date_last){

    if($row_com_id=="1"||$row_com_id=="10"){$str_com_user = " ( isl.company_id_user=1 or isl.company_id_user=10 ) ";}
    else{$str_com_user = " isl.company_id_user='".$row_com_id."' ";}

    if($col_com_id=="1"||$col_com_id=="10"){$str_com_map = " and ( isl.company_id_mapping=1 or isl.company_id_mapping=10 ) ";}
    else{$str_com_map = " and isl.company_id_mapping='".$col_com_id."' ";}

    if($type_gl=="B/S"){  /// B/S ไม่ SUM ช่วงให้ เพราะเป็นยอดปิด ไม่งั้นจะผิด
        $str_return_q = "select (isl.amount) as amount,isl.type_edit,isl.created_date
  from ie_summary_lastest  isl
  where ".$str_com_user." ".$str_com_map." and isl.ie_grouping_gl_id='".$gl_id."'
  and (isl.year='".$year."' and isl.month='".$month_end."')";
    }else{ /// P/L จะ SUM ช่วงให้
        $str_return_q = "select SUM(isl.amount) as amount
        from ie_summary_lastest isl
        where
        ".$str_com_user."
        ".$str_com_map."
        and isl.ie_grouping_gl_id='".$gl_id."'
        and isl.year = ".$year."
        and isl.month between ".$month_start." and ".$month_end." ";
        /*$str_return_q = "select
    ISNULL(SUM(CASE WHEN DateSearch>='".$date_start."' AND DateSearch<= '".$date_last."' THEN amount ELSE 0 END),0) as amount
    FROM ( select (CAST(LTRIM(RTRIM(year)) AS VARCHAR(4)))+'-'+RIGHT('0'+CAST(LTRIM(RTRIM(month)) AS VARCHAR(2)),2)+'-01' as DateSearch,amount from ie_summary_lastest where company_id_user='".$row_com_id."' and company_id_mapping='".$col_com_id."' and ie_grouping_gl_id='".$gl_id."' ) tb ";
        */
    }
    return $str_return_q;
}


$class_q_local = new Query_local();
$q_company = "select * from company where revenue_id='1' ";
$arr_company=$class_q_local->query_table($q_company);
$num_company = sizeof($arr_company); $num_company_row = sizeof($arr_company)-1; $num_company_col = sizeof($arr_company)-1;
if($num_company==0){
    echo "ไม่มีข้อมูล Revenue นี้"; exit();
}
$width_cal = floor((85/(($num_company)+2)));

echo '<table border="1" id="table_show_compare" style="width:100%;border-collapse: collapse;">';

$count_in_gl_loop = 0;
$q_gl = "select * from ie_grouping_gl   order by group_no asc,orderg asc";
$arr_gl = $class_q_local->query_table($q_gl);
$i=0; $sum_vertical = array();
while($i<sizeof($arr_gl)){
    $data_gl = $arr_gl[$i];  $sum_combine_cmp_all = NULL;
    if($count_in_gl_loop==0){
        echo '<tr class="GridviewScrollHeader">';
            echo '<td width="15%">'.$data_gl["name"].' ('.$data_gl["type_show"].')</td>';
            $j=0;
            while($j<$num_company){
                if($arr_company[$j]["company_code"]=="PEM"){ echo '<td width="'.$width_cal.'%">PEM+PEM1</td>';}
                else if($arr_company[$j]["company_code"]=="PEM1"){}
                else{echo '<td width="'.$width_cal.'%">'.$arr_company[$j]["company_code"].'</td>';}
                $j++;
            }

            echo '<td width="'.$width_cal.'%">Total</td>';

        echo '</tr>';
        $count_in_gl_loop++;
    }
    $j=0;
    while($j<$num_company_row){ /// วน row cmp
        echo '<tr style="font-size:90%;">';
            if($arr_company[$j]["company_code"]=="PEM"){
                echo '<td>PEM+PEM1</td>';
            }else{
                echo '<td>'.$arr_company[$j]["company_code"].'</td>';
            }

            $k=0; $sum_horizonal = NULL; $sum_combine_cmp = NULL;
            while($k<$num_company_col){
                $row_cmp_code = $arr_company[$j]["company_code"];   $row_cmp_id = $arr_company[$j]["company_id"];
                $col_cmp_code = $arr_company[$k]["company_code"];   $col_cmp_id = $arr_company[$k]["company_id"]; $value_amount=NULL;
                if(!isset($sum_vertical[$k])){$sum_vertical[$k]=NULL;}


                if(($row_cmp_id=="1"&&$col_cmp_id=="1")||$row_cmp_id!=$col_cmp_id)
                {
                    $str_q_value = get_q_value($data_gl["type"],$data_gl["atid"],$row_cmp_id,$col_cmp_id,$year,$month_start,$month_end,$date_start,$date_last);
                    $arr_value_amount = $class_q_local->query_table($str_q_value);

                    if(isset($arr_value_amount[0]["amount"])){
                        $sum_horizonal+=$arr_value_amount[0]["amount"];
                        if(($col_cmp_id==$cmp_id_first)||($col_cmp_id==$cmp_id_second)){$sum_combine_cmp+=$arr_value_amount[0]["amount"];}
                        $value_amount = $arr_value_amount[0]["amount"];
                        $sum_vertical[$k]=$sum_vertical[$k]+$value_amount;
                    }
                    else{
                        $value_amount = NULL;
                    }

                    if($col_cmp_id!="10"){
                        echo '<td title="'.$str_q_value.'">';
                            echo '<span class="span_amount" head-com="'.$row_cmp_id.'" head-com-code="'.$row_cmp_code.'" gl-id="'.$data_gl["atid"].'" this-com="'.$col_cmp_id.'" this-com-code="'.$col_cmp_code.'" deb-crd-type="'.$data_gl["type"].'">';
                                if($value_amount!=NULL){
                                    echo number_format($value_amount,2);
                                }
                            echo '</span>';
                        echo '</td>';
                    }
                }else{
                    echo '<td  style="background-color:#CCCCCC;"></td>';
                }
                $k++;
            }

        echo '<td>'; echo ($sum_horizonal!=NULL)?number_format($sum_horizonal,2):""; echo '</td>';
        //echo '<td>'; echo ($sum_combine_cmp!=NULL)?number_format($sum_combine_cmp,2):"";  $sum_combine_cmp_all+=$sum_combine_cmp; echo '</td>';
        echo '</tr>';
        $j++;
    }
    if($j==$num_company_row){
        $count_in_gl_loop=0;
        echo '<tr style="font-size:90%;background-color:#F5F5F5;">';
        echo '<td>Total : </td>';
        $k=0; $sum_horizonal_of_vertical = NULL;
        while($k<$num_company_col){
            echo '<td>';
                if(isset($sum_vertical[$k])){
                    echo number_format($sum_vertical[$k],2); $sum_horizonal_of_vertical+=$sum_vertical[$k];
                }else{
                    echo "";
                }
            echo '</td>';
            $k++;
        }
        echo '<td>'; echo ($sum_horizonal_of_vertical!=NULL)?number_format($sum_horizonal_of_vertical,2):""; echo '</td>';
        //echo '<td>';  echo ($sum_combine_cmp_all!=NULL)?number_format($sum_combine_cmp_all,2):""; '</td>';
        echo '</tr>';
        $sum_vertical = array();
    }

    $i++; ///////// loop GL
}

echo '</table>';
?>
<script type="text/javascript">
blink('span[status=blink]');
function blink(selector){
    $(selector).css("background-color","#FFE4E1");
}
</script>
<style type="text/css">
#table_show_compare td{
 text-align: center;
}
.td_total{
	font-size: 80%;
}
.td_to_compare_trans,.td_total,.sum_by_gl_left,.sum_by_gl_right,.sum_by_gl_diff{
	font-size: 90%;
	word-wrap: break-word;
    overflow-wrap: break-word;
}
.td_to_compare_trans_blink{
	font-size: 90%;
	background-color: #FFE4E1;
	word-wrap: break-word;
    overflow-wrap: break-word;
}
.GridviewScrollHeader TH, .GridviewScrollHeader TD
{
    padding: 5px;
    font-weight: bold;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #EFEFEF;
    text-align: center;
    vertical-align: middle;
}
.GridviewScrollItem TD
{
    padding: 5px;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #FFFFFF;
    font-size: 90%;
}
</style>
