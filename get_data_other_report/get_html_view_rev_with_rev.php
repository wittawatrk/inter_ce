<?php
require_once '../function.php';
//$_POST['user_company_code']="CI";
//$_POST['user_company_id']="3";
//$_POST['year']="2016";
//$_POST['month']="1";
$date_start = $_POST['year']."-".str_pad($_POST['month_start'],2,"0",STR_PAD_LEFT)."-01";
$date_last =  $_POST['year']."-".str_pad($_POST['month_end'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($_POST['year']."-".$_POST['month_end']."-01")),2,"0",STR_PAD_LEFT);
$year = $_POST['year'];
$month_start = $_POST['month_start'];
$month_end = $_POST['month_end'];

function get_q_value($type_gl,$gl_id,$row_rev_id,$col_rev_id,$year,$month_start,$month_end,$date_start,$date_last){
    if($type_gl=="B/S"){  /// B/S ไม่ SUM ช่วงให้ เพราะเป็นยอดปิด ไม่งั้นจะผิด
        $str_return_q = "select (isl.amount) as amount,isl.type_edit,isl.created_date
    from ie_summary_lastest  isl
    left join company cmp1 on isl.company_id_user = cmp1.company_id
    left join revenue rev1 on cmp1.revenue_id = rev1.revenue_id
    left join company cmp2 on isl.company_id_mapping = cmp2.company_id
    left join revenue rev2 on cmp2.revenue_id = rev2.revenue_id
    where rev1.revenue_id = '".$row_rev_id."' and rev2.revenue_id = '".$col_rev_id."' and isl.ie_grouping_gl_id='".$gl_id."'
    and (isl.year='".$year."' and isl.month='".$month_end."')";
    }else{ /// P/L จะ SUM ช่วงให้
        $str_return_q = "select SUM(isl.amount) as amount
        from ie_summary_lastest isl
        left join company cmp1 on isl.company_id_user = cmp1.company_id
        left join revenue rev1 on cmp1.revenue_id = rev1.revenue_id
        left join company cmp2 on isl.company_id_mapping = cmp2.company_id
        left join revenue rev2 on cmp2.revenue_id = rev2.revenue_id
        where
        rev1.revenue_id = '".$row_rev_id."'
        and rev2.revenue_id = '".$col_rev_id."'
        and isl.ie_grouping_gl_id='".$gl_id."'
        and isl.year = ".$year."
        and isl.month between ".$month_start." and ".$month_end." ";
        /*$str_return_q = "select
    ISNULL(SUM(CASE WHEN DateSearch>='".$date_start."' AND DateSearch<= '".$date_last."' THEN amount ELSE 0 END),0) as amount
    FROM ( select year+'-'+RIGHT('0'+CAST(LTRIM(RTRIM(month)) AS VARCHAR(2)),2)+'-01' as DateSearch,amount from ie_summary_lastest where company_id_user='".$row_com_id."' and company_id_mapping='".$col_com_id."' and ie_grouping_gl_id='".$gl_id."' ) tb ";
        */
    }
    return $str_return_q;
}
function sum_value($arr_q){
    $sum_value  = 0;
    foreach ($arr_q as $key => $value) {
        $sum_value+=$value['amount'];
    }
    return $sum_value;
}


$class_q_local = new Query_local();
$q_revenue = "select * from revenue ";
$arr_revenue =$class_q_local->query_table($q_revenue);
$num_revenue = sizeof($arr_revenue);
if($num_revenue==0){
    echo "ไม่มีข้อมูล"; exit();
}
$width_cal = floor((85/(($num_revenue)+1)));

echo '<table border="1" id="table_show_compare" style="width:100%;border-collapse: collapse;">';

$count_in_gl_loop = 0;
$q_gl = "select * from ie_grouping_gl   order by group_no asc,orderg asc";
$arr_gl = $class_q_local->query_table($q_gl);
$i=0; $sum_vertical = array();
while($i<sizeof($arr_gl)){
    $data_gl = $arr_gl[$i];
    if($count_in_gl_loop==0){
        echo '<tr class="GridviewScrollHeader">';
            echo '<td>'.$data_gl["name"].' ('.$data_gl["type_show"].')</td>';
            $j=0;
            while($j<$num_revenue){
                echo '<td>'.$arr_revenue[$j]["revenue_name"].'</td>';
                $j++;
            }
            echo '<td>Total</td>';
        echo '</tr>';
        $count_in_gl_loop++;
    }
    $j=0;
    while($j<$num_revenue){ /// วน row revenue
        echo '<tr class="GridviewScrollItem">';
            echo '<td>'.$arr_revenue[$j]["revenue_name"].'</td>';
            $k=0; $sum_horizonal = NULL;
            while($k<$num_revenue){
                $row_rev_code = $arr_revenue[$j]["revenue_name"];   $row_rev_id = $arr_revenue[$j]["revenue_id"];
                $col_rev_code = $arr_revenue[$k]["revenue_name"];   $col_rev_id = $arr_revenue[$k]["revenue_id"]; $value_amount=NULL;
                if(!isset($sum_vertical[$k])){$sum_vertical[$k]=NULL;}
                if($row_rev_id==$col_rev_id){
                    echo '<td style="background-color:#CCCCCC;"></td>';
                }else{
                    $str_q_value = get_q_value($data_gl["type"],$data_gl["atid"],$row_rev_id,$col_rev_id,$year,$month_start,$month_end,$date_start,$date_last);
                    $arr_value_amount = $class_q_local->query_table($str_q_value);


                    if(isset($arr_value_amount[0])){
                        $loop_value = sum_value($arr_value_amount);
                        $sum_horizonal+=$loop_value; $value_amount = $loop_value;
                        $sum_vertical[$k]=$sum_vertical[$k]+$value_amount;
                    }
                    else{
                        $value_amount = NULL;
                    }

                    echo '<td>';
                        echo '<span class="span_amount" head-rev="'.$row_rev_id.'" head-rev-code="'.$row_rev_code.'" gl-id="'.$data_gl["atid"].'" this-rev="'.$col_rev_id.'" this-rev-code="'.$col_rev_code.'" deb-crd-type="'.$data_gl["type"].'">';
                            if($value_amount!=NULL){
                                echo number_format($value_amount,2);
                            }
                        echo '</span>';
                    echo '</td>';
                }
                $k++;
            }
        echo '<td>'; echo ($sum_horizonal!=NULL)?number_format($sum_horizonal,2):""; echo '</td>';
        echo '</tr>';
        $j++;
    }
    if($j==$num_revenue){
        $count_in_gl_loop=0;
        echo '<tr style="font-size:90%;background-color:#F5F5F5;">';
        echo '<td>Total : </td>';
        $k=0; $sum_horizonal_of_vertical = NULL;
        while($k<$num_revenue){
            echo '<td>';
                if(isset($sum_vertical[$k])){
                    echo number_format($sum_vertical[$k],2); $sum_horizonal_of_vertical+=$sum_vertical[$k];
                }else{
                    echo "";
                }
            echo '</td>';
            $k++;
        }
        echo '<td>'; echo ($sum_horizonal_of_vertical!=NULL)?number_format($sum_horizonal_of_vertical,2):""; echo '</td>';
        echo '</tr>';
        $sum_vertical = array();
    }

    $i++; ///////// loop GL
}

echo '</table>';
?>
<script type="text/javascript">
blink('span[status=blink]');
function blink(selector){
    $(selector).css("background-color","#FFE4E1");
}
</script>
<style type="text/css">
#table_show_compare td{
 text-align: center;
}
.td_total{
    font-size: 80%;
}
.td_to_compare_trans,.td_total,.sum_by_gl_left,.sum_by_gl_right,.sum_by_gl_diff{
    font-size: 90%;
    word-wrap: break-word;
    overflow-wrap: break-word;
}
.td_to_compare_trans_blink{
    font-size: 90%;
    background-color: #FFE4E1;
    word-wrap: break-word;
    overflow-wrap: break-word;
}
.GridviewScrollHeader TH, .GridviewScrollHeader TD
{
    padding: 5px;
    font-weight: bold;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #EFEFEF;
    text-align: center;
    vertical-align: middle;
}
.GridviewScrollItem TD
{
    padding: 5px;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #FFFFFF;
    font-size: 90%;
}
</style>
