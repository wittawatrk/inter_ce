<?php
require_once '../function.php';

if($_POST['check_type_filter']=="2"){ // เป็นช่วง
	$date_start = $_POST['year']."-".str_pad($_POST['month_start'],2,"0",STR_PAD_LEFT)."-01";
	$date_last =  $_POST['year']."-".str_pad($_POST['month_end'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($_POST['year']."-".$_POST['month_end']."-01")),2,"0",STR_PAD_LEFT);

}else{
	$date_start = $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-01";
	$date_last =  $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($_POST['year']."-".$_POST['month']."-01")),2,"0",STR_PAD_LEFT);
}

function query_debtor($date_start,$date_last,$gl_id,$head_com_id,$this_com_id)  {
	return "select iedc.debnr as cmp_code_exact,igm.gl_code,ci.cmp_name AS OffSetName, (CASE WHEN bt.Type = 'S' THEN bt.ValueDate ELSE bt.InvoiceDate END) AS InvDate, debcode AS Relation, bt.InvoiceNumber, bt.SupplierInvoiceNumber, (CASE WHEN bt.Type = 'W' THEN bt.AmountDC ELSE -bt.AmountDC END ) AS AmountDC, bt.Description, bt.EntryNumber, (CASE WHEN bt.Type = 'W' THEN bt.AmountTC ELSE -bt.AmountTC END ) AS AmountTC, (SELECT SUM(X.AmountTC) FROM BankTransactions X 
  WHERE X.Type = 'W' AND X.Status IN ('C','A','P','J') 
    AND X.InvoiceNumber = bt.InvoiceNumber 
    AND X.DebtorNumber = bt.DebtorNumber 
    AND X.TransactionType NOT IN ('Y','Z','E') 
    AND X.EntryNumber = bt.EntryNumber ) AS InvoiceAmt, (CASE bt.TransactionType WHEN 'N' THEN 'Other'
 WHEN 'K' THEN 'Sales invoice'
 WHEN 'C' THEN 'Sales credit note'
 WHEN 'T' THEN 'Purchase invoice'
 WHEN 'Q' THEN 'Purchase credit note'
 WHEN 'Z' THEN 'Cash receipt'
 WHEN 'Y' THEN 'Payment'
 WHEN 'R' THEN 'Refund'
 WHEN 'P' THEN 'Interbank'
 WHEN 'S' THEN 'Reversal credit note'
 WHEN 'D' THEN 'Debit memo/Financial charge'
 WHEN 'F' THEN 'Discount'
 WHEN 'U' THEN 'Credit surcharge'
 WHEN 'M' THEN 'Machine hours'
 WHEN 'L' THEN 'Labour hours'
 WHEN 'E' THEN 'Revaluation'
 WHEN 'I' THEN 'Disposal'
 WHEN 'V' THEN 'Depreciation'
 WHEN 'A' THEN 'Receipt'
 WHEN 'B' THEN 'Fulfilment'
 WHEN 'G' THEN 'Counts'
 WHEN 'H' THEN 'Return fulfilment'
 WHEN 'J' THEN 'Return receipt'
 WHEN 'W' THEN 'Payroll'
 WHEN 'O' THEN 'POS Sales invoice'
 WHEN 'X' THEN 'Settled'
 ELSE  bt.TransactionType END) AS TransactionType, bt.OffsetLedgerAccountNumber, (DATEDIFF(dd, ISNULL(bt.DueDate,bt.ValueDate), {d '".$date_last."'})) AS iAge, bt.OrderNumber, DebtorNumber AS OffsetNumber, bt.DueDate AS DueDate, (CASE WHEN bt.AmountDC > 0 AND bt.Type = 'W' THEN bt.AmountDC ELSE 
      (CASE WHEN bt.Type = 'S' AND bt.AmountDC < 0 THEN -bt.AmountDC ELSE NULL END ) 
 END ) AS Debit, (CASE WHEN bt.AmountDC < 0 AND bt.Type = 'W' THEN -bt.AmountDC ELSE 
      (CASE WHEN bt.Type = 'S' AND bt.AmountDC > 0 THEN bt.AmountDC ELSE NULL END ) 
 END ) AS Credit, ROUND((CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) < 0 AND bt.Type = 'W'       THEN bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) < 0 AND bt.Type = 'S'       THEN -bt.AmountDC ELSE NULL END ) END ) , 2) AS NotDue, bt.TCCode, bt.ID, addr.AddressLine1 AS Adres, addr.Postcode AS Postcode, addr.City AS City, addr.Phone AS Tel, addr.Fax AS Fax, cp.cnt_l_name AS Contact, ci.vatnumber AS VatNumber, ci.cmp_fctry AS Country, (CASE WHEN bt.DocumentID IS NULL THEN 0 ELSE 1 END) AS DocumentID, ci.creditline AS Creditline, bt.ExchangeRate, bt.Blocked AS Blocked, bt.OffsetReference, (CASE bt.Type 
 WHEN 'S' THEN 'Cash flow'
 WHEN 'W' THEN 'Terms'
 END) AS Type, (CASE ci.cmp_status WHEN 'A' THEN 'Active'
 WHEN 'B' THEN 'Blocked'
 WHEN 'E' THEN 'Inactive'
 WHEN 'N' THEN 'Not validated'
 WHEN 'P' THEN 'Pilot'
 WHEN 'R' THEN 'Reference'
 WHEN 'S' THEN 'Passive'
 ELSE  ci.cmp_status END) AS RSTATUS, ROUND(
(CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) BETWEEN 0 AND 30 AND bt.Type = 'W'       THEN bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) BETWEEN 0 AND 30 AND bt.Type = 'S'       THEN -bt.AmountDC ELSE NULL END ) END ) , 2) AS T1, ROUND(
(CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) BETWEEN 31 AND 60 AND bt.Type = 'W'       THEN bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) BETWEEN 31 AND 60 AND bt.Type = 'S'       THEN -bt.AmountDC ELSE NULL END ) END ) , 2) AS T2, ROUND(
(CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) BETWEEN 61 AND 90 AND bt.Type = 'W'       THEN bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) BETWEEN 61 AND 90 AND bt.Type = 'S'       THEN -bt.AmountDC ELSE NULL END ) END ) , 2) AS T3, ROUND(
(CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) > 90 AND bt.Type = 'W'       THEN bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) > 90 AND bt.Type = 'S'       THEN -bt.AmountDC ELSE NULL END ) END ) , 2) AS T4, cc.ClassificationID AS Classification, (CASE WHEN bt.MatchID IS NOT NULL THEN 'Matched' ELSE  (CASE WHEN ISNULL (bt.Status, '') = 'C' AND bt.Approved IS NULL THEN 'Entered' WHEN ISNULL (bt.Status, '') = 'C' AND bt.Approved IS NOT NULL THEN 'Authorised: 1' WHEN ISNULL (bt.Status, '') = 'A' AND bt.Approved IS NOT NULL THEN 'Authorised' WHEN ISNULL (bt.Status, '') = 'A' AND bt.Approved2 IS NOT NULL THEN 'Authorised: 2' WHEN ISNULL (bt.Status, '') = 'P' THEN 'Processed' WHEN ISNULL (bt.Status, '') = 'D' THEN 'Deposit' WHEN ISNULL (bt.Status, '') IN ('U','J') THEN 'Unallocated' ELSE ' ' END) END) AS TSTATUS, (CASE ci.cmp_type WHEN 'A' THEN 'Associate' WHEN 'B' THEN 'Bank' WHEN 'C' THEN 'Customer'
 WHEN 'D' THEN 'Division' WHEN 'E' THEN 'Employee' WHEN 'N' THEN 'Not validated'
 WHEN 'P' THEN 'Prospect' WHEN 'R' THEN 'Reseller' WHEN 'L' THEN 'Lead'
 WHEN 'S' THEN 'Supplier'
 WHEN 'T' THEN 'Suspect' ELSE  ci.cmp_type END) AS RTYPE, (CASE bt.PaymentType WHEN 'A' THEN 'Automatic collection'
 WHEN 'B' THEN 'On credit'
 WHEN 'C' THEN 'Cheque'
 WHEN 'D' THEN 'Post dated cheque'
 WHEN 'E' THEN 'EFT'
 WHEN 'F' THEN 'Factoring'
 WHEN 'H' THEN 'Chipknip'
 WHEN 'I' THEN 'Collection'
 WHEN 'K' THEN 'Cash'
 WHEN 'L' THEN 'Factoring: Letter of credit'
 WHEN 'O' THEN 'Debt collection'
 WHEN 'P' THEN 'Payment on delivery'
 WHEN 'Q' THEN 'Confirming: Cheque'
 WHEN 'R' THEN 'Credit card'
 WHEN 'S' THEN 'To be settled'
 WHEN 'W' THEN 'Letter of credit'
 WHEN 'M' THEN 'Not accepted letter of credit'
 WHEN 'N' THEN 'Promissory note'
 WHEN 'T' THEN 'Factoring: Collection'
 WHEN 'U' THEN 'Confirming: On credit'
 WHEN 'V' THEN 'ESR payments'
 WHEN 'Y' THEN 'Payments in FC'
 WHEN 'X' THEN 'Payments in CHF'
 WHEN 'Z' THEN 'Payments abroad'
 ELSE  bt.PaymentType END) AS PaymentType FROM (( 
      
       SELECT '' AS Empty, bt.ID, DebtorNumber, CreditorNumber, ValueDate, AmountDC, AmountTC, 
               ProcessingDate, InvoiceDate, ReportingDate, Type, OffSetName, PaymentType, SupplierInvoiceNumber, 
               CAST(Description AS VARCHAR(400)) AS Description, TransactionType, OffsetReference, 
               OffSetLedgerAccountNumber, bt.Blocked, DocumentID, OrderNumber, InvoiceNumber, 
               DueDate, TcCode, bt.Status, bt.MatchID, BatchNumber, OwnBankAccount, EntryNumber, bt.LedgerAccount, 
 
               bt.sysguid, (CASE WHEN ExchangeRate = 0 THEN CONVERT(VARCHAR(20),ROUND(ExchangeRate,6)) ELSE (CASE WHEN 1/ExchangeRate > 10000 THEN '1/' + CONVERT(VARCHAR(20), ROUND(1/ExchangeRate,6)) ELSE CONVERT(VARCHAR(20), ROUND(ExchangeRate,6)) END) END) AS ExchangeRate,
               bt.Approved, bt.Approved2 
       FROM BankTransactions bt 
       LEFT OUTER JOIN (
               SELECT HS.ID, HS.StatementType 
               FROM BankTransactions HS 
               WHERE HS.Type = 'S' AND HS.Status = 'J' 
               AND HS.DebtorNumber IS NOT NULL 
               AND (  
                   (ISNULL(HS.StatementType, '') <> 'F' AND ISNULL(ISNULL(HS.InvoiceDate,HS.ProcessingDate),HS.valuedate) <= {d '".$date_last."'}) 
                   OR HS.StatementType = 'F')
                   )
       AS HS ON bt.MatchID = HS.ID 
       LEFT OUTER JOIN (
           SELECT MatchID FROM BankTransactions W
               WHERE W.Type = 'W' AND W.Status <> 'V' AND W.MatchID IS NOT NULL
                   AND W.DebtorNumber IS NOT NULL 
               AND ISNULL(ISNULL(W.InvoiceDate,W.ProcessingDate),W.ValueDate) > {d '".$date_last."'}
               GROUP BY MatchID
       ) AS HW ON bt.MatchID = HW.MatchID AND HS.StatementType = 'F'
       INNER JOIN cicmpy ci on bt.DebtorNumber = ci.debnr
       WHERE Type = 'W' AND bt.Status IN ('C','A','P','J')  AND EntryNumber IS NOT NULL 
       AND DebtorNumber IS NOT NULL AND ROUND(AmountDC,2) <> 0 
       AND ISNULL(ISNULL(InvoiceDate,ProcessingDate),ValueDate) <= {d '".$date_last."'}
       AND (HS.ID IS NULL OR (HS.ID IS NOT NULL AND HW.MatchID IS NOT NULL))
                  
  UNION ALL 
       
       SELECT '' AS Empty, s.ID, s.DebtorNumber, s.CreditorNumber, s.ValueDate,        (ISNULL(s.AmountDC,0) - ISNULL(W2.AmountDC,0)) AS AmountDC,        (ISNULL(s.AmountTC,0) - ISNULL(W2.AmountTC,0)) AS AmountTC, 
       s.ProcessingDate, s.InvoiceDate, s.ReportingDate, s.Type, s.OffSetName, s.PaymentType, s.SupplierInvoiceNumber,  
       CAST(s.Description AS VARCHAR(400)) AS Description , s.TransactionType, s.OffsetReference, 
       s.OffSetLedgerAccountNumber, s.Blocked, s.DocumentID, s.OrderNumber, 
       ISNULL(s.InvoiceNumber, '') AS InvoiceNumber 
       , s.DueDate, s.TCCode, s.Status , s.MatchID, s.BatchNumber, s.OwnBankAccount, s.EntryNumber,  s.LedgerAccount, 

       s.sysguid,  (CASE WHEN ExchangeRate = 0 THEN CONVERT(VARCHAR(20),ROUND(ExchangeRate,6)) ELSE (CASE WHEN 1/ExchangeRate > 10000 THEN '1/' + CONVERT(VARCHAR(20), ROUND(1/ExchangeRate,6)) ELSE CONVERT(VARCHAR(20), ROUND(ExchangeRate,6)) END) END) AS ExchangeRate, 
       s.Approved, s.Approved2 
       FROM BankTransactions s 
       INNER JOIN cicmpy ci ON s.DebtorNumber = ci.debnr           LEFT OUTER JOIN
               (SELECT MatchID, ROUND(SUM(ROUND(ISNULL(AmountDC,0),2)),2) AS AmountDC, 
               ROUND(SUM(ROUND(ISNULL(AmountTC,0),2)),2) AS AmountTC  FROM BankTransactions w 
               WHERE w.Type = 'W' AND w.Status IN ('C','A','P','J') 
               AND ISNULL(w.InvoiceDate,ISNULL(w.ProcessingDate,w.ValueDate)) <= {d '".$date_last."'}
               AND w.EntryNumber IS NOT NULL 

               GROUP BY MatchID 
               HAVING MatchID IS NOT NULL ) AS W2 ON W2.MatchID = S.ID 
       WHERE s.Type='S' AND s.Status = 'J' 
       AND s.DebtorNumber IS NOT NULL 
       AND ROUND(s.AmountDC,2) <> 0 
       AND ROUND((ISNULL(s.AmountDC, 0) - ISNULL(W2.AmountDC, 0)),2) <> 0 
                   
       AND s.ValueDate <= {d '".$date_last."'}
)) bt 
 INNER JOIN cicmpy ci on bt.DebtorNumber= ci.debnr AND bt.DebtorNumber IS NOT NULL 
 LEFT OUTER JOIN addresses addr on ci.cmp_wwn = addr.account AND addr.Type = 'INV' 
 LEFT OUTER JOIN classifications cc ON ci.ClassificationId = cc.ClassificationID
 LEFT OUTER JOIN cicntp cp ON cp.cnt_id=ci.cnt_id 
 left join [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_map] igm on igm.ie_grouping_id='".$gl_id."' and igm.company_id='".$head_com_id."'
 left join [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_debtor] iedc on iedc.company_id_user='".$head_com_id."' and company_id_mapping='".$this_com_id."'
 WHERE 
 ci.debcode IS NOT NULL 
 AND ISNULL(ISNULL(bt.InvoiceDate,bt.ProcessingDate),bt.Valuedate) <= {d '".$date_last."'}
 AND (bt.CreditorNumber IS NULL OR bt.CreditorNumber NOT IN ('000002')) 
 and RTRIM(LTRIM(igm.gl_code))= RTRIM(LTRIM(bt.OffsetLedgerAccountNumber))
 and RTRIM(LTRIM(bt.DebtorNumber)) = RTRIM(LTRIM(iedc.debnr)) 
 ORDER BY ci.debcode, ci.cmp_name, (CASE WHEN bt.Type = 'S' THEN bt.ValueDate ELSE bt.InvoiceDate END)
";
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




function query_creditor($date_start,$date_last,$gl_id,$head_com_id,$this_com_id){
	return "select iedc.crdnr as cmp_code_exact,igm.gl_code,ci.cmp_name AS OffSetName, (CASE WHEN bt.Type = 'S' THEN bt.ValueDate ELSE bt.InvoiceDate END) AS InvDate, crdcode AS Relation, bt.InvoiceNumber, bt.SupplierInvoiceNumber, (CASE WHEN bt.Type = 'W' THEN -bt.AmountDC ELSE bt.AmountDC END ) AS AmountDC, bt.Description, bt.EntryNumber, (CASE ci.cmp_type WHEN 'A' THEN 'Associate' WHEN 'B' THEN 'Bank' WHEN 'C' THEN 'Customer'
 WHEN 'D' THEN 'Division' WHEN 'E' THEN 'Employee' WHEN 'N' THEN 'Not validated'
 WHEN 'P' THEN 'Prospect' WHEN 'R' THEN 'Reseller' WHEN 'L' THEN 'Lead'
 WHEN 'S' THEN 'Supplier'
 WHEN 'T' THEN 'Suspect' ELSE  ci.cmp_type END) AS RTYPE, bt.OffsetLedgerAccountNumber, (DATEDIFF(dd, ISNULL(bt.DueDate,bt.ValueDate), {d '".$date_last."'})) AS iAge, bt.OrderNumber, CreditorNumber AS OffsetNumber, bt.DueDate AS DueDate, (CASE WHEN bt.AmountDC > 0 AND bt.Type = 'W' THEN bt.AmountDC ELSE 
      (CASE WHEN bt.Type = 'S' AND bt.AmountDC < 0 THEN -bt.AmountDC ELSE NULL END ) 
 END ) AS Debit, (CASE WHEN bt.AmountDC < 0 AND bt.Type = 'W' THEN -bt.AmountDC ELSE 
      (CASE WHEN bt.Type = 'S' AND bt.AmountDC > 0 THEN bt.AmountDC ELSE NULL END ) 
 END ) AS Credit, ROUND((CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) < 0 AND bt.Type = 'W'       THEN -bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) < 0 AND bt.Type = 'S'       THEN bt.AmountDC ELSE NULL END ) END ) , 2) AS NotDue, bt.TCCode, bt.ID, addr.AddressLine1 AS Adres, addr.Postcode AS Postcode, addr.City AS City, addr.Phone AS Tel, addr.Fax AS Fax, cp.cnt_l_name AS Contact, ci.vatnumber AS VatNumber, ci.cmp_fctry AS Country, (CASE WHEN bt.DocumentID IS NULL THEN 0 ELSE 1 END) AS DocumentID, ci.creditline AS Creditline, bt.ExchangeRate, ROUND(
(CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) BETWEEN 0 AND 30 AND bt.Type = 'W'       THEN -bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) BETWEEN 0 AND 30 AND bt.Type = 'S'       THEN bt.AmountDC ELSE NULL END ) END ) , 2) AS T1, ROUND(
(CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) BETWEEN 31 AND 60 AND bt.Type = 'W'       THEN -bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) BETWEEN 31 AND 60 AND bt.Type = 'S'       THEN bt.AmountDC ELSE NULL END ) END ) , 2) AS T2, ROUND(
(CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) BETWEEN 61 AND 90 AND bt.Type = 'W'       THEN -bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) BETWEEN 61 AND 90 AND bt.Type = 'S'       THEN bt.AmountDC ELSE NULL END ) END ) , 2) AS T3, ROUND(
(CASE WHEN DATEDIFF(dd, bt.DueDate, {d '".$date_last."'} ) > 90 AND bt.Type = 'W'       THEN -bt.AmountDC ELSE   (CASE WHEN DATEDIFF(dd, bt.ValueDate, {d '".$date_last."'} ) > 90 AND bt.Type = 'S'       THEN bt.AmountDC ELSE NULL END ) END ) , 2) AS T4, cc.ClassificationID AS Classification, (CASE bt.PaymentType WHEN 'A' THEN 'Automatic collection'
 WHEN 'B' THEN 'On credit'
 WHEN 'C' THEN 'Cheque'
 WHEN 'D' THEN 'Post dated cheque'
 WHEN 'E' THEN 'EFT'
 WHEN 'F' THEN 'Factoring'
 WHEN 'H' THEN 'Chipknip'
 WHEN 'I' THEN 'Collection'
 WHEN 'K' THEN 'Cash'
 WHEN 'L' THEN 'Factoring: Letter of credit'
 WHEN 'O' THEN 'Debt collection'
 WHEN 'P' THEN 'Payment on delivery'
 WHEN 'Q' THEN 'Confirming: Cheque'
 WHEN 'R' THEN 'Credit card'
 WHEN 'S' THEN 'To be settled'
 WHEN 'W' THEN 'Letter of credit'
 WHEN 'M' THEN 'Not accepted letter of credit'
 WHEN 'N' THEN 'Promissory note'
 WHEN 'T' THEN 'Factoring: Collection'
 WHEN 'U' THEN 'Confirming: On credit'
 WHEN 'V' THEN 'ESR payments'
 WHEN 'Y' THEN 'Payments in FC'
 WHEN 'X' THEN 'Payments in CHF'
 WHEN 'Z' THEN 'Payments abroad'
 ELSE  bt.PaymentType END) AS PaymentType, bt.Blocked AS Blocked, bt.OffsetReference, (CASE ci.cmp_status WHEN 'A' THEN 'Active'
 WHEN 'B' THEN 'Blocked'
 WHEN 'E' THEN 'Inactive'
 WHEN 'N' THEN 'Not validated'
 WHEN 'P' THEN 'Pilot'
 WHEN 'R' THEN 'Reference'
 WHEN 'S' THEN 'Passive'
 ELSE  ci.cmp_status END) AS RSTATUS, (CASE WHEN bt.MatchID IS NOT NULL THEN 'Matched' ELSE  (CASE WHEN ISNULL (bt.Status, '') = 'C' AND bt.Approved IS NULL THEN 'Entered' WHEN ISNULL (bt.Status, '') = 'C' AND bt.Approved IS NOT NULL THEN 'Authorised: 1' WHEN ISNULL (bt.Status, '') = 'A' AND bt.Approved IS NOT NULL THEN 'Authorised' WHEN ISNULL (bt.Status, '') = 'A' AND bt.Approved2 IS NOT NULL THEN 'Authorised: 2' WHEN ISNULL (bt.Status, '') = 'P' THEN 'Processed' WHEN ISNULL (bt.Status, '') = 'D' THEN 'Deposit' WHEN ISNULL (bt.Status, '') IN ('U','J') THEN 'Unallocated' ELSE ' ' END) END) AS TSTATUS, (CASE bt.Type 
 WHEN 'S' THEN 'Cash flow'
 WHEN 'W' THEN 'Terms'
 END) AS Type, (CASE bt.TransactionType WHEN 'N' THEN 'Other'
 WHEN 'K' THEN 'Sales invoice'
 WHEN 'C' THEN 'Sales credit note'
 WHEN 'T' THEN 'Purchase invoice'
 WHEN 'Q' THEN 'Purchase credit note'
 WHEN 'Z' THEN 'Cash receipt'
 WHEN 'Y' THEN 'Payment'
 WHEN 'R' THEN 'Refund'
 WHEN 'P' THEN 'Interbank'
 WHEN 'S' THEN 'Reversal credit note'
 WHEN 'D' THEN 'Debit memo/Financial charge'
 WHEN 'F' THEN 'Discount'
 WHEN 'U' THEN 'Credit surcharge'
 WHEN 'M' THEN 'Machine hours'
 WHEN 'L' THEN 'Labour hours'
 WHEN 'E' THEN 'Revaluation'
 WHEN 'I' THEN 'Disposal'
 WHEN 'V' THEN 'Depreciation'
 WHEN 'A' THEN 'Receipt'
 WHEN 'B' THEN 'Fulfilment'
 WHEN 'G' THEN 'Counts'
 WHEN 'H' THEN 'Return fulfilment'
 WHEN 'J' THEN 'Return receipt'
 WHEN 'W' THEN 'Payroll'
 WHEN 'O' THEN 'POS Sales invoice'
 WHEN 'X' THEN 'Settled'
 ELSE  bt.TransactionType END) AS TransactionType, (CASE WHEN bt.Type = 'W' THEN -bt.AmountTC ELSE bt.AmountTC END ) AS AmountTC, (SELECT -1 * SUM(X.AmountTC) FROM BankTransactions X 
  WHERE X.Type = 'W' AND X.Status IN ('C','A','P','J')     AND X.InvoiceNumber = bt.InvoiceNumber 
    AND X.CreditorNumber = bt.CreditorNumber 
    AND X.TransactionType NOT IN ('Y','Z','E') 
    AND X.EntryNumber = bt.EntryNumber ) AS InvoiceAmt FROM (( 
       
       SELECT '' AS Empty, bt.ID, DebtorNumber, CreditorNumber, ValueDate, AmountDC, AmountTC, 
               ProcessingDate, InvoiceDate, ReportingDate, Type, OffSetName, PaymentType, SupplierInvoiceNumber, 
               CAST(Description AS VARCHAR(400)) AS Description, TransactionType, OffsetReference, 
               OffSetLedgerAccountNumber, bt.Blocked, DocumentID, OrderNumber, InvoiceNumber, 
               DueDate, TcCode, bt.Status, bt.MatchID, BatchNumber, OwnBankAccount, EntryNumber, bt.LedgerAccount, 
 
               bt.sysguid, (CASE WHEN ExchangeRate = 0 THEN CONVERT(VARCHAR(20),ROUND(ExchangeRate,6)) ELSE (CASE WHEN 1/ExchangeRate > 10000 THEN '1/' + CONVERT(VARCHAR(20), ROUND(1/ExchangeRate,6)) ELSE CONVERT(VARCHAR(20), ROUND(ExchangeRate,6)) END) END) AS ExchangeRate,
               bt.Approved, bt.Approved2 
       FROM BankTransactions bt 
       LEFT OUTER JOIN (
               SELECT HS.ID, HS.StatementType 
               FROM BankTransactions HS 
               WHERE HS.Type = 'S' AND HS.Status = 'J' 
               AND HS.CreditorNumber IS NOT NULL 
               AND (  
                   (ISNULL(HS.StatementType, '') <> 'F' AND ISNULL(ISNULL(HS.InvoiceDate,HS.ProcessingDate),HS.valuedate) <= {d '".$date_last."'}) 
                   OR HS.StatementType = 'F')
                   )
       AS HS ON bt.MatchID = HS.ID 
       LEFT OUTER JOIN (
           SELECT MatchID FROM BankTransactions W
               WHERE W.Type = 'W' AND W.Status <> 'V' AND W.MatchID IS NOT NULL
                   AND W.CreditorNumber IS NOT NULL 
               AND ISNULL(ISNULL(W.InvoiceDate,W.ProcessingDate),W.ValueDate) > {d '".$date_last."'}
               GROUP BY MatchID
       ) AS HW ON bt.MatchID = HW.MatchID AND HS.StatementType = 'F'
       INNER JOIN cicmpy ci on bt.CreditorNumber = ci.crdnr
       WHERE Type = 'W' AND bt.Status IN ('C','A','P','J')  AND EntryNumber IS NOT NULL 
       AND CreditorNumber IS NOT NULL AND ROUND(AmountDC,2) <> 0 
       AND ISNULL(ISNULL(InvoiceDate,ProcessingDate),ValueDate) <= {d '".$date_last."'}
       AND (HS.ID IS NULL OR (HS.ID IS NOT NULL AND HW.MatchID IS NOT NULL))
                  
  UNION ALL 
      
       SELECT '' AS Empty, s.ID, s.DebtorNumber, s.CreditorNumber, s.ValueDate,        (ISNULL(s.AmountDC,0) - ISNULL(W2.AmountDC,0)) AS AmountDC,        (ISNULL(s.AmountTC,0) - ISNULL(W2.AmountTC,0)) AS AmountTC, 
       s.ProcessingDate, s.InvoiceDate, s.ReportingDate, s.Type, s.OffSetName, s.PaymentType, s.SupplierInvoiceNumber,  
       CAST(s.Description AS VARCHAR(400)) AS Description , s.TransactionType, s.OffsetReference, 
       s.OffSetLedgerAccountNumber, s.Blocked, s.DocumentID, s.OrderNumber, 
       ISNULL(s.InvoiceNumber, '') AS InvoiceNumber 
       , s.DueDate, s.TCCode, s.Status , s.MatchID, s.BatchNumber, s.OwnBankAccount, s.EntryNumber,  s.LedgerAccount, 

       s.sysguid,  (CASE WHEN ExchangeRate = 0 THEN CONVERT(VARCHAR(20),ROUND(ExchangeRate,6)) ELSE (CASE WHEN 1/ExchangeRate > 10000 THEN '1/' + CONVERT(VARCHAR(20), ROUND(1/ExchangeRate,6)) ELSE CONVERT(VARCHAR(20), ROUND(ExchangeRate,6)) END) END) AS ExchangeRate, 
       s.Approved, s.Approved2 
       FROM BankTransactions s 
       INNER JOIN cicmpy ci ON s.CreditorNumber = ci.crdnr           LEFT OUTER JOIN
               (SELECT MatchID, ROUND(SUM(ROUND(ISNULL(AmountDC,0),2)),2) AS AmountDC, 
               ROUND(SUM(ROUND(ISNULL(AmountTC,0),2)),2) AS AmountTC  FROM BankTransactions w 
               WHERE w.Type = 'W' AND w.Status IN ('C','A','P','J') 
               AND ISNULL(w.InvoiceDate,ISNULL(w.ProcessingDate,w.ValueDate)) <= {d '".$date_last."'}
               AND w.EntryNumber IS NOT NULL 

               GROUP BY MatchID 
               HAVING MatchID IS NOT NULL ) AS W2 ON W2.MatchID = S.ID 
       WHERE s.Type='S' AND s.Status = 'J' 
       AND s.CreditorNumber IS NOT NULL 
       AND ROUND(s.AmountDC,2) <> 0 
       AND ROUND((ISNULL(s.AmountDC, 0) - ISNULL(W2.AmountDC, 0)),2) <> 0 
                   
       AND s.ValueDate <= {d '".$date_last."'}
)) bt 
 INNER JOIN cicmpy ci on bt.CreditorNumber= ci.crdnr AND bt.CreditorNumber IS NOT NULL 
 LEFT OUTER JOIN addresses addr on ci.cmp_wwn = addr.account AND addr.Type = 'INV' 
 LEFT OUTER JOIN classifications cc ON ci.ClassificationId = cc.ClassificationID
 LEFT OUTER JOIN cicntp cp ON cp.cnt_id=ci.cnt_id
 left join [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_map] igm on igm.ie_grouping_id='".$gl_id."' and igm.company_id='".$head_com_id."'
 left join [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_creditor] iedc on iedc.company_id_user='".$head_com_id."' and company_id_mapping='".$this_com_id."'
 WHERE ci.crdcode IS NOT NULL 
 AND ISNULL(ISNULL(bt.InvoiceDate,bt.ProcessingDate),bt.Valuedate) <= {d '".$date_last."'}
 AND (bt.CreditorNumber IS NULL OR bt.CreditorNumber NOT IN ('000002')) 
 and RTRIM(LTRIM(igm.gl_code))= RTRIM(LTRIM(bt.OffsetLedgerAccountNumber))
 and RTRIM(LTRIM(bt.CreditorNumber)) = RTRIM(LTRIM(iedc.crdnr)) 
 ORDER BY ci.crdcode, ci.cmp_name, (CASE WHEN bt.Type = 'S' THEN bt.ValueDate ELSE bt.InvoiceDate END)
";
}


if($_POST['deb_crd_type']=="debnr"){
	$table_deb_crd = "ie_debtor";
}else{$table_deb_crd = "ie_creditor";}
//echo $table_deb_crd;
/*
$q_local = new Query_local();
$check_pl_bs = $q_local->query_table("select type from ie_grouping_gl where atid='".$_POST['gl_id']."'");
if(is_array($check_pl_bs)&&sizeof($check_pl_bs)>0){
	$type_gl = $check_pl_bs[0]['type'];
}else{
	$type_gl = NULL;
	exit();
}*/

if($_POST['role_td']=="left"){
	$q_domain = new Query_domain($_POST['head_com_code']);
	if($table_deb_crd=="ie_debtor"){$str_q = query_debtor($date_start,$date_last,$_POST['gl_id'],$_POST['head_com'],$_POST['this_com']);}
	else{$str_q = query_creditor($date_start,$date_last,$_POST['gl_id'],$_POST['head_com'],$_POST['this_com']);}
}else{
	$q_domain = new Query_domain($_POST['this_com_code']);
	if($table_deb_crd=="ie_debtor"){$str_q = query_debtor($date_start,$date_last,$_POST['gl_id'],$_POST['this_com'],$_POST['head_com']);}
	else{$str_q = query_creditor($date_start,$date_last,$_POST['gl_id'],$_POST['this_com'],$_POST['head_com']);}
}

$arr_q = $q_domain->query_table($str_q);
if(is_array($arr_q)&&sizeof($arr_q)>0){
?>
	<table style="font-size:12px;" border="1" id="table_trans_inner_view_by_com"  width="100%">
	<thead style="background-color:#fafafa;border-bottom:2px solid;">
		<tr>
			<td>Name</td>
			<td>Account</td>
			<td>Date</td>
			<td>ourref</td>
			<td>yourref</td>
			<td>Amount (THB)</td>
			<td>Classification</td>
			<td>Description</td>
			<td>Entry Number</td>
			<td>Offset Account</td>
			<td>Age</td>
			<td>PO/SO</td>
		</tr>
	</thead>
	<tbody>
	<?php
	$i=0; $sum_amount = 0;
	while($i<sizeof($arr_q)){
		$arr_row = $arr_q[$i];
		echo '<tr>';
		echo '<td>'.$arr_row["OffSetName"].'</td>';
		echo '<td>'.$arr_row["cmp_code_exact"].'</td>';
		echo '<td>'.$class_date->change_date_from_db_to_show_date($arr_row["InvDate"]).'</td>';
		echo '<td>'.$arr_row["InvoiceNumber"].'</td>';
		echo '<td>'.$arr_row["SupplierInvoiceNumber"].'</td>';
		echo '<td>'.number_format($arr_row["AmountDC"],4).'</td>'; $sum_amount+=$arr_row["AmountDC"];
		echo '<td>'.$arr_row["Classification"].'</td>';
		echo '<td>'.$arr_row["Description"].'</td>';
		echo '<td>'.$arr_row["EntryNumber"].'</td>';
		echo '<td>'.$arr_row["gl_code"].'</td>';
		echo '<td>'.$arr_row["iAge"].'</td>';
		echo '<td>'.$arr_row["OrderNumber"].'</td>';
		echo '</tr>';
		$i++;
	}echo '<tr><td colspan="12"> Sum Amount : '.number_format($sum_amount,4).'</td></tr>';
	?>
	</tbody>
	</table>
<?php
}else{echo "ไม่มีข้อมูล";}
?>
<br>
<?php
	//echo $str_q;
?>
<style type="text/css">
#table_trans_inner_view_by_com{
    border:1px grey solid;
}
#table_trans_inner_view_by_com thead td{
    padding: 12px;
    background-color: #F5F5F5;
    /*font-weight: bold;*/
}
#table_trans_inner_view_by_com tbody td{
    padding: 8px;
    cursor:pointer;
    border-right:1px grey solid;
}
#table_trans_inner_view_by_com tfoot td{
    padding: 8px;
    border-right:1px grey solid;
}
</style>