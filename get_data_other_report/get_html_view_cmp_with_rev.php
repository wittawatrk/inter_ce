<?php
require_once '../function.php';
//$_POST['user_company_code']="CI";
//$_POST['user_company_id']="3";
//$_POST['year']="2016";
//$_POST['month']="1";
$date_start = $_POST['year']."-".str_pad($_POST['month_start'],2,"0",STR_PAD_LEFT)."-01";
$date_last =  $_POST['year']."-".str_pad($_POST['month_end'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($_POST['year']."-".$_POST['month_end']."-01")),2,"0",STR_PAD_LEFT);
$cmp_id = $_POST['user_cmp_id'];
$cmp_code = $_POST['user_cmp_code'];
$year = $_POST['year'];
$month_start = $_POST['month_start'];
$month_end = $_POST['month_end'];

function get_q_value($type_gl,$gl_id,$com_id,$rev_id,$year,$month_start,$month_end,$date_start,$date_last){
    if($type_gl=="B/S"){  /// B/S ไม่ SUM ช่วงให้ เพราะเป็นยอดปิด ไม่งั้นจะผิด
        $str_return_q = " select sum(isl.amount) as amount
  from ie_summary_lastest  isl
  left join company cmp on isl.company_id_mapping = cmp.company_id
  left join revenue rev on cmp.revenue_id = rev.revenue_id
  where isl.company_id_user='".$com_id."'
  and rev.revenue_id = '".$rev_id."'
  and isl.ie_grouping_gl_id='".$gl_id."'
  and (isl.year='".$year."' and isl.month='".$month_end."') ";
    }else{
        $str_return_q = " select SUM(isl.amount) as amount
        from ie_summary_lastest isl
        left join company cmp on isl.company_id_mapping = cmp.company_id
        left join revenue rev on cmp.revenue_id = rev.revenue_id
        where isl.company_id_user='".$com_id."'
        and rev.revenue_id = '".$rev_id."'
        and isl.ie_grouping_gl_id='".$gl_id."'
        and isl.year = ".$year."
        and isl.month between ".$month_start." and ".$month_end." ";
    }
    return $str_return_q;
}
function sum_value($arr_q){
    $sum_value  = 0;
    foreach ($arr_q as $key => $value) {
        $sum_value+=$value['amount'];
    }
    return $sum_value;
}


$class_q_local = new Query_local();
$q_company = "select * from company where company_id='".$cmp_id."'";
$arr_company=$class_q_local->query_table($q_company);
$num_company = sizeof($arr_company);

$q_revenue = "select * from revenue ";
$arr_revenue=$class_q_local->query_table($q_revenue);
$num_revenue = sizeof($arr_revenue);

$width_cal = floor((85/(($num_revenue)+1)));

echo '<table border="1" id="table_show_compare" style="width:100%;border-collapse: collapse;">';
echo '<tr class="GridviewScrollHeader">';
        echo '<td scope="col" >GL ('.$_POST["user_cmp_code"].')</td>';
        echo '<td scope="col" >Type</td>';
        $j=0;
        while($j<$num_revenue){
            echo '<td scope="col" width="'.$width_cal.'%">'.$arr_revenue[$j]["revenue_name"].'</td>';
            $j++;
        }
        echo '<td scope="col" width="'.$width_cal.'%">Total</td>';
echo '</tr>';
$count_in_gl_loop = 0;
$q_gl = "select * from ie_grouping_gl   order by group_no asc,orderg asc";
$arr_gl = $class_q_local->query_table($q_gl);
$i=0; $sum_vertical = array();
while($i<sizeof($arr_gl)){
    $data_gl = $arr_gl[$i];
    echo '<tr class="GridviewScrollItem">';
    echo '<td>'.$data_gl["name"].'</td>';
    echo '<td>'.$data_gl["type_show"].'</td>';
    $j=0; $sum_horizonal = NULL;
    while($j<$num_revenue){
        $rev_id = $arr_revenue[$j]["revenue_id"];
        $rev_code = $arr_revenue[$j]["revenue_name"];
        if(!isset($sum_vertical[$j])){$sum_vertical[$j]=NULL;}
        $get_query = get_q_value($data_gl['type'],$data_gl['atid'],$cmp_id,$rev_id,$year,$month_start,$month_end,$date_start,$date_last);
        $arr_value_amount = $class_q_local->query_table($get_query);

        if(isset($arr_value_amount[0])){
        	$loop_value = sum_value($arr_value_amount);
            $value_amount = number_format($loop_value,2);
            $sum_horizonal+=$loop_value;
            $sum_vertical[$j]=$sum_vertical[$j]+$loop_value;
        }else{
            $value_amount = NULL;
        }

        echo '<td>';
            echo '<span class="span_amount" head-com="'.$cmp_id.'" head-com-code="'.$cmp_code.'" gl-id="'.$data_gl["atid"].'" this-rev="'.$rev_id.'" this-rev-code="'.$rev_code.'" deb-crd-type="'.$data_gl["type"].'">';
            echo $value_amount;
            echo '</span>';
        echo '</td>';
        //echo '<td title="'.$get_query.'">'.$value_amount.'</td>';

        $j++;
    }
    echo '<td>'; echo ($sum_horizonal!=NULL)?number_format($sum_horizonal,2):"";  echo '</td>';
    echo '</tr>';
    $i++; ///////// loop GL
    ////////////////////////////////////////////////////////// total group
    $next_gl_group_no = (isset($arr_gl[$i]['group_no']))?$arr_gl[$i]['group_no']:""; $sum_horizonal_of_vertical=NULL;
    if(($next_gl_group_no!=$data_gl['group_no'])){
        echo '<tr style="background-color:#EFEFEF;">';
        echo '<td>Total Group : '.$data_gl["group_no"].'</td>'; echo '<td></td>';
        $k=0;
        while($k<sizeof($arr_revenue)){
            echo '<td>'; if($sum_vertical[$k]!=NULL){echo number_format($sum_vertical[$k],2); $sum_horizonal_of_vertical+=$sum_vertical[$k];}  echo '</td>';
            $k++;
        }
        echo '<td>'; echo ($sum_horizonal_of_vertical!=NULL)?number_format($sum_horizonal_of_vertical,2):""; echo '</td>';
        echo '</tr>';
        $sum_vertical = array();
    }

}
echo '</table>';
?>
<script type="text/javascript">
blink('span[status=blink]');
function blink(selector){
    $(selector).css("background-color","#FFE4E1");
}
</script>
<style type="text/css">
#table_show_compare td{
 text-align: center;
}
.td_total{
	font-size: 80%;
}
.td_to_compare_trans,.td_total,.sum_by_gl_left,.sum_by_gl_right,.sum_by_gl_diff{
	font-size: 90%;
	word-wrap: break-word;
    overflow-wrap: break-word;
}
.td_to_compare_trans_blink{
	font-size: 90%;
	background-color: #FFE4E1;
	word-wrap: break-word;
    overflow-wrap: break-word;
}
.GridviewScrollHeader TH, .GridviewScrollHeader TD
{
    padding: 5px;
    font-weight: bold;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #EFEFEF;
    text-align: center;
    vertical-align: middle;
}
.GridviewScrollItem TD
{
    padding: 5px;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #FFFFFF;
    font-size: 90%;
}
</style>
