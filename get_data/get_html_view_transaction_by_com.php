<?php
require_once '../function.php';
$date_start = $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-01";
$date_last =  $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($date_start)),2,"0",STR_PAD_LEFT);

if($_POST['deb_crd_type']=="debnr"){
	$table_deb_crd = "ie_debtor";
}else{$table_deb_crd = "ie_creditor";}

$q_domain = new Query_domain($_POST['head_com_code']);
$q_local = new Query_local();

$check_pl_bs = $q_local->query_table("select type from ie_grouping_gl where atid='".$_POST['gl_id']."'");
if(is_array($check_pl_bs)&&sizeof($check_pl_bs)>0){
	$type_gl = $check_pl_bs[0]['type'];
}else{
	$type_gl = NULL;
	exit();
}


if($_POST['head_com_code']=="PSP"||$_POST['head_com_code']=="PCE"||$_POST['head_com_code']=="PSS"){
	$str_q = "select op.gl_no as glcode
,entry_no
,ourref
,yourref
,des
,itemcode
,item_des
,(op.debit-op.credit) as Amount
, CASE op.customer  
         WHEN '0' THEN ''  
         WHEN '1' THEN op.ref  
         ELSE ''  
      END as debnr
, CASE op.customer  
         WHEN '0' THEN ''  
         WHEN '1' THEN op.name  
         ELSE ''  
      END as deb_name


, CASE op.supplier  
         WHEN '0' THEN ''  
         WHEN '1' THEN op.ref  
         ELSE ''  
      END as crdnr
, CASE op.supplier  
         WHEN '0' THEN ''  
         WHEN '1' THEN op.name  
         ELSE ''  
      END as crd_name
,'' as so

 from openquery([OP_DB_".$_POST['head_com_code']."_CLEAN],'select
aa.code as gl_no,

am.name as entry_no,
am.ref as ourref,
'''' as yourref,
am.description as des,
aaa.code as itemcode,
'''' as item_des,

rp.ref ,
rp.name,
rp.customer,
rp.supplier,
'''' as so,

al.credit,al.debit
from account_move_line as al
left outer join account_move  as am on al.move_id=am.id
left outer join account_account as aa on al.account_id=aa.id
left outer join account_period as ap on am.period_id=ap.id
left outer join res_partner as rp on al.partner_id=rp.id
left outer join account_analytic_account aaa on al.analytic_account_id=aaa.id
where
ap.name=''".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."/".$_POST['year']."'' and al.partner_id is not null;') as op

WHERE
op.gl_no IN (SELECT rtrim(ltrim([gl_code])) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_map] WHERE [company_id] = ".$_POST['head_com']." AND [ie_grouping_id] = ".$_POST['gl_id']." ) 
AND op.ref IN (SELECT rtrim(ltrim(".$_POST['deb_crd_type'].")) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[".$table_deb_crd."] WHERE [company_id_user] = ".$_POST['head_com']." AND [company_id_mapping] = ".$_POST['this_com'].")";

}
else{
	$str_q = "select g.reknr as glcode,g.bkstnr as entry_no,g.faktuurnr as ourref,g.docnumber as yourref,g.oms25 as des,g.artcode as itemcode
,i.Description as item_des
,g.aantal as qty,g.bdr_hfl as Amount
,c1.debnr,c1.cmp_name as deb_name
,c2.crdnr,c2.cmp_name as crd_name
,g.bkstnr_sub as so
from gbkmut g with(nolock) 
LEFT OUTER JOIN items i ON i.ItemCode = g.artcode
LEFT OUTER JOIN cicmpy c1 ON g.debnr = c1.debnr
LEFT OUTER JOIN cicmpy c2 ON g.crdnr = c2.crdnr

WHERE
g.transtype IN ('N', 'C', 'P') 
 AND g.bud_vers is NULL  
 AND g.reknr IN (SELECT [gl_code] FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_map] WHERE [company_id] = ".$_POST['head_com']." AND [ie_grouping_id] = ".$_POST['gl_id']." ) AND
 ltrim(rtrim(g.[".$_POST['deb_crd_type']."])) IN (SELECT ltrim(rtrim(".$_POST['deb_crd_type'].")) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[".$table_deb_crd."] WHERE [company_id_user] = ".$_POST['head_com']." AND [company_id_mapping] = ".$_POST['this_com'].")
  AND ISNULL(g.transsubtype,'') <> 'X'  AND g.datum >='".$date_start."'   AND g.datum <= '".$date_last."'
AND g.oorsprong <> 'S'  AND g.remindercount <=13 AND g.bkstnr IS NOT NULL
";
}



$arr_trans = $q_domain->query_table($str_q);
if(!is_array($arr_trans)){echo "error"; exit();}
else{
	$size_arr_trans = sizeof($arr_trans); $i=0; 
	?>
	<table style="font-size:12px;" border="1" id="table_trans_inner_view_by_com"  width="100%">
	<thead style="background-color:#fafafa;border-bottom:2px solid;">
		<tr>
			<td>glcode</td>
			<td>entry_no</td>
			<td>ourref</td>
			<td>yourref</td>
			<td>des</td>
			<td>itemcode</td>
			<td>item_des</td>
			<td>Amount</td>
			<td>debnr</td>
			<td>deb_name</td>
			<td>crdnr</td>
			<td>crd_name</td>
			<td>so</td>
		</tr>
	</thead>
	<tbody>
	<?php
	$sum_amount = 0;
	while($i<$size_arr_trans){
		$arr_in_trans = $arr_trans[$i];
		echo '<tr>';
		echo '<td>'.$arr_in_trans["glcode"].'</td>';
		echo '<td>'.$arr_in_trans["entry_no"].'</td>';
		echo '<td>'.$arr_in_trans["ourref"].'</td>';
		echo '<td>'.$arr_in_trans["yourref"].'</td>';
		echo '<td>'.$arr_in_trans["des"].'</td>';
		echo '<td>'.$arr_in_trans["itemcode"].'</td>';
		echo '<td>'.$arr_in_trans["item_des"].'</td>';
			if(($arr_in_trans["Amount"]!=NULL&&$arr_in_trans["Amount"]!="")){
				echo '<td>'.number_format($arr_in_trans["Amount"],2).'</td>';
			}else{
				echo '<td></td>';
			}
		echo '<td>'.$arr_in_trans["debnr"].'</td>';
		echo '<td>'.$arr_in_trans["deb_name"].'</td>';
		echo '<td>'.$arr_in_trans["crdnr"].'</td>';
		echo '<td>'.$arr_in_trans["crd_name"].'</td>';
		echo '<td>'.$arr_in_trans["so"].'</td>';
		echo '</tr>';
		$sum_amount+=$arr_in_trans["Amount"];
		$i++;
	}
	echo '<tr><td colspan="13"> Sum This Month : '.number_format($sum_amount,2).'</td></tr>';
	

	$str_start_balance = NULL;
	if($type_gl=="B/S"){
			if($_POST['month']==1&&$_POST['year']==2017){
				$str_start_balance_text = "B/S (Start Balance+ยอดเดือนปัจจุบัน)";
				$str_start_balance = "select io.open_amount as start_balance
	  				from ie_grouping_gl_open_amount io
	  				where io.company_id='".$_POST['head_com']."' and io.company_id_mapping='".$_POST['this_com']."' and io.ie_grouping_id='".$_POST['gl_id']."'
	  				";
	  				$start_balance = $q_local->query_table($str_start_balance);
	  				$num_sum_amount = $sum_amount;
	  				if(is_array($start_balance)&&sizeof($start_balance)>0){$start_balance_num = $start_balance[0]['start_balance'];}else{$start_balance_num = 0;}
	  				echo '<tr><td colspan="13">'.$str_start_balance_text.' : ('.number_format($start_balance_num,2).' + '.number_format($num_sum_amount,2).') = '.number_format(($start_balance_num+$num_sum_amount),2).'</td></tr>';	
	  		}else{
	  			$str_start_balance_text = "B/S ยอดยกมา ";
	  			$str_last_month = "select top 1 ISNULL(amount,0) as amount,MONTH
	from [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_summary_lastest] 
	where company_id_user=".$_POST['head_com']." and company_id_mapping=".$_POST['this_com']."
	and ie_grouping_gl_id=".$_POST['gl_id']." and ((year='".$_POST['year']."' and month<".$_POST['month'].") or (year<".$_POST['year'].")) order by year desc,month desc,revision desc";
				$q_last_month = $q_domain->query_table($str_last_month);
				if(is_array($q_last_month)&&sizeof($q_last_month)>0){$num_last_month = $q_last_month[0]['amount'];}else{$num_last_month = 0;}
				$str_start_balance_text = "B/S ยอดยกมา ";
				echo '<tr><td colspan="13">'.$str_start_balance_text.' : '.number_format($num_last_month,2).' </td></tr>';	
	  		}
	}
	else{
			$str_start_balance_text = "P/L ยอดในเดือนปัจจุบัน";
			$num_sum_amount = $sum_amount;
			echo '<tr><td colspan="13">'.$str_start_balance_text.' : '.number_format($num_sum_amount,2).' </td></tr>';	
	}
	?>
	</tbody>
	</table>
	
	<?php
}
?>
<style type="text/css">
#table_trans_inner_view_by_com{
    border:1px grey solid;
}
#table_trans_inner_view_by_com thead td{
    padding: 12px;
    background-color: #F5F5F5;
    /*font-weight: bold;*/
}
#table_trans_inner_view_by_com tbody td{
    padding: 8px;
    cursor:pointer;
    border-right:1px grey solid;
}
#table_trans_inner_view_by_com tfoot td{
    padding: 8px;
    border-right:1px grey solid;
}
</style>