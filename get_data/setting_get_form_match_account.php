<?php
require_once '../function.php';
$class_query = new Query_local();
$arr_old_acc = $class_query->query_table("select * from ie_grouping_gl_map where company_id='".$_POST['company_id']."' and ie_grouping_id='".$_POST['gl_atid']."'");
?>
<input type="hidden" id="hidden_gl_atid_to_match" value="<?=$_POST['gl_atid']?>">
<div class="form-inline">
    <div class="form-group">
        <input type="text" id="add_account_id" placeholder="ACCOUNT ID" class="form-control">  
    </div>
    <div class="form-group">
        <input type="button" id="add_row_account" onclick="add_row_match_gl();" class="btn btn-default" value="Add" disabled>
    </div>
    <div class="form-group">
        <span id="add_account_des"></span>
    </div>
</div>
<table id="table_add_match_gl" cmp-id="<?=$_POST['company_id']?>" cmp-text="<?=$_POST['company_text']?>" width="100%">
	<thead>
		<tr>
			<td>ID</td>
			<td width="25%">ACCOUNT ID</td>
			<td width="70%">DESCRIPTION</td>
			<td width="5%">DELETE</td>
		</tr>
	</thead>
	<tbody>
    <?php
        $i=0;
        while($i<sizeof($arr_old_acc)){
            echo '<tr>';
            echo '<td>'.$arr_old_acc[$i]["atid"].'</td>';
            echo '<td>'.$arr_old_acc[$i]["gl_code"].'</td>';
            echo '<td>'.$arr_old_acc[$i]["gl_des"].'</td>';
            echo '<td><div align="center"><img class="delete_row_account" src="img/delete_row.png" width="30" onclick="" style="cursor:pointer;" height="30"></div></td>';
            echo '</tr>';
            $i++;
        }

    ?>
	</tbody>
</table>
<script type="text/javascript">
//$('#add_account_id').bind('keypress',function(e){var charCode = (e.which) ? e.which : e.keyCode; if (charCode > 31 && (charCode < 48 || charCode > 57)) { return false;}});
var arr_get_all_account = null;
var arr_autocomplete_account = null;

get_list_autocomplete_account_arr();
var table_add_match_gl=$('#table_add_match_gl').DataTable({"dom":'<<t>p>',"aaSorting":[0],"columnDefs": [{"targets": [0],"visible": false,"searchable": false}],"bPaginate": false,"bSort":false,"createdRow":function(row,data,index){}});
$('#table_add_match_gl tbody').on( 'click', 'img.delete_row_account', function () {
	table_add_match_gl.row($(this).parents('tr')).remove().draw();
});
$("#add_account_id").autocomplete({
 	source: function(request, response) {
 		if(arr_autocomplete_account!=null){
        	var results = $.ui.autocomplete.filter(arr_autocomplete_account, request.term);
        	response(results.slice(0, 10));
    	}
    },
 	select : function(event, ui){
		var des = arr_get_all_account[ui.item.value];
        $("#add_row_account").removeAttr("disabled");
		$("#add_account_des").html(des);
    }
}).click(function() {
	    $(this).autocomplete('search',' ');
}).keypress(function(){$("#add_row_account").attr("disabled","disabled");});
function add_row_match_gl(){
	var i=0; var check_add_row = true;
	var rowCount=table_add_match_gl.column(0).data().length; 
    while(i<rowCount){
        var data_tr = table_add_match_gl.row(i).data();
        if(data_tr[1]==$("#add_account_id").val()){var check_add_row = false; alert("Cannot Add"); break; return false;}
        i++;
    }
	if(check_add_row==true){
        row_node_setting_account=table_add_match_gl.row.add(['',$("#add_account_id").val(),$("#add_account_des").html(),'<div align="center"><img class="delete_row_account" src="img/delete_row.png" width="30" onclick="" style="cursor:pointer;" height="30"></div>']).order( [[ 0, 'asc' ]] ).draw().node(); 
        $("#add_account_id").val('');    
        $("#add_row_account").attr("disabled","disabled");
    }
}
function get_list_autocomplete_account_arr(){
	$.ajax({
            url: "get_data/get_list_autocomplete_account_arr.php",
            async: false,
            dataType: "json",
            type: "post",
            data: {"company_text":$("#table_add_match_gl").attr('cmp-text'),"company_id":$("#table_add_match_gl").attr('cmp-id')},
            success: function (result) {
            	console.log(result);
            	if(result!=undefined&&result[0]==true){
            		arr_get_all_account = result[1];
            		arr_autocomplete_account = result[2];
            	}else{
            		arr_get_all_account = null;
					arr_autocomplete_account = null;
            	}
            	//console.log(arr_autocomplete_account);
            }
    }); 
}

</script>
<style type="text/css">
#table_add_match_gl{
    border:1px grey solid;
}
#table_add_match_gl thead td{
    padding: 12px;
    background-color: #f6f6f6;
    border-bottom: 1px #adaead solid;
    /*font-weight: bold;*/
}
#table_add_match_gl tbody td{
    padding: 8px;
    /*cursor:pointer;*/
    border-right:1px grey solid;
    /*border-bottom:1px grey solid;*/
}
img.delete_row_account{
    cursor: pointer;
    border:1px solid #fff;
        -moz-box-shadow: 10px 10px 5px #ccc;
        -webkit-box-shadow: 10px 10px 5px #ccc;
        box-shadow: 2px 2px 5px #ccc;
        -moz-border-radius:4px;
        -webkit-border-radius:2px;
        border-radius:4px;
}
</style>