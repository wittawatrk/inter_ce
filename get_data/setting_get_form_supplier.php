<?php
require_once '../function.php';
$class_query = new Query_local();
$arr_old_supplier = $class_query->query_table("select * from ie_creditor where company_id_user='".$_POST['user_company_id']."' and company_id_mapping='".$_POST['company_id_map']."'");
?>
<input type="hidden" id="hidden_supplier_company_id_map" value="<?=$_POST['company_id_map']?>">
<div class="form-inline">
    <div class="form-group">
        <input type="text" id="add_supplier_company_code_exact" placeholder="COMPANY CODE" class="form-control">  
    </div>
    <div class="form-group">
        <input type="button" id="add_supplier_row_company_code_exact" onclick="add_row_supplier();" class="btn btn-default" value="Add" disabled>
    </div>
    <div class="form-group">
        <span id="add_supplier_company_name_exact"></span>
    </div>
</div>
<table id="table_add_supplier" cmp-id="<?=$_POST['user_company_id']?>" cmp-text="<?=$_POST['user_company_text']?>" width="100%">
	<thead>
		<tr>
			<td>ID</td>
            <td>DEBNR</td>
			<td width="25%">COMPANY CODE</td>
			<td width="70%">COMPANY NAME</td>
			<td width="5%">DELETE</td>
		</tr>
	</thead>
	<tbody>
    <?php
        $i=0;
        while($i<sizeof($arr_old_supplier)){
            echo '<tr>';
            echo '<td>'.$arr_old_supplier[$i]["atid"].'</td>';
            echo '<td>'.$arr_old_supplier[$i]["crdnr"].'</td>';
            echo '<td>'.$arr_old_supplier[$i]["cmp_code_exact"].'</td>';
            echo '<td>'.$arr_old_supplier[$i]["cmp_name_exact"].'</td>';
            echo '<td><div align="center"><img class="delete_row_supplier" src="img/delete_row.png" width="30" onclick="" style="cursor:pointer;" height="30"></div></td>';
            echo '</tr>';
            $i++;
        }

    ?>
	</tbody>
</table>
<script type="text/javascript">
//$('#add_account_id').bind('keypress',function(e){var charCode = (e.which) ? e.which : e.keyCode; if (charCode > 31 && (charCode < 48 || charCode > 57)) { return false;}});
var arr_get_all_supplier = null;
var arr_autocomplete_supplier = null;

get_list_autocomplete_supplier();

var table_add_supplier=$('#table_add_supplier').DataTable({"dom":'<<t>p>',"aaSorting":[0],"columnDefs": [{"targets": [0,1],"visible": false,"searchable": false}],"bPaginate": false,"bSort":false,"createdRow":function(row,data,index){}});
$('#table_add_supplier tbody').on( 'click', 'img.delete_row_supplier', function () {
	table_add_supplier.row($(this).parents('tr')).remove().draw();
});
$("#add_supplier_company_code_exact").autocomplete({
 	source: function(request, response) {
 		if(arr_autocomplete_supplier!=null){
        	var results = $.ui.autocomplete.filter(arr_autocomplete_supplier, request.term);
        	response(results.slice(0, 10));
    	}
    },
 	select : function(event, ui){
		var arr_cus_name_and_deb_nr = arr_get_all_supplier[ui.item.value];
        $("#add_supplier_row_company_code_exact").removeAttr("disabled");
		$("#add_supplier_company_name_exact").html(arr_cus_name_and_deb_nr[0]);

    }
}).click(function() {
	    $(this).autocomplete('search',' ');
}).keypress(function(){$("#add_supplier_row_company_code_exact").attr("disabled","disabled");});

function add_row_supplier(){
	var i=0; var check_add_row = true;
	var rowCount=table_add_supplier.column(0).data().length; 
    while(i<rowCount){
        var data_tr = table_add_supplier.row(i).data();
        if(data_tr[2]==$("#add_supplier_company_code_exact").val()){var check_add_row = false; alert("Cannot Add"); break; return false;}
        i++;
    }
	if(check_add_row==true){
	   row_node_setting_supplier=table_add_supplier.row.add(['',arr_get_all_supplier[$("#add_supplier_company_code_exact").val()][1],$("#add_supplier_company_code_exact").val(),$("#add_supplier_company_name_exact").html(),'<div align="center"><img class="delete_row_supplier" src="img/delete_row.png" width="30" onclick="" style="cursor:pointer;" height="30"></div>']).order( [[ 0, 'asc' ]] ).draw().node(); 
       $('#add_supplier_company_code_exact').val('');
       $("#add_supplier_row_company_code_exact").attr("disabled","disabled");
    }
}
function get_list_autocomplete_supplier(){
	$.ajax({
            url: "get_data/get_list_autocomplete_supplier.php",
            async: false,
            dataType: "json",
            type: "post",
            data: {"user_company_text":$("#table_add_supplier").attr('cmp-text'),"user_company_id":$("#table_add_supplier").attr('cmp-id')},
            success: function (result) {
            	console.log(result);
            	if(result!=undefined&&result[0]==true){
            		arr_get_all_supplier = result[1];
            		arr_autocomplete_supplier = result[2];
            	}else{
            		arr_get_all_supplier = null;
					arr_autocomplete_supplier = null;
            	}
            	
            }
    }); 
}

</script>
<style type="text/css">
#table_add_supplier{
    border:1px grey solid;
}
#table_add_supplier thead td{
    padding: 12px;
    background-color: #f6f6f6;
    border-bottom: 1px #adaead solid;
    /*font-weight: bold;*/
}
#table_add_supplier tbody td{
    padding: 8px;
    /*cursor:pointer;*/
    border-right:1px grey solid;
    /*border-bottom:1px grey solid;*/
}
img.delete_row_supplier{
    cursor: pointer;
    border:1px solid #fff;
        -moz-box-shadow: 10px 10px 5px #ccc;
        -webkit-box-shadow: 10px 10px 5px #ccc;
        box-shadow: 2px 2px 5px #ccc;
        -moz-border-radius:4px;
        -webkit-border-radius:2px;
        border-radius:4px;
}
</style>