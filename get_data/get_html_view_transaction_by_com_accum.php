<?php
require_once '../function.php';
$date_start = $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-01";
$date_last =  $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($date_start)),2,"0",STR_PAD_LEFT);
if($_POST['deb_crd_type']=="debnr"){
	$table_deb_crd = "ie_debtor";
}else{$table_deb_crd = "ie_creditor";}
$q_local = new Query_local();

$check_pl_bs = $q_local->query_table("select type from ie_grouping_gl where atid='".$_POST['gl_id']."'");
if(is_array($check_pl_bs)&&sizeof($check_pl_bs)>0){
	$type_gl = $check_pl_bs[0]['type'];
}else{
	$type_gl = NULL;
	exit();
}


if($_POST['role_td']=="left"){
	/*$str_q = "select izoom.* from ie_summary_lastest as isum 
inner join ie_zoom izoom on izoom.ie_summary_id = isum.atid 
where isum.company_id_user='".$_POST['head_com']."' and isum.company_id_mapping='".$_POST['this_com']."' 
and isum.year='".$_POST['year']."' and isum.month='".$_POST['month']."' 
and isum.ie_grouping_gl_id='".$_POST['gl_id']."'
order by izoom.atid asc
";*/
	$str_q= "select tblog.*
from
(
select ilog.revision,ilog.month,ilog.year,ilog.ie_grouping_gl_id,
ilog.company_id_user,ilog.company_id_mapping,
izoom.* 
from ie_summary_log as ilog 
inner join ie_zoom izoom on izoom.ie_summary_id = ilog.atid 
where ilog.company_id_user='".$_POST['head_com']."' and ilog.company_id_mapping='".$_POST['this_com']."' 
and ilog.year='".$_POST['year']."'
and (ilog.month =  ".$_POST['month'].") 
and ilog.ie_grouping_gl_id='".$_POST['gl_id']."' 
) as tblog
inner join (
	select max(maxilog.revision) as maxrev,maxilog.month,maxilog.year,
	maxilog.company_id_user,maxilog.company_id_mapping,maxilog.ie_grouping_gl_id
	from ie_summary_log as maxilog
	group by maxilog.month,maxilog.year,maxilog.company_id_user,maxilog.company_id_mapping,maxilog.ie_grouping_gl_id
) tbmaxrev on 
tblog.revision = tbmaxrev.maxrev 
and tblog.month = tbmaxrev.month
and tblog.year = tbmaxrev.year
and tblog.company_id_user = tbmaxrev.company_id_user
and tblog.company_id_mapping = tbmaxrev.company_id_mapping
and tblog.ie_grouping_gl_id = tbmaxrev.ie_grouping_gl_id";

	if($type_gl=="B/S"){
		$str_start_balance_text = "B/S เดือนปัจจุบัน(Ending) ";
		$str_start_balance = "select sum(isl.amount) as sum_amount,
  				from ie_summary_lastest  isl
  				where isl.company_id_user='".$_POST['head_com']."' and isl.company_id_mapping='".$_POST['this_com']."' and isl.ie_grouping_gl_id='".$_POST['gl_id']."'
  				and (isl.year=".$_POST['year']." and isl.month=".$_POST['month'].") ";
	}else{
			$str_start_balance_text = "P/L (จำนวนรวมตั้งแต่มกราคมในปีที่เลือก + เดือนปัจจุบัน)";
			$str_start_balance = "select SUM(amount) as sum_amount,(select amount from ie_summary_lastest isl where 
isl.company_id_user='".$_POST['head_com']."' and isl.company_id_mapping='".$_POST['this_com']."' and isl.ie_grouping_gl_id='".$_POST['gl_id']."' and isl.year=".$_POST['year']." and isl.month=".$_POST['month'].") as sum_this_month from ie_summary_lastest isl where 
isl.company_id_user='".$_POST['head_com']."' and isl.company_id_mapping='".$_POST['this_com']."' and isl.ie_grouping_gl_id='".$_POST['gl_id']."' and isl.year=".$_POST['year']." and isl.month<".$_POST['month']." ";
		
	}


}else{


$str_q= "select tblog.*
from
(
select ilog.revision,ilog.month,ilog.year,ilog.ie_grouping_gl_id,
ilog.company_id_user,ilog.company_id_mapping,
izoom.* 
from ie_summary_log as ilog 
inner join ie_zoom izoom on izoom.ie_summary_id = ilog.atid 
where ilog.company_id_user='".$_POST['this_com']."' and ilog.company_id_mapping='".$_POST['head_com']."' 
and ilog.year='".$_POST['year']."'
and (ilog.month =  ".$_POST['month'].") 
and ilog.ie_grouping_gl_id='".$_POST['gl_id']."' 
) as tblog
inner join (
	select max(maxilog.revision) as maxrev,maxilog.month,maxilog.year,
	maxilog.company_id_user,maxilog.company_id_mapping,maxilog.ie_grouping_gl_id
	from ie_summary_log as maxilog
	group by maxilog.month,maxilog.year,maxilog.company_id_user,maxilog.company_id_mapping,maxilog.ie_grouping_gl_id
) tbmaxrev on 
tblog.revision = tbmaxrev.maxrev 
and tblog.month = tbmaxrev.month
and tblog.year = tbmaxrev.year
and tblog.company_id_user = tbmaxrev.company_id_user
and tblog.company_id_mapping = tbmaxrev.company_id_mapping
and tblog.ie_grouping_gl_id = tbmaxrev.ie_grouping_gl_id";

  if($type_gl=="B/S"){
		$str_start_balance_text = "B/S เดือนปัจจุบัน(Ending) ";
		$str_start_balance = "select sum(isl.amount) as sum_amount,
  				from ie_summary_lastest  isl
  				where isl.company_id_user='".$_POST['this_com']."' and isl.company_id_mapping='".$_POST['head_com']."' and isl.ie_grouping_gl_id='".$_POST['gl_id']."'
  				and (isl.year=".$_POST['year']." and isl.month=".$_POST['month'].") ";
	}else{
			$str_start_balance_text = "P/L (จำนวนรวมตั้งแต่มกราคมในปีที่เลือก + เดือนปัจจุบัน)";
			$str_start_balance = "select SUM(amount) as sum_amount,(select amount from ie_summary_lastest isl where 
isl.company_id_user='".$_POST['this_com']."' and isl.company_id_mapping='".$_POST['head_com']."' and isl.ie_grouping_gl_id='".$_POST['gl_id']."' and isl.year=".$_POST['year']." and isl.month=".$_POST['month'].") as sum_this_month from ie_summary_lastest isl where 
isl.company_id_user='".$_POST['this_com']."' and isl.company_id_mapping='".$_POST['head_com']."' and isl.ie_grouping_gl_id='".$_POST['gl_id']."' and isl.year=".$_POST['year']." and isl.month<".$_POST['month']." ";	
	}
}
//echo $str_q;
//echo $str_start_balance;
$arr_trans = $q_local->query_table($str_q);
if(!is_array($arr_trans)){echo "error"; exit();}
else{
	$size_arr_trans = sizeof($arr_trans); $i=0; 
	?>
	<table style="font-size:12px;" border="1" id="table_trans_inner_view_by_com"  width="100%">
	<thead style="background-color:#fafafa;border-bottom:2px solid;">
		<tr>
			<td>glcode</td>
			<td>entry_no</td>
			<td>ourref</td>
			<td>yourref</td>
			<td>des</td>
			<td>itemcode</td>
			<td>item_des</td>
			<td>Amount</td>
			<td>debnr</td>
			<td>deb_name</td>
			<td>crdnr</td>
			<td>crd_name</td>
			<td>so</td>
		</tr>
	</thead>
	<tbody>
	<?php
	$sum_amount = NULL;
	while($i<$size_arr_trans){
		$arr_in_trans = $arr_trans[$i];
		echo '<tr>';
		echo '<td>'.$arr_in_trans["glcode"].'</td>';
		echo '<td>'.$arr_in_trans["entry_no"].'</td>';
		echo '<td>'.$arr_in_trans["ourref"].'</td>';
		echo '<td>'.$arr_in_trans["yourref"].'</td>';
		echo '<td>'.$arr_in_trans["des"].'</td>';
		echo '<td>'.$arr_in_trans["itemcode"].'</td>';
		echo '<td>'.$arr_in_trans["item_des"].'</td>';
			if(($arr_in_trans["qty"]!=NULL&&$arr_in_trans["qty"]!="")){
				echo '<td>'.number_format($arr_in_trans["qty"],2).'</td>';
			}else{
				echo '<td></td>';
			}
		echo '<td>'.$arr_in_trans["debnr"].'</td>';
		echo '<td>'.$arr_in_trans["deb_name"].'</td>';
		echo '<td>'.$arr_in_trans["crdnr"].'</td>';
		echo '<td>'.$arr_in_trans["crd_name"].'</td>';
		echo '<td>'.$arr_in_trans["so"].'</td>';
		echo '</tr>';
		$sum_amount+=$arr_in_trans["qty"];
		$i++;
	}
	echo '<tr><td colspan="13"> Sum This Month : '.number_format($sum_amount,2).'</td></tr>';
	//echo $str_start_balance;
	$start_balance = $q_local->query_table($str_start_balance);
	//print_r($start_balance);
	if(is_array($start_balance)&&sizeof($start_balance)>0){
		
		if($type_gl=="B/S"){
			$num_sum_amount = $start_balance[0]['sum_amount'];
			echo '<tr><td colspan="13">'.$str_start_balance_text.' : '.number_format($num_sum_amount,2).' </td></tr>';
		}else{
			$num_sum_amount = $start_balance[0]['sum_amount'];
			$num_sum_this_month = $start_balance[0]['sum_this_month'];
			echo '<tr><td colspan="13">'.$str_start_balance_text.' : '.number_format($num_sum_amount,2).' + ('.number_format($num_sum_this_month,2).') = '.number_format(($num_sum_amount+$num_sum_this_month),2).' </td></tr>';
		}
		
	}
	?>
	</tbody>
	</table>
	
	<?php
}
?>
<style type="text/css">
#table_trans_inner_view_by_com{
    border:1px grey solid;
}
#table_trans_inner_view_by_com thead td{
    padding: 12px;
    background-color: #F5F5F5;
    /*font-weight: bold;*/
}
#table_trans_inner_view_by_com tbody td{
    padding: 8px;
    cursor:pointer;
    border-right:1px grey solid;
}
#table_trans_inner_view_by_com tfoot td{
    padding: 8px;
    border-right:1px grey solid;
}
</style>