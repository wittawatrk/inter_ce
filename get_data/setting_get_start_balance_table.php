<?php
require_once '../function.php';
$class_q_local = new Query_local();

           // echo '<table id="header-fixed"></table>';
            $arr_all_cmp = $class_q_local->query_table("select * from  company order by order_no asc");
            echo '<table cellspacing="0" id="table_show_start_bs" style="width:100%;border-collapse: collapse;">';
            //echo '<thead>';
            echo '<tr class="GridviewScrollHeader">';
            echo '<th scope="col" >Type(Group)</th>'; echo '<th scope="col" >GL ('.$_POST["user_company_code"].')</th>';
            $i=0; $array_other_company = array(); $array_this_company = NULL; $count_other_company=0;
            while($i<sizeof($arr_all_cmp)){
                $data_s = $arr_all_cmp[$i];
                if($_POST['user_company_id']==$data_s['company_id']){
                    $array_this_company = array("company_id"=>$data_s['company_id'],"company_code"=>$data_s['company_code']);
                }
                else{
                    array_push($array_other_company,array($data_s['company_id'],$data_s['company_code']));
                    echo '<th style="min-width:150px;" scope="col" align="center">'.$data_s['company_code'].'</th>';
                    $count_other_company++;
                }

                $i++;
            }
            echo '<th scope="col" style="width:300px">Sum By GL</th>';
            echo '</tr>';
           // echo '</thead>';
           // echo '<tbody>';
            $arr_all_gl = $class_q_local->query_table("select * from ie_grouping_gl order by group_no asc,orderg asc,atid asc");
            $j=0;
            while($j<sizeof($arr_all_gl)){
                $gl_id = $arr_all_gl[$j]['atid'];
                $group_no = $arr_all_gl[$j]['group_no'];
                echo '<tr class="GridviewScrollItem">';
                echo '<td>'.$arr_all_gl[$j]['type'].' ('.$arr_all_gl[$j]['group_no'].')</td>';
                echo '<td>'.$arr_all_gl[$j]['name'].'</td>';
                    $k=0; $sum_by_gl = 0;
                    while($k<$count_other_company){
                        $com_id_and_code = $array_other_company[$k];

                        $query_value = "select * from ie_grouping_gl_open_amount where company_id='".$_POST["user_company_id"]."' and company_id_mapping='".$com_id_and_code[0]."' and ie_grouping_id='".$gl_id."'";

                        $arr_value_balance = $class_q_local->query_table($query_value);
                        if(is_array($arr_value_balance)&&sizeof($arr_value_balance)>0){
                            if($arr_value_balance[0]['open_amount']!=NULL){
                                $default_balance = number_format($arr_value_balance[0]['open_amount'],2);
                                $sum_by_gl+=$arr_value_balance[0]['open_amount'];
                            }else{$default_balance = "";}
                            $default_id = $arr_value_balance[0]['atid'];
                        }else{$default_balance=""; $default_id = "";}

                        echo '<td>';
                            echo '<input type="text" head-com-id="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com-id="'.$com_id_and_code[0].'" this-com-code="'.$com_id_and_code[1].'" gl-id="'.$gl_id.'" old-id-balance="'.$default_id.'" value="'.$default_balance.'" class="input_balance">';
                        echo '</td>';
                        $k++;
                    }
                    echo '<td >'.number_format($sum_by_gl,2).'</td>';

                echo '</tr>';
                $j++;
            }
          //  echo '</tbody>';
            echo '</table>';
?>
<style>

.input_balance{
    border:none;
    width:99%;
    text-align: left;
    font-size: 90%;
}
.img_zoom_trans{
    cursor:pointer;
    width: 20px;
    height: 20px;
}
.span_total{
    text-align: right;
}
.GridviewScrollHeader TH, .GridviewScrollHeader TD
{
    padding: 5px;
    font-weight: bold;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #EFEFEF;
    text-align: center;
    vertical-align: middle;
}
.GridviewScrollItem TD
{
    padding: 5px;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #FFFFFF;
}
.GridviewScrollPager
{
    border-top: 1px solid #AAAAAA;
    background-color: #FFFFFF;
}
.GridviewScrollPager TD
{
    padding-top: 3px;
    font-size: 14px;
    padding-left: 5px;
    padding-right: 5px;
}
.GridviewScrollPager A
{
    color: #666666;
}
.GridviewScrollPager SPAN

{

    font-size: 16px;

    font-weight: bold;

}
</style>
