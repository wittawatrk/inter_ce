<?php
require_once '../function.php';
  // $_POST['user_company_code']="CI";
  // $_POST['user_company_id']="3";
  // $_POST['year']="2016";
  // $_POST['month']="1";
function get_q_value_left($type_gl,$gl_id,$com_user,$com_map,$year,$month,$year_start,$year_end,$month_start,$month_end){
    if($type_gl=="B/S"){  /// B/S ไม่ SUM ช่วงให้ เพราะเป็นยอดปิด ไม่งั้นจะผิด
        $str_return_q = "select (isl.amount) as amount
  from ie_summary_lastest  isl
  where isl.company_id_user='".$com_user."' and isl.company_id_mapping='".$com_map."' and isl.ie_grouping_gl_id='".$gl_id."'
  and (isl.year=".$year." and isl.month=".$month_end.")";
    }else{ /// P/L จะ SUM ช่วงให้
        $str_return_q = "select
    ISNULL(SUM(CASE WHEN DateSearch>='".$year_start."-".str_pad($month_start,2,'0',STR_PAD_LEFT)."-01' AND DateSearch<= '".$year_end."-".str_pad($month_end,2,'0',STR_PAD_LEFT)."-01' THEN amount ELSE 0 END),0) as amount
    FROM ( select (CAST(LTRIM(RTRIM(year)) AS VARCHAR(4)))+'-'+RIGHT('0'+CAST(LTRIM(RTRIM(month)) AS VARCHAR(2)),2)+'-01' as DateSearch,amount from ie_summary_lastest where company_id_user='".$com_user."' and company_id_mapping='".$com_map."' and ie_grouping_gl_id='".$gl_id."' ) tb ";
    }
    return $str_return_q;
}
function get_q_value_right($type_gl,$gl_id,$com_user,$com_map,$year,$month,$year_start,$year_end,$month_start,$month_end){
    if($type_gl=="B/S"){  /// B/S ไม่ SUM ช่วงให้ เพราะเป็นยอดปิด ไม่งั้นจะผิด
        $str_return_q = "select (isl.amount) as amount
  from ie_summary_lastest  isl
  where isl.company_id_user='".$com_map."' and isl.company_id_mapping='".$com_user."' and isl.ie_grouping_gl_id='".$gl_id."'
  and (isl.year=".$year." and isl.month=".$month_end.")";
    }else{ /// P/L จะ SUM ช่วงให้
        $str_return_q = "select
    ISNULL(SUM(CASE WHEN DateSearch>='".$year_start."-".str_pad($month_start,2,'0',STR_PAD_LEFT)."-01' AND DateSearch<= '".$year_end."-".str_pad($month_end,2,'0',STR_PAD_LEFT)."-01' THEN amount ELSE 0 END),0) as amount
    FROM ( select (CAST(LTRIM(RTRIM(year)) AS VARCHAR(4)))+'-'+RIGHT('0'+CAST(LTRIM(RTRIM(month)) AS VARCHAR(2)),2)+'-01' as DateSearch,amount from ie_summary_lastest where company_id_user='".$com_map."' and company_id_mapping='".$com_user."' and ie_grouping_gl_id='".$gl_id."' ) tb ";
    }
    return $str_return_q;
}


if($_POST['check_type_filter']=="1"){
    $date_start = $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-01";
    $date_last =  $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($date_start)),2,"0",STR_PAD_LEFT);
    $month_start = $_POST['month']; $year_start = $_POST['year'];
    $month_end = $_POST['month']; $year_end = $_POST['year'];
    $year=$_POST['year'];
    $month=$_POST['month'];

}else{
    $month_start = $_POST['month_start']; $year_start = $_POST['year'];
    $month_end = $_POST['month_end']; $year_end = $_POST['year'];
    $year=$_POST['year'];
    $month=$_POST['month'];
}
if(isset($_POST['array_company_hidden'])&&sizeof($_POST['array_company_hidden'])>0){
    $str_hidden_com = NULL; $count_hidden_com = 0;
    $i=0; $size_com_hidden = sizeof($_POST['array_company_hidden']);
    while($i<$size_com_hidden){
        if($i==0){$str_hidden_com.=" where c.company_id='".$_POST['array_company_hidden'][$i]."'";}
        else if($i>0){
            $str_hidden_com.=" or c.company_id='".$_POST['array_company_hidden'][$i]."' ";
        }
        $i++;
    }

}else{$str_hidden_com = NULL;}

$class_q_local = new Query_local();
$str_q_local = "select c.*,
(select top 1 status_last from ie_summary_lastest where
company_id_user=c.company_id and year=".$year." and month=".$month.")
as status_last from  company c ".$str_hidden_com."
order by order_no asc";
$arr_all_cmp = $class_q_local->query_table($str_q_local);

//echo $str_q_local;
echo '<table id="table_show_compare" style="width:100%;border-collapse: collapse;">';
echo '<tr class="GridviewScrollHeader">';
echo '<th rowspan="2" scope="col">Type</th>'; echo '<th rowspan="2" scope="col">GL ('.$_POST['user_company_code'].')</th>';
$i=0; $array_other_company = array(); $array_this_company = NULL; $count_other_company=0;
while($i<sizeof($arr_all_cmp)){
    $data_s = $arr_all_cmp[$i];
    if($_POST['user_company_id']==$data_s['company_id']){
        $array_this_company = array("company_id"=>$data_s['company_id'],"company_code"=>$data_s['company_code']);
    }
    else{
        array_push($array_other_company,array($data_s['company_id'],$data_s['company_code']));
      
        if($data_s['status_last']=="1"){$status_last = " (CONFIRM)";}else{$status_last=NULL;}
        echo '<td colspan="2" align="center">'.$data_s['company_code'].$status_last.'</td>';
        $count_other_company++;
        $cmp[]= $data_s['company_code'];
    }
    $i++;
}
echo '<td colspan="2" align="center">Total GL</td>';
echo '<th rowspan="2" scope="col" align="center">DIFF</th>';
echo '</tr>';
echo '<tr class="GridviewScrollHeader">';
$i=0;
while($i<(($count_other_company)*2)){
  if($i%2==0){
    echo '<th scope="col">'.$_POST['user_company_code'].'</th>';
  }else{

    echo '<th scope="col">'.$cmp[(int)($i/2)].'</th>';


  }

  $i++;
}
echo '<th scope="col"></th>';
echo '<th scope="col"></th>';
echo '</tr>';


$arr_all_gl = $class_q_local->query_table("select * from ie_grouping_gl where group_no!='23' order by group_no asc,orderg asc,atid asc");
$j=0;
$array_com_sum_total_left = array(); $array_com_sum_total_right = array();
$array_com_sum_grand_total_left = array(); $array_com_sum_grand_total_right = array();
while($j<sizeof($arr_all_gl)){
    $gl_id = $arr_all_gl[$j]['atid'];
    $group_no = $arr_all_gl[$j]['group_no'];
    $condition_field_gl = $arr_all_gl[$j]['condition_field'];
    $type_gl =$arr_all_gl[$j]['type'];
    $type_gl_show =$arr_all_gl[$j]['type_show'];
    echo '<tr class="GridviewScrollItem">';
    echo '<td>'.$type_gl_show.' ('.$arr_all_gl[$j]['group_no'].')</td>';
    echo '<td>'.$arr_all_gl[$j]['name'].'</td>';
    $k=0;
    $sum_total_gl_left = NULL; $sum_total_gl_right = NULL;
    while($k<$count_other_company){ //// loop right side by company in this gl
        $com_id_and_code = $array_other_company[$k];
        if(!isset($array_com_sum_total_left[$com_id_and_code[0]])){$array_com_sum_total_left[$com_id_and_code[0]]=NULL;}
        if(!isset($array_com_sum_total_right[$com_id_and_code[0]])){$array_com_sum_total_right[$com_id_and_code[0]]=NULL;}

        $arr_q_row_left=NULL; $arr_q_row_right=NULL;
        $arr_q_row_left=$class_q_local->query_table(get_q_value_left($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end));
        $arr_q_row_right=$class_q_local->query_table(get_q_value_right($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end));

        	if(is_array($arr_q_row_left)&&sizeof($arr_q_row_left)>0){
               // echo get_q_value_left($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end);
                $num_left = $arr_q_row_left[0]['amount'];
            }
            else{
                $num_left=NULL;
            }

        	if(is_array($arr_q_row_right)&&sizeof($arr_q_row_right)>0){
                $num_right = $arr_q_row_right[0]['amount'];
            }else{
                $num_right=NULL;
            }
        if($num_left!=$num_right){$normal_or_blink="blink";}else{$normal_or_blink="normal";}
        /////////////////////////////////////////////////////////////////////////////// TD data

		echo '<td  style="min-width:150px;">';
        //echo get_q_value_left($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end);
            if($num_left!=NULL){echo '<span span-side="left" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$com_id_and_code[0].'" gl-id="'.$gl_id.'" deb-crd-type="'.$condition_field_gl.'" status="'.$normal_or_blink.'" style="padding:5px;">'.number_format($num_left,2).'</span>'; $sum_total_gl_left+=$num_left; $array_com_sum_total_left[$com_id_and_code[0]]+=$num_left;}
        echo '</td>';
            //////////////////////////////////////////////////////////////////////////////// Between Left and Right
            	//if($num_left!=NULL||$num_right!=NULL){echo '<font color="RED" size="3">|</font>';}
            //////////////////////////////////////////////////////////////////////////////// Between Left and Right
        echo '<td title="'.get_q_value_right($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end).'" style="min-width:150px;">';
            if($num_right!=NULL){echo '<span span-side="right" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$com_id_and_code[0].'" gl-id="'.$gl_id.'" deb-crd-type="'.$condition_field_gl.'" status="'.$normal_or_blink.'" style="padding:5px;">'.number_format($num_right,2).'</span>'; $sum_total_gl_right+=$num_right; $array_com_sum_total_right[$com_id_and_code[0]]+=$num_right;}
        echo '</td>';
        /////////////////////////////////////////////////////////////////////////////// End TD data
        $k++;
        if($k==$count_other_company){ /// Total GL&Diff
        	echo '<td style="min-width:150px;">';
        		echo ($sum_total_gl_left!=NULL)?number_format($sum_total_gl_left,2):NULL;

            echo '</td>';
            echo '<td style="min-width:150px;">';
        		echo ($sum_total_gl_right!=NULL)?number_format($sum_total_gl_right,2):NULL;
        	echo '</td>';
    		echo '<td style="min-width:150px;">';

    			echo ($sum_total_gl_left!=NULL||$sum_total_gl_right!=NULL)?number_format(($sum_total_gl_left+$sum_total_gl_right),2):NULL;
    		echo '</td>';
        }
    }
    echo '</tr>';
    $echo_total = false;
    if(isset($arr_all_gl[$j+1]['group_no'])){
        $check_next_group = $arr_all_gl[$j+1]['group_no'];
        if($check_next_group!=$group_no){$echo_total = true;}else{$echo_total = false;}
    }else{$echo_total = true;}
    if($echo_total==true){
        echo '<tr  class="GridviewScrollItem">';
            echo '<td style="border-right:#EFEFEF;background-color:#EFEFEF;">';
            echo '</td>';
            echo '<td align="right" style="border-left:#EFEFEF;background-color:#EFEFEF;">Total Group '.$group_no.' :';
            echo '</td>';
    		$y=0;
    		$sum_total_of_total_gl_left = NULL; $sum_total_of_total_gl_right = NULL;
    		while($y<$count_other_company){
    			$com_id_and_code_total = $array_other_company[$y];
    			if(!isset($array_com_sum_grand_total_left[$com_id_and_code_total[0]])){$array_com_sum_grand_total_left[$com_id_and_code_total[0]]=NULL;}
        		if(!isset($array_com_sum_grand_total_right[$com_id_and_code_total[0]])){$array_com_sum_grand_total_right[$com_id_and_code_total[0]]=NULL;}
    			$total_left = $array_com_sum_total_left[$com_id_and_code_total[0]];
    			$total_right = $array_com_sum_total_right[$com_id_and_code_total[0]];
    			echo '<td style="background-color:#EFEFEF;">';
    				if($total_left!=NULL){echo '<span style="padding:5px;">'.number_format($total_left,2).'</span>'; $sum_total_of_total_gl_left+=$total_left; $array_com_sum_grand_total_left[$com_id_and_code_total[0]]+=$total_left;}
    			echo '</td>';

    			echo '<td style="background-color:#EFEFEF;">';
                    if($total_right!=NULL){echo '<span style="padding:5px;">'.number_format($total_right,2).'</span>'; $sum_total_of_total_gl_right+=$total_right; $array_com_sum_grand_total_right[$com_id_and_code_total[0]]+=$total_right;}
    			echo '</td>';
    			$array_com_sum_total_left[$com_id_and_code_total[0]]=NULL;
    			$array_com_sum_total_right[$com_id_and_code_total[0]]=NULL;
    			$y++;
    			if($y==$count_other_company){ ///////// total of total GL && DIF
    				echo '<td style="background-color:#EFEFEF;">';
    					echo ($sum_total_of_total_gl_left!=NULL)?number_format($sum_total_of_total_gl_left,2):NULL;
		        	echo '</td>';
                        //echo ($sum_total_of_total_gl_left!=NULL||$sum_total_of_total_gl_right!=NULL)?'<font color="RED" size="3">|</font>':NULL;
		        	echo '<td style="background-color:#EFEFEF;">';
                        echo ($sum_total_of_total_gl_right!=NULL)?number_format($sum_total_of_total_gl_right,2):NULL;
		    		echo '</td>';
		    		echo '<td style="background-color:#EFEFEF;">';
		    			echo ($sum_total_of_total_gl_left!=NULL||$sum_total_of_total_gl_right!=NULL)?number_format(($sum_total_of_total_gl_left+$sum_total_of_total_gl_right),2):NULL;
		    		echo '</td>';
    			}
    		}
    	echo '</tr>';
    }
    //////////////////////////////////////////////////////// Grand Total
    if(!isset($arr_all_gl[$j+1]['group_no'])){
    	echo '<tr class="GridviewScrollItem">';
    		echo '<td style="border-right:#CCCCCC;background-color:#CCCCCC;">';
    		echo '</td>';
    		echo '<td align="right" style="border-left:#CCCCCC;background-color:#CCCCCC;">Grand Total :';
    		echo '</td>';
    		$z=0;
    		$sum_total_of_grand_total_gl_left = NULL; $sum_total_of_grand_total_gl_right = NULL;
    		while($z<$count_other_company){
    			$com_id_and_code_grand_total = $array_other_company[$z];

    			$grand_total_left = $array_com_sum_grand_total_left[$com_id_and_code_grand_total[0]];
    			$grand_total_right = $array_com_sum_grand_total_right[$com_id_and_code_grand_total[0]];
    			echo '<td style="background-color:#CCCCCC;">';
    				if($grand_total_left!=NULL){echo '<span style="padding:5px;">'.number_format($grand_total_left,2).'</span>'; $sum_total_of_grand_total_gl_left+=$grand_total_left;}
    			echo '</td>';
                	//if($grand_total_left!=NULL||$grand_total_right!=NULL){echo '<font color="RED" size="3">|</font>';}
    			echo '<td style="background-color:#CCCCCC;">';
                    if($grand_total_right!=NULL){echo '<span style="padding:5px;">'.number_format($grand_total_right,2).'</span>'; $sum_total_of_grand_total_gl_right+=$grand_total_right;}
    			echo '</td>';
    			$z++;
    			if($z==$count_other_company){ ///////// total of total GL && DIF
    				echo '<td style="background-color:#CCCCCC;">';
    					echo ($sum_total_of_grand_total_gl_left!=NULL)?number_format($sum_total_of_grand_total_gl_left,2):NULL;
		        	echo '</td>';
                        //echo ($sum_total_of_grand_total_gl_left!=NULL||$sum_total_of_grand_total_gl_right!=NULL)?'<font color="RED" size="3">|</font>':NULL;
		        	echo '<td style="background-color:#CCCCCC;">';
                        echo ($sum_total_of_grand_total_gl_right!=NULL)?number_format($sum_total_of_grand_total_gl_right,2):NULL;
		    		echo '</td>';
		    		echo '<td style="background-color:#CCCCCC;">';
		    			echo ($sum_total_of_grand_total_gl_left!=NULL||$sum_total_of_grand_total_gl_right!=NULL)?number_format(($sum_total_of_grand_total_gl_left+$sum_total_of_grand_total_gl_right),2):NULL;
		    		echo '</td>';
    			}
    		}
    }
    $j++;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////// Group 23


$arr_all_gl = $class_q_local->query_table("select * from ie_grouping_gl where group_no='23' order by group_no asc,atid asc");
$j=0;
$array_com_sum_total_left = array(); $array_com_sum_total_right = array();
$array_com_sum_grand_total_left = array(); $array_com_sum_grand_total_right = array();
while($j<sizeof($arr_all_gl)){
    $gl_id = $arr_all_gl[$j]['atid'];
    $group_no = $arr_all_gl[$j]['group_no'];
    $condition_field_gl = $arr_all_gl[$j]['condition_field'];
    $type_gl =$arr_all_gl[$j]['type'];
    $type_gl_show =$arr_all_gl[$j]['type_show'];
    echo '<tr class="GridviewScrollItem">';
    echo '<td>'.$type_gl_show.' ('.$arr_all_gl[$j]['group_no'].')</td>';
    echo '<td>'.$arr_all_gl[$j]['name'].'</td>';
    $k=0;
    $sum_total_gl_left = NULL; $sum_total_gl_right = NULL;
    while($k<$count_other_company){ //// loop right side by company in this gl
        $com_id_and_code = $array_other_company[$k];
        if(!isset($array_com_sum_total_left[$com_id_and_code[0]])){$array_com_sum_total_left[$com_id_and_code[0]]=NULL;}
        if(!isset($array_com_sum_total_right[$com_id_and_code[0]])){$array_com_sum_total_right[$com_id_and_code[0]]=NULL;}

        $arr_q_row_left=NULL; $arr_q_row_right=NULL;
        $arr_q_row_left=$class_q_local->query_table(get_q_value_left($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end));
        $arr_q_row_right=$class_q_local->query_table(get_q_value_right($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end));

            if(is_array($arr_q_row_left)&&sizeof($arr_q_row_left)>0){
                $num_left = $arr_q_row_left[0]['amount'];
            }
            else{
                $num_left=NULL;
            }

            if(is_array($arr_q_row_right)&&sizeof($arr_q_row_right)>0){
                $num_right = $arr_q_row_right[0]['amount'];
            }else{
                $num_right=NULL;
            }
        if($num_left!=$num_right){$normal_or_blink="blink";}else{$normal_or_blink="normal";}
        /////////////////////////////////////////////////////////////////////////////// TD data

        echo '<td title="'.get_q_value_left($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end). '" style="min-width:150px;">';

            if($num_left!=NULL){echo '<span span-side="left" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$com_id_and_code[0].'" gl-id="'.$gl_id.'" deb-crd-type="'.$condition_field_gl.'" status="'.$normal_or_blink.'" style="padding:5px;">'.number_format($num_left,2).'</span>'; $sum_total_gl_left+=$num_left; $array_com_sum_total_left[$com_id_and_code[0]]+=$num_left;}
        echo '</td>';
            //////////////////////////////////////////////////////////////////////////////// Between Left and Right

            //////////////////////////////////////////////////////////////////////////////// Between Left and Right
        echo '<td  title="'.get_q_value_right($type_gl,$gl_id,$_POST['user_company_id'],$com_id_and_code[0],$year,$month,$year_start,$year_end,$month_start,$month_end).'"  style="min-width:150px;">';
            if($num_right!=NULL){echo '<span span-side="right" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$com_id_and_code[0].'" gl-id="'.$gl_id.'" deb-crd-type="'.$condition_field_gl.'" status="'.$normal_or_blink.'" style="padding:5px;">'.number_format($num_right,2).'</span>'; $sum_total_gl_right+=$num_right; $array_com_sum_total_right[$com_id_and_code[0]]+=$num_right;}
        echo '</td>';
        /////////////////////////////////////////////////////////////////////////////// End TD data
        $k++;
        if($k==$count_other_company){ /// Total GL&Diff
            echo '<td style="min-width:150px;">';
                echo ($sum_total_gl_left!=NULL)?number_format($sum_total_gl_left,2):NULL;

            echo '</td>';
            echo '<td style="min-width:150px;">';
                echo ($sum_total_gl_right!=NULL)?number_format($sum_total_gl_right,2):NULL;
            echo '</td>';
            echo '<td style="min-width:150px;">';
                //echo $sum_total_gl_right;
                echo ($sum_total_gl_left!=NULL||$sum_total_gl_right!=NULL)?number_format(($sum_total_gl_left+$sum_total_gl_right),2):NULL;
            echo '</td>';
        }
    }
    echo '</tr>';
    $echo_total = false;
    if(isset($arr_all_gl[$j+1]['group_no'])){
        $check_next_group = $arr_all_gl[$j+1]['group_no'];
        if($check_next_group!=$group_no){$echo_total = true;}else{$echo_total = false;}
    }else{$echo_total = true;}
    if($echo_total==true){
        echo '<tr  class="GridviewScrollItem">';
            echo '<td style="border-right:#EFEFEF;background-color:#EFEFEF;">';
            echo '</td>';
            echo '<td align="right" style="border-left:#EFEFEF;background-color:#EFEFEF;">Total Group '.$group_no.' :';
            echo '</td>';
            $y=0;
            $sum_total_of_total_gl_left = NULL; $sum_total_of_total_gl_right = NULL;
            while($y<$count_other_company){
                $com_id_and_code_total = $array_other_company[$y];
                if(!isset($array_com_sum_grand_total_left[$com_id_and_code_total[0]])){$array_com_sum_grand_total_left[$com_id_and_code_total[0]]=NULL;}
                if(!isset($array_com_sum_grand_total_right[$com_id_and_code_total[0]])){$array_com_sum_grand_total_right[$com_id_and_code_total[0]]=NULL;}
                $total_left = $array_com_sum_total_left[$com_id_and_code_total[0]];
                $total_right = $array_com_sum_total_right[$com_id_and_code_total[0]];
                echo '<td style="background-color:#EFEFEF;">';
                    if($total_left!=NULL){echo '<span style="padding:5px;">'.number_format($total_left,2).'</span>'; $sum_total_of_total_gl_left+=$total_left; $array_com_sum_grand_total_left[$com_id_and_code_total[0]]+=$total_left;}
                echo '</td>';

                echo '<td style="background-color:#EFEFEF;">';
                    if($total_right!=NULL){echo '<span style="padding:5px;">'.number_format($total_right,2).'</span>'; $sum_total_of_total_gl_right+=$total_right; $array_com_sum_grand_total_right[$com_id_and_code_total[0]]+=$total_right;}
                echo '</td>';
                $array_com_sum_total_left[$com_id_and_code_total[0]]=NULL;
                $array_com_sum_total_right[$com_id_and_code_total[0]]=NULL;
                $y++;
                if($y==$count_other_company){ ///////// total of total GL && DIF
                    echo '<td style="background-color:#EFEFEF;">';
                        echo ($sum_total_of_total_gl_left!=NULL)?number_format($sum_total_of_total_gl_left,2):NULL;
                    echo '</td>';

                    echo '<td style="background-color:#EFEFEF;">';
                        echo ($sum_total_of_total_gl_right!=NULL)?number_format($sum_total_of_total_gl_right,2):NULL;
                    echo '</td>';
                    echo '<td style="background-color:#EFEFEF;">';
                        echo ($sum_total_of_total_gl_left!=NULL||$sum_total_of_total_gl_right!=NULL)?number_format(($sum_total_of_total_gl_left+$sum_total_of_total_gl_right),2):NULL;
                    echo '</td>';
                }
            }
        echo '</tr>';
    }
    $j++;
}

echo '</table>';
?>
<script type="text/javascript">
blink('span[status=blink]');
function blink(selector){
	/*$(selector).fadeOut(1500, function(){
	    $(this).fadeIn(1500, function(){
	        blink(this);
	    });
	});*/
    $(selector).css("background-color","#FFE4E1");
}
</script>
<style type="text/css">
.td_total{
	font-size: 80%;
}
.td_to_compare_trans,.td_total,.sum_by_gl_left,.sum_by_gl_right,.sum_by_gl_diff{
	font-size: 90%;
	word-wrap: break-word;
    overflow-wrap: break-word;
}
.td_to_compare_trans_blink{
	font-size: 90%;
	background-color: #FFE4E1;
	word-wrap: break-word;
    overflow-wrap: break-word;
}
.GridviewScrollHeader TH, .GridviewScrollHeader TD
{
    padding: 5px;
    font-weight: bold;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #EFEFEF;
    text-align: center;
    vertical-align: middle;
}
.GridviewScrollItem TD
{
    padding: 5px;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #FFFFFF;
    font-size: 90%;
}
.GridviewScrollPager
{
    border-top: 1px solid #AAAAAA;
    background-color: #FFFFFF;
}
.GridviewScrollPager TD
{
    padding-top: 3px;
    font-size: 14px;
    padding-left: 5px;
    padding-right: 5px;
}
.GridviewScrollPager A
{
    color: #666666;
}
.GridviewScrollPager SPAN

{

    font-size: 16px;

    font-weight: bold;

}
</style>
