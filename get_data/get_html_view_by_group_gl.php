﻿<?php
require_once '../function.php';
//$_POST['user_company_code']="CI";
//$_POST['user_company_id']="3";
//$_POST['year']="2016";
//$_POST['month']="1";


function check_map_gl($q_local,$group_id_gl,$company_id_user){
	$check_map_gl =  $q_local->query_table("select atid from ie_grouping_gl_map where ie_grouping_id='".$group_id_gl."' and company_id='".$company_id_user."'");
	if(sizeof($check_map_gl)>0){return true;}else{return false;}
}
function check_map_deb_crd($q_local,$condition_field_gl,$company_id_user,$td_company){
	if($condition_field_gl=="debnr"){
		$q_check_map_deb_crd = "select atid from ie_debtor where company_id_user='".$company_id_user."' and company_id_mapping='".$td_company."'";
	}else{
		$q_check_map_deb_crd = "select atid from ie_creditor where company_id_user='".$company_id_user."' and company_id_mapping='".$td_company."'";
	}
	$check_map_deb_crd = $q_local->query_table($q_check_map_deb_crd);
	if(sizeof($check_map_deb_crd)>0){return true;}else{return false;}
}
function get_query_str_check($type_erp,$db_name,$gl_id,$condition_field_gl,$table_condition,$type_gl,$this_year,$this_month,$date_start,$date_last,$head_com,$this_com,$head_com_code){
	$query_row = NULL;

	if($type_erp=="openerp"){

		if($type_gl!="B/S"){ /// พวก P/L ให้ดึงตามเดือนปกติเลย
			$plus_query = "set @start_balance = 0";
		}else{
			///////////////// type B/S
			if($this_year=="2018"&&$this_month=="1"){ ///////////////////// This month + Start Balance
				$plus_query = " set @start_balance = (ISNULL((select ISNULL(open_amount,0) from [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_open_amount] where company_id=".$head_com." and company_id_mapping=".$this_com." and ie_grouping_id=".$gl_id." and row_active=1),0)) ";
			}
			else{////////////////////////////////////////////////////////// This month + Sum month < this month in table lastest
				$plus_query = " set @start_balance = (ISNULL((select top 1 ISNULL(amount,0) from [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_summary_lastest] where company_id_user=".$head_com." and company_id_mapping=".$this_com." and ie_grouping_gl_id=".$gl_id." and ((year=".$this_year." and month<".$this_month.") or (year<".$this_year.")) order by year desc,month desc,revision desc ),0)) ";
			}
		}

			$query_row = "
			DECLARE @start_balance float;
			DECLARE @value_erp float;
			".$plus_query."
			set @value_erp= (ISNULL((select ISNULL(SUM(op.debit-op.credit),0) as Balance from openquery([OP_DB_".$head_com_code."_CLEAN],'select
am.name as account_number,
am.ref as account_ref,
am.description as account_description,
rp.ref as code_partner,
rp.name as partner_name,
ap.name as period,
aa.code as account_code,
aa.name as account_name,
al.credit,al.debit,
rp.ref
from account_move_line as al
left outer join account_move  as am on al.move_id=am.id
left outer join account_account as aa on al.account_id=aa.id
left outer join account_period as ap on am.period_id=ap.id
left outer join res_partner as rp on al.partner_id=rp.id
where
ap.name=''".str_pad($this_month,2,'0',STR_PAD_LEFT)."/".$this_year."'' and al.partner_id is not null;') as op
WHERE
LTRIM(RTRIM(op.account_code)) IN (SELECT LTRIM(RTRIM([gl_code])) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_map] WHERE [company_id] = '".$head_com."' AND [ie_grouping_id] = '".$gl_id."' )
AND LTRIM(RTRIM(op.ref)) IN (SELECT RTRIM(LTRIM([".$condition_field_gl."])) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[".$table_condition."] WHERE [company_id_user] = '".$head_com."' AND [company_id_mapping] = '".$this_com."')

),0))

	 select @start_balance+@value_erp as Balance
";

	}else{
		if($type_gl!="B/S"){ /// พวก P/L ให้ดึงตามเดือนปกติเลย
				$plus_query = "set @start_balance = 0";
		}else{    ///////////////////////// B/S ต้อง Sum
			if($this_year=="2018"&&$this_month=="1"){ ///////////////////// This month + Start Balance
				$plus_query = " set @start_balance = (ISNULL((select ISNULL(open_amount,0) from [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_open_amount] where company_id=".$head_com." and company_id_mapping=".$this_com." and ie_grouping_id=".$gl_id." and row_active=1),0)) ";
			}
			else{////////////////////////////////////////////////////////// This month + Sum month < this month in table lastest
				$plus_query = " set @start_balance = (ISNULL((select top 1 ISNULL(amount,0) from [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_summary_lastest] where company_id_user=".$head_com." and company_id_mapping=".$this_com." and ie_grouping_gl_id=".$gl_id." and ((year=".$this_year." and month<".$this_month.") or (year<".$this_year.")) order by year desc,month desc,revision desc ),0)) ";
			}
		}
		$query_row = "
		DECLARE @start_balance float;
		DECLARE @value_erp float;
		".$plus_query."
		set @value_erp=ISNULL((select ISNULL(SUM(gbkmut.bdr_hfl),0) AS Balance FROM gbkmut WITH (NOLOCK)
 WHERE gbkmut.transtype IN ('N', 'C', 'P')
 AND bud_vers is NULL
 AND gbkmut.reknr IN (SELECT [gl_code] FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_map] WHERE [company_id] = ".$head_com." AND [ie_grouping_id] = ".$gl_id." )
 AND rtrim(ltrim(gbkmut.[".$condition_field_gl."])) IN (SELECT rtrim(ltrim(".$condition_field_gl.")) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[".$table_condition."] WHERE [company_id_user] = '".$head_com."' AND [company_id_mapping] = ".$this_com.")
 AND ISNULL(gbkmut.transsubtype,'') <> 'X'  AND gbkmut.datum >='".$date_start."'   AND gbkmut.datum <= '".$date_last."'
 AND gbkmut.oorsprong <> 'S'  AND gbkmut.remindercount <=13 AND gbkmut.bkstnr IS NOT NULL),0)

		select @start_balance+@value_erp as Balance
";
 if($db_name=="localhost"){$query_row = str_replace("[192.168.66.22].","",$query_row);}
	}
	//echo $query_row;
	return $query_row;
}


$date_start = $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-01";
$date_last =  $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($date_start)),2,"0",STR_PAD_LEFT);
$arr_obj_source = array();
$q_local = new Query_local();

	$arr_all_source = $q_local->query_table("select dm.*,c.company_id,c.company_db,c.company_code,c.db_type from datasource_mapping dm right join company c on dm.mapping_id = c.datasource_mapping_id  order by order_no asc");



echo '<table id="table_show_gl" style=" border-collapse: collapse;">';
echo '<tr class="GridviewScrollHeader">';
echo '<th scope="col">Type</th>';  echo '<th scope="col">GL ('.$_POST['user_company_code'].')</th>';
$i=0; $count_other_company = 0; $array_other_company=array(); $array_this_company=array();
while($i<sizeof($arr_all_source)){
	$conn = NULL;
	$data_s = $arr_all_source[$i];
	try{
		$conn = new PDO("sqlsrv:server=".$data_s['mapping_ip']."; Database = ".$data_s['company_db'], $data_s['mapping_username'], $data_s['mapping_password']);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$arr_obj_source[$data_s['company_id']] = $conn;

		if($data_s['company_id']!=$_POST['user_company_id']){
			echo '<th style="min-width:150px;" scope="col" align="center">'.$data_s['company_code'].'</th>';
			array_push($array_other_company,array($data_s['company_id'],$data_s['mapping_ip'],$data_s['db_type']));
			$count_other_company++;
		}else{
			array_push($array_this_company,array($data_s['company_id'],$data_s['mapping_ip'],$data_s['db_type']));
		}
	}catch(PDOException $e){
		echo $e;
		exit();
	}
	$i++;
}
echo '</tr>';
//print_r($array_this_company);

$count_row_total = 0; $group_gl=NULL;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Tbody
$array_sum_balance = array();
$all_group_gl = $q_local->query_table("select igg.* from ie_grouping_gl igg order by group_no asc,orderg asc,atid asc");
$i=0;
while($i<sizeof($all_group_gl)){
	$line_gl = $all_group_gl[$i];
	$des_gl=$line_gl['name'];
	$type_gl=$line_gl['type'];
	$type_gl_show =$line_gl['type_show'];
	$group_no_gl=$line_gl['group_no'];
	$group_id_gl=$line_gl['atid'];
	$condition_field_gl = $line_gl['condition_field']; if($condition_field_gl=="debnr"){$table_condition = "ie_debtor";}else{$table_condition = "ie_creditor";}


	echo '<tr tr-role="data" class="GridviewScrollItem">';
		echo '<td>'.$type_gl_show.'</td>';
		echo '<td gl-id="'.$group_id_gl.'">'.$des_gl.'</td>';
		$echo_balance_st1=check_map_gl($q_local,$group_id_gl,$_POST['user_company_id']);


		$j=0;
		while($j<$count_other_company){
			//$array_other_company[$j][0] == index (com_id)  ,,   $array_other_company[$j][1] == mapping source ,, $array_other_company[$j][2] == type erp
			echo '<td role-td="td_to_save" class="td_to_zoom_trans" gl-id="'.$group_id_gl.'" gl-group="'.$group_no_gl.'" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$array_other_company[$j][0].'" deb-crd-type="'.$condition_field_gl.'">';

			if($echo_balance_st1==true){
				$echo_balance_st2 = check_map_deb_crd($q_local,$condition_field_gl,$_POST["user_company_id"],$array_other_company[$j][0]);
				if($echo_balance_st2==true){
					$conn_row = $arr_obj_source[$_POST['user_company_id']];

					$query_row=get_query_str_check($array_this_company[0][2],$array_this_company[0][1],$group_id_gl,$condition_field_gl,$table_condition,$type_gl,$_POST['year'],$_POST['month'],$date_start,$date_last,$_POST['user_company_id'],$array_other_company[$j][0],$_POST["user_company_code"]);

					//echo $query_row;
 					$stmt_query=$conn_row->prepare($query_row);
					$stmt_query->execute();
					$arr_q_row=$stmt_query->fetch( PDO::FETCH_ASSOC );

					if($arr_q_row['Balance']==NULL){$number_balance=NULL; $show_number_balance=NULL; $cursor_pointer = "";}
					else{
						$number_balance = $arr_q_row['Balance'];
						$cursor_pointer = "cursor:pointer;";
						$show_number_balance = number_format($number_balance,2);
					}
					if(isset($array_sum_balance[$array_other_company[$j][0]])){
						$array_sum_balance[$array_other_company[$j][0]] = $array_sum_balance[$array_other_company[$j][0]]+$number_balance;
					}else{
						$array_sum_balance[$array_other_company[$j][0]] = $number_balance;
					}

					if($_POST["user_company_code"]=="PC"&&$group_id_gl=="47")
					{
						//echo $query_row.'<br>';
					}
					/*echo '<input type="text" class="input_balance" value="'.$show_number_balance.'" gl-id="'.$group_id_gl.'" gl-group="'.$group_no_gl.'" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$array_other_company[$j][0].'" deb-crd-type="'.$condition_field_gl.'" type-input="erp" disabled>';*/

					echo '<img title="'.$query_row.'" src="img/zoom.png" class="img_zoom_trans" width="13" height="13" style="'.$cursor_pointer.';float:right;" gl-id="'.$group_id_gl.'" gl-group="'.$group_no_gl.'" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$array_other_company[$j][0].'" deb-crd-type="'.$condition_field_gl.'">';
					echo '<span class="input_balance" style="float:right">'.$show_number_balance.'</span>';

				}else{} //st2
			}else{} // st1
			echo '</td>';
			$j++;
		}
	echo '</tr>';

	////////////////////////// Total
	$echo_totel = false;
	if(isset($all_group_gl[$i+1]['group_no'])){
		$check_next_group = $all_group_gl[$i+1]['group_no'];
		if($check_next_group!=$group_no_gl){$echo_totel = true;}else{$echo_totel = false;}
	}else{$echo_totel = true;}
	if($echo_totel==true){
		echo '<tr tr-role="total" class="GridviewScrollItem"><td style="background-color:#CCCCCC;"></td><td style="background-color:#CCCCCC;" align="right">Total Group '.$group_no_gl.' : </td>';
		$j=0;
		while($j<$count_other_company){		//$array_other_company[$j][0] = index of company
			if(isset($array_sum_balance[$array_other_company[$j][0]])){$total_balance = $array_sum_balance[$array_other_company[$j][0]];}else{$total_balance=NULL;}
			echo '<td align="right">';
			echo '<span class="span_total" gl-group="'.$group_no_gl.'" com-id="'.$array_other_company[$j][0].'">';
			if(isset($array_sum_balance[$array_other_company[$j][0]])==true&&$array_sum_balance[$array_other_company[$j][0]]!=NULL){echo number_format($total_balance,2);}
			echo '</span>';
			echo '</td>';
			$j++;
		}
		$array_sum_balance = array();
		echo '</tr>';

	}
	/////////////////////////
	$i++;
}

echo '</table>';
?>
<script type="text/javascript">
</script>
<style type="text/css">
/*
.td_to_zoom_trans{
	font-size: 90%;
	word-wrap: break-word;
    overflow-wrap: break-word;

}*/
.input_balance{
	border:none;
	width:75%;
	text-align: right;

}
.img_zoom_trans{
	cursor:pointer;
}
.span_total{
	text-align: right;
}
.GridviewScrollHeader TH, .GridviewScrollHeader TD
{
    padding: 5px;
    font-weight: bold;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #EFEFEF;
    text-align: left;
    vertical-align: bottom;
}
.GridviewScrollItem TD
{
    padding: 5px;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #FFFFFF;
    font-size: 85%;
}
.GridviewScrollPager
{
    border-top: 1px solid #AAAAAA;
    background-color: #FFFFFF;
}
.GridviewScrollPager TD
{
    padding-top: 3px;
    font-size: 14px;
    padding-left: 5px;
    padding-right: 5px;
}
.GridviewScrollPager A
{
    color: #666666;
}
.GridviewScrollPager SPAN
{

    font-size: 16px;

    font-weight: bold;

}
</style>
