<?php
require_once '../function.php';
//$_POST['user_company_code']="CI";
//$_POST['user_company_id']="3";
//$_POST['year']="2016";
//$_POST['month']="1";



$date_start = $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-01";
$date_last =  $_POST['year']."-".str_pad($_POST['month'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($date_start)),2,"0",STR_PAD_LEFT);
$arr_obj_source = array();
$q_local = new Query_local();
$arr_all_source = $q_local->query_table("select dm.*,c.company_id,c.company_db,c.company_code from datasource_mapping dm right join company c on dm.mapping_id = c.datasource_mapping_id order by order_no asc ");

echo '<table id="table_show_gl" style=" border-collapse: collapse;">';
echo '<tr class="GridviewScrollHeader">';
echo '<th scope="col">Type</th>';  echo '<th scope="col">GL ('.$_POST['user_company_code'].')</th>';
$i=0; $count_other_company = 0; $array_other_company=array(); $array_this_company=array();
while($i<sizeof($arr_all_source)){
	$conn = NULL;
	$data_s = $arr_all_source[$i];
	try{

		if($data_s['company_id']!=$_POST['user_company_id']){
			echo '<th style="min-width:150px;" scope="col" align="center">'.$data_s['company_code'].'</th>';
			array_push($array_other_company,array($data_s['company_id'],$data_s['mapping_ip']));
			$count_other_company++;
		}else{
			array_push($array_this_company,array($data_s['company_id'],$data_s['mapping_ip']));
		}
	}catch(PDOException $e){
		echo $e; exit();
	}
	$i++;
}
echo '<th scope="col" style="width:300px">Sum By GL</th>';
echo '</tr>';

$count_row_total = 0; $group_gl=NULL;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Tbody

$query_check_rev = "select top 1 revision as revision from ie_summary_lastest where  company_id_user='".$_POST['user_company_id']."' and year=".$_POST['year']." and month=".$_POST['month']." order by revision desc";
$arr_q_row_revision = $q_local->query_table($query_check_rev);
if(is_array($arr_q_row_revision)&&sizeof($arr_q_row_revision)>0){
	$revision = $arr_q_row_revision[0]['revision'];
}else{$revision = 0;}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$array_sum_balance = array();
$all_group_gl = $q_local->query_table("select igg.* from ie_grouping_gl igg order by group_no asc,orderg asc,atid asc");
$i=0;
while($i<sizeof($all_group_gl)){
	$line_gl = $all_group_gl[$i];
	$des_gl=$line_gl['name'];
	$type_gl=$line_gl['type'];
	$group_no_gl=$line_gl['group_no'];
	$group_id_gl=$line_gl['atid'];
	$condition_field_gl = $line_gl['condition_field']; if($condition_field_gl=="debnr"){$table_condition = "ie_debtor";}else{$table_condition = "ie_creditor";}


	echo '<tr tr-role="data" class="GridviewScrollItem">';
		echo '<td>'.$type_gl.'</td>';
		echo '<td gl-id="'.$group_id_gl.'">'.$des_gl.'</td>';
		$total_balance_sum_gl = 0;
		$j=0;
		while($j<$count_other_company){
			//$array_other_company[$j][0] == index (com_id)  ,,   $array_other_company[$j][1] == mapping source



					$query_row = "select atid as old_id,amount as Balance,type_edit from ie_summary_lastest where ie_grouping_gl_id='".$group_id_gl."' and company_id_user='".$_POST['user_company_id']."' and company_id_mapping='".$array_other_company[$j][0]."' and year=".$_POST['year']." and month=".$_POST['month']." ";
 					$arr_q_row_all=$q_local->query_table($query_row);
 					if(sizeof($arr_q_row_all)>0){$row_balance = $arr_q_row_all[0]['Balance']; $old_id= $arr_q_row_all[0]['old_id']; $type_edit= $arr_q_row_all[0]['type_edit'];}
 					else{$row_balance = NULL; $old_id=NULL; $type_edit=NULL;}

					if($row_balance==NULL){$number_balance=NULL; $show_number_balance=NULL; $cursor_pointer = "";}
					else{
						$number_balance = $row_balance;
						$show_number_balance = number_format($number_balance,2); /*echo $query_row;*/
					}
					if(isset($array_sum_balance[$array_other_company[$j][0]])){
						$array_sum_balance[$array_other_company[$j][0]] = $array_sum_balance[$array_other_company[$j][0]]+$number_balance;
					}else{
						$array_sum_balance[$array_other_company[$j][0]] = $number_balance;
					}

					echo '<td role-td="td_to_save" class="td_to_zoom_trans" gl-id="'.$group_id_gl.'" gl-group="'.$group_no_gl.'" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$array_other_company[$j][0].'" deb-crd-type="'.$condition_field_gl.'" revision="'.$revision.'" old-id="'.$old_id.'" type-input="erp">';

					echo '<input type="text" class="input_balance" value="'.$show_number_balance.'" gl-id="'.$group_id_gl.'" gl-group="'.$group_no_gl.'" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$array_other_company[$j][0].'" deb-crd-type="'.$condition_field_gl.'" revision="'.$revision.'" old-id="'.$old_id.'" type-input="'.$type_edit.'" >';
					/*echo '<img src="img/zoom.png" class="img_zoom_trans" style="'.$cursor_pointer.'" gl-id="'.$group_id_gl.'" gl-group="'.$group_no_gl.'" head-com="'.$_POST["user_company_id"].'" head-com-code="'.$_POST["user_company_code"].'" this-com="'.$array_other_company[$j][0].'" deb-crd-type="'.$condition_field_gl.'">';*/

			$total_balance_sum_gl+=$class_general->remove_comma($show_number_balance);
			echo '</td>';
			$j++;
		}
	echo '<td >'.number_format($total_balance_sum_gl,2).'</td>';
	echo '</tr>';

	////////////////////////// Total
	$echo_totel = false;
	if(isset($all_group_gl[$i+1]['group_no'])){
		$check_next_group = $all_group_gl[$i+1]['group_no'];
		if($check_next_group!=$group_no_gl){$echo_totel = true;}else{$echo_totel = false;}
	}else{$echo_totel = true;}
	if($echo_totel==true){
		echo '<tr tr-role="total" class="GridviewScrollItem"><td style="background-color:#CCCCCC;"></td><td style="background-color:#CCCCCC;" align="right">Total Group '.$group_no_gl.' : </td>';
		$j=0;
		while($j<$count_other_company){		//$array_other_company[$j][0] = index of company
			if(isset($array_sum_balance[$array_other_company[$j][0]])){$total_balance = $array_sum_balance[$array_other_company[$j][0]];}else{$total_balance=NULL;}
			echo '<td style="background-color:#CCCCCC;" align="right">';
			echo '<span class="span_total" gl-group="'.$group_no_gl.'" com-id="'.$array_other_company[$j][0].'">';
			if(isset($array_sum_balance[$array_other_company[$j][0]])==true&&$array_sum_balance[$array_other_company[$j][0]]!=NULL){echo number_format($total_balance,2);}
			echo '</span>';
			echo '</td>';
			$j++;
		}
		$array_sum_balance = array();
		echo '<td style="background-color:#CCCCCC;"></td>';
		echo '</tr>';

	}
	/////////////////////////
	$i++;
}

echo '</table>';
?>
<script type="text/javascript">
</script>
<style type="text/css">
/*
.td_to_zoom_trans{
	font-size: 90%;
	word-wrap: break-word;
    overflow-wrap: break-word;

}*/
.input_balance{
	border:none;
	width:100%;
	font-size: 90%;
	text-align: right;
}
.img_zoom_trans{
	cursor:pointer;
	width: 20px;
	height: 20px;
}
.span_total{
	text-align: right;
}
.GridviewScrollHeader TH, .GridviewScrollHeader TD
{
    padding: 5px;
    font-weight: bold;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #EFEFEF;
    text-align: left;
    vertical-align: bottom;
}
.GridviewScrollItem TD
{
    padding: 5px;
    white-space: nowrap;
    border-right: 1px solid #AAAAAA;
    border-bottom: 1px solid #AAAAAA;
    background-color: #FFFFFF;

}
.GridviewScrollPager
{
    border-top: 1px solid #AAAAAA;
    background-color: #FFFFFF;
}
.GridviewScrollPager TD
{
    padding-top: 3px;
    font-size: 14px;
    padding-left: 5px;
    padding-right: 5px;
}
.GridviewScrollPager A
{
    color: #666666;
}
.GridviewScrollPager SPAN
{

    font-size: 16px;

    font-weight: bold;

}
</style>
