<?php
	header("Content-Type: application/vnd.ms-excel;");
	header('Content-Disposition: attachment; filename="excel_export_InterCE.xls"');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<meta http-equiv="Content-type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
</HEAD>
<BODY>
<?php
require_once '../function.php';
//$_POST['hidden_companycode_excel']="CI";
//$_POST['hidden_company_excel']="3";
//$_POST['hidden_year_excel']="2016";
//$_POST['hidden_month_excel']="1";


$date_start = $_POST['hidden_year_excel']."-".str_pad($_POST['hidden_month_excel'],2,"0",STR_PAD_LEFT)."-01";
$date_last =  $_POST['hidden_year_excel']."-".str_pad($_POST['hidden_month_excel'],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($date_start)),2,"0",STR_PAD_LEFT);
//$arr_obj_source = array();
$class_q_local = new Query_local();
$arr_all_cmp = $class_q_local->query_table("select * from  company order by order_no asc");


echo '<table border="1">';
echo '<thead>';
echo '<tr style="background-color:#CCCCCC;">';
echo '<td>Type</td>'; echo '<td>GL ('.$_POST['hidden_companycode_excel'].')</td>';
$i=0; $array_other_company = array(); $array_this_company = NULL; $count_other_company=0;
while($i<sizeof($arr_all_cmp)){
    $data_s = $arr_all_cmp[$i];
    if($_POST['hidden_company_excel']==$data_s['company_id']){
        $array_this_company = array("company_id"=>$data_s['company_id'],"company_code"=>$data_s['company_code']);
    }
    else{
        array_push($array_other_company,array($data_s['company_id'],$data_s['company_code']));
        echo '<td colspan="2" style="min-width:150px;" scope="col" align="center">'.$data_s['company_code'].'</td>';
        $count_other_company++;
    }
    $i++;
} 
echo '<td colspan="2"  align="center">Total GL</td>';
echo '<td  align="center">DIFF</td>';
echo '</tr>';
echo '</thead>';
echo '<tbody>';
$arr_all_gl = $class_q_local->query_table("select * from ie_grouping_gl order by group_no asc,atid asc");
$j=0;
$array_com_sum_total_left = array(); $array_com_sum_total_right = array();
$array_com_sum_grand_total_left = array(); $array_com_sum_grand_total_right = array();
while($j<sizeof($arr_all_gl)){
    $gl_id = $arr_all_gl[$j]['atid'];
    $group_no = $arr_all_gl[$j]['group_no'];
    $condition_field_gl = $arr_all_gl[$j]['condition_field'];
    $type_gl =$arr_all_gl[$j]['type'];
    echo '<tr>';
    echo '<td>'.$type_gl.' ('.$arr_all_gl[$j]['group_no'].')</td>';
    echo '<td>'.$arr_all_gl[$j]['name'].'</td>';
    $k=0;
    $sum_total_gl_left = NULL; $sum_total_gl_right = NULL;
    while($k<$count_other_company){ //// loop right side by company in this gl
        $com_id_and_code = $array_other_company[$k];
        if(!isset($array_com_sum_total_left[$com_id_and_code[0]])){$array_com_sum_total_left[$com_id_and_code[0]]=NULL;}
        if(!isset($array_com_sum_total_right[$com_id_and_code[0]])){$array_com_sum_total_right[$com_id_and_code[0]]=NULL;}

        if($type_gl=="B/S"){
            $query_value_left = "select (isl.amount) 
 as amount
  from ie_summary_lastest  isl
  where isl.company_id_user='".$_POST['hidden_company_excel']."' and isl.company_id_mapping='".$com_id_and_code[0]."' and isl.ie_grouping_gl_id='".$gl_id."'
  and (isl.year='".$_POST['hidden_year_excel']."' and isl.month='".$_POST['hidden_month_excel']."')";
            $query_value_right = "select (isl.amount) 
 as amount
  from ie_summary_lastest  isl
  where isl.company_id_user='".$com_id_and_code[0]."' and isl.company_id_mapping='".$_POST['hidden_company_excel']."' and isl.ie_grouping_gl_id='".$gl_id."'
  and (isl.year='".$_POST['hidden_year_excel']."' and isl.month='".$_POST['hidden_month_excel']."')";
        }else{
                if($_POST['hidden_year_excel']=="2016"){
                    $query_value_left = "select amount+IsNULL((select open_amount from ie_grouping_gl_open_amount where row_active=1 and ie_grouping_id='".$gl_id."' and company_id='".$_POST['hidden_company_excel']."' and company_id_mapping='".$com_id_and_code[0]."'),0) as amount from ie_summary_lastest where company_id_user='".$_POST['hidden_company_excel']."' and company_id_mapping='".$com_id_and_code[0]."' and  ie_grouping_gl_id='".$gl_id."' and (year=".$_POST['hidden_year_excel']." and month<=".$_POST['hidden_month_excel'].")  ";
                    $query_value_right = "select amount+IsNULL((select open_amount from ie_grouping_gl_open_amount where row_active=1 and ie_grouping_id='".$gl_id."' and company_id='".$com_id_and_code[0]."' and company_id_mapping='".$_POST['hidden_company_excel']."'),0) as amount from ie_summary_lastest where company_id_user='".$com_id_and_code[0]."' and company_id_mapping='".$_POST['hidden_company_excel']."' and year=".$_POST['hidden_year_excel']." and month<=".$_POST['hidden_month_excel']." and ie_grouping_gl_id='".$gl_id."' ";
                }else{
                    $query_value_left = "select amount from ie_summary_lastest where company_id_user='".$_POST['hidden_company_excel']."' and company_id_mapping='".$com_id_and_code[0]."' and  ie_grouping_gl_id='".$gl_id."' and (year=".$_POST['hidden_year_excel']." and month<=".$_POST['hidden_month_excel'].")  ";
                    $query_value_right = "select amount from ie_summary_lastest where company_id_user='".$com_id_and_code[0]."' and company_id_mapping='".$_POST['hidden_company_excel']."' and year=".$_POST['hidden_year_excel']." and month<=".$_POST['hidden_month_excel']." and ie_grouping_gl_id='".$gl_id."' ";

                }
        }
        
       
        $arr_q_row_left=$class_q_local->query_table($query_value_left); 
        $arr_q_row_right=$class_q_local->query_table($query_value_right);
        //select open_amount from ie_grouping_gl_open_amount where company_id='9' and company_id_mapping='1' and ie_grouping_id='8' and row_active=1
            if(is_array($arr_q_row_left)&&sizeof($arr_q_row_left)>0){
                $num_left = $arr_q_row_left[0]['amount'];
            }
            else{
                $num_left=NULL;
            }
        
            if(is_array($arr_q_row_right)&&sizeof($arr_q_row_right)>0){
                $num_right = $arr_q_row_right[0]['amount'];
            }else{
                $num_right=NULL;
            }
        if($num_left!=$num_right){$normal_or_blink="blink";}else{$normal_or_blink="normal";}
        /////////////////////////////////////////////////////////////////////////////// TD data
       
        echo '<td  style="min-width:150px;">'; 
            //echo $query_value_left
            if($num_left!=NULL){echo '<span span-side="left" head-com="'.$_POST["hidden_company_excel"].'" head-com-code="'.$_POST["hidden_companycode_excel"].'" this-com="'.$com_id_and_code[0].'" gl-id="'.$gl_id.'" deb-crd-type="'.$condition_field_gl.'" status="'.$normal_or_blink.'" style="padding:5px;">'.number_format($num_left,2).'</span>'; $sum_total_gl_left+=$num_left; $array_com_sum_total_left[$com_id_and_code[0]]+=$num_left;}  

        echo '</td>';   

            //////////////////////////////////////////////////////////////////////////////// Between Left and Right 
            //    if($num_left!=NULL||$num_right!=NULL){echo '<font color="RED" size="3">|</font>';}
            //////////////////////////////////////////////////////////////////////////////// Between Left and Right 

        echo '<td  style="min-width:150px;">'; 

            if($num_right!=NULL){echo '<span span-side="right" head-com="'.$_POST["hidden_company_excel"].'" head-com-code="'.$_POST["hidden_companycode_excel"].'" this-com="'.$com_id_and_code[0].'" gl-id="'.$gl_id.'" deb-crd-type="'.$condition_field_gl.'" status="'.$normal_or_blink.'" style="padding:5px;">'.number_format($num_right,2).'</span>'; $sum_total_gl_right+=$num_right; $array_com_sum_total_right[$com_id_and_code[0]]+=$num_right;}
        
        echo '</td>';
        /////////////////////////////////////////////////////////////////////////////// End TD data
        $k++;
        if($k==$count_other_company){ /// Total GL&Diff
            echo '<td>'; 
                echo ($sum_total_gl_left!=NULL)?number_format($sum_total_gl_left,2):NULL; 
            echo '</td>'; 
                //echo ($sum_total_gl_left!=NULL||$sum_total_gl_right!=NULL)?'<font color="RED" size="3">|</font>':NULL;
            echo '<td>'; 
                echo ($sum_total_gl_right!=NULL)?number_format($sum_total_gl_right,2):NULL; 
            echo '</td>';
            echo '<td>';
                echo ($sum_total_gl_left!=NULL||$sum_total_gl_right!=NULL)?number_format(($sum_total_gl_left-$sum_total_gl_right),2):NULL;
            echo '</td>'; 
        }
    }                     
    echo '</tr>';
    $echo_total = false;
    if(isset($arr_all_gl[$j+1]['group_no'])){
        $check_next_group = $arr_all_gl[$j+1]['group_no'];
        if($check_next_group!=$group_no){$echo_total = true;}else{$echo_total = false;}
    }else{$echo_total = true;}
    if($echo_total==true){
        echo '<tr>';
            echo '<td style="border-right:#CCCCCC;background-color:#CCCCCC;">'; 
            echo '</td>';
            echo '<td align="right" style="border-left:#CCCCCC;background-color:#CCCCCC;">Total Group '.$group_no.' :'; 
            echo '</td>';
            $y=0;
            $sum_total_of_total_gl_left = NULL; $sum_total_of_total_gl_right = NULL;
            while($y<$count_other_company){
                $com_id_and_code_total = $array_other_company[$y];
                if(!isset($array_com_sum_grand_total_left[$com_id_and_code_total[0]])){$array_com_sum_grand_total_left[$com_id_and_code_total[0]]=NULL;}
                if(!isset($array_com_sum_grand_total_right[$com_id_and_code_total[0]])){$array_com_sum_grand_total_right[$com_id_and_code_total[0]]=NULL;}
                $total_left = $array_com_sum_total_left[$com_id_and_code_total[0]];
                $total_right = $array_com_sum_total_right[$com_id_and_code_total[0]];
                echo '<td>'; 
                    if($total_left!=NULL){echo '<span style="padding:5px;">'.number_format($total_left,2).'</span>'; $sum_total_of_total_gl_left+=$total_left; $array_com_sum_grand_total_left[$com_id_and_code_total[0]]+=$total_left;}
                echo '</td>';  
                   // if($total_left!=NULL||$total_right!=NULL){echo '<font color="RED" size="3">|</font>';}
                echo '<td>';     
                    if($total_right!=NULL){echo '<span style="padding:5px;">'.number_format($total_right,2).'</span>'; $sum_total_of_total_gl_right+=$total_right; $array_com_sum_grand_total_right[$com_id_and_code_total[0]]+=$total_right;}
                echo '</td>';
                $array_com_sum_total_left[$com_id_and_code_total[0]]=NULL;
                $array_com_sum_total_right[$com_id_and_code_total[0]]=NULL;
                $y++;
                if($y==$count_other_company){ ///////// total of total GL && DIF
                    echo '<td>'; 
                        echo ($sum_total_of_total_gl_left!=NULL)?number_format($sum_total_of_total_gl_left,2):NULL; 
                    echo '</td>';     
                        //echo ($sum_total_of_total_gl_left!=NULL||$sum_total_of_total_gl_right!=NULL)?'<font color="RED" size="3">|</font>':NULL;
                    echo '<td>';     
                        echo ($sum_total_of_total_gl_right!=NULL)?number_format($sum_total_of_total_gl_right,2):NULL; 
                    echo '</td>';
                    echo '<td>'; 
                        echo ($sum_total_of_total_gl_left!=NULL||$sum_total_of_total_gl_right!=NULL)?number_format(($sum_total_of_total_gl_left-$sum_total_of_total_gl_right),2):NULL;
                    echo '</td>';
                }
            }
        echo '</tr>';
    }
    //////////////////////////////////////////////////////// Grand Total
    if(!isset($arr_all_gl[$j+1]['group_no'])){
        echo '<tr>';
            echo '<td style="border-right:#CCCCCC;background-color:#CCCCCC;">'; 
            echo '</td>';
            echo '<td align="right" style="border-left:#CCCCCC;background-color:#CCCCCC;">Grand Total :'; 
            echo '</td>';
            $z=0;
            $sum_total_of_grand_total_gl_left = NULL; $sum_total_of_grand_total_gl_right = NULL;
            while($z<$count_other_company){
                $com_id_and_code_grand_total = $array_other_company[$z];
                //if(!isset($array_com_sum_grand_total_left[$com_id_and_code_grand_total[0]])){$array_com_sum_grand_total_left[$com_id_and_code_grand_total[0]]=NULL;}
                //if(!isset($array_com_sum_grand_total_right[$com_id_and_code_grand_total[0]])){$array_com_sum_grand_total_right[$com_id_and_code_grand_total[0]]=NULL;}
                $grand_total_left = $array_com_sum_grand_total_left[$com_id_and_code_grand_total[0]];
                $grand_total_right = $array_com_sum_grand_total_right[$com_id_and_code_grand_total[0]];
                echo '<td>'; 
                    if($grand_total_left!=NULL){echo '<span style="padding:5px;">'.number_format($grand_total_left,2).'</span>'; $sum_total_of_grand_total_gl_left+=$grand_total_left;}
                echo '</td>';     
                    //if($grand_total_left!=NULL||$grand_total_right!=NULL){echo '<font color="RED" size="3">|</font>';}
                echo '<td>';     
                    if($grand_total_right!=NULL){echo '<span style="padding:5px;">'.number_format($grand_total_right,2).'</span>'; $sum_total_of_grand_total_gl_right+=$grand_total_right;}
                echo '</td>';
                $z++;
                if($z==$count_other_company){ ///////// total of total GL && DIF
                    echo '<td>'; 
                        echo ($sum_total_of_grand_total_gl_left!=NULL)?number_format($sum_total_of_grand_total_gl_left,2):NULL; 
                    echo '</td>';     
                        //echo ($sum_total_of_grand_total_gl_left!=NULL||$sum_total_of_grand_total_gl_right!=NULL)?'<font color="RED" size="3">|</font>':NULL;
                    echo '<td>';     
                        echo ($sum_total_of_grand_total_gl_right!=NULL)?number_format($sum_total_of_grand_total_gl_right,2):NULL; 
                    echo '</td>';
                    echo '<td>'; 
                        echo ($sum_total_of_grand_total_gl_left!=NULL||$sum_total_of_grand_total_gl_right!=NULL)?number_format(($sum_total_of_grand_total_gl_left-$sum_total_of_grand_total_gl_right),2):NULL;
                    echo '</td>';
                }
            }
    }
    $j++;
}
echo '</tbody>';
echo '</table>';
?>
</BODY>
</HTML>
