<?php
require_once 'SSO/SSO.php'; // นำเข้าไฟล์ Library
require_once 'function.php'; 

$APP_ID = 1610281337; // ไอดีของแอพพลิเคชั่น

// ตรวจสอบการล็อกอิน
$sso = new SSO($APP_ID);
$ssoResponse = $sso->getAuthentication();

$personDetail = $ssoResponse['personDetail']; // ข้อมูลพนักงาน
$panelLogout = $ssoResponse['panelLogout']; // html code แสดงปุ่มออกจากระบบ

// แสดงข้อมูล
echo $panelLogout;
//echo "<hr>";
//echo "<br>";
//var_dump($personDetail);
//echo $personDetail['CompanyID'];
$class_q_local = new Query_local();

//$personDetail['CompanyCode'] = "CI";
$arr_com_id=$class_q_local->query_table("select company_id from company where company_code='".$personDetail['CompanyCode']."'");

if(!is_array($arr_com_id)){exit();}else{
   $company_id = $arr_com_id[0]['company_id'];
   if($company_id==0||$company_id==NULL){exit();}
}

?>
<?php
    //if(trim($personDetail['CompanyCode'])=="PC"){
        ?>
        <script type="text/javascript">
            //window.location = "index_mc_confirm.php";
        </script>
        <?php
    //}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Intercompany Eliminations System</title>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/small-business.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="datatable/datatable.bootstrap.css">
    <link type="text/css" rel="stylesheet" href="datepicker/datepicker3.css" media="screen" />
    <link href="dialog/css/black-tie/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="css/isloading.css" rel="stylesheet">
    <link rel='stylesheet' type='text/css'href='timepicker/css/timepicki.css'/>
    <link rel='stylesheet' type='text/css'href='css/GridviewScroll.css'/>

    <script src="js/jquery.js"></script> 
    <script src="js/bootstrap.min.js"></script>
    <script src="dialog/js/jquery-ui-1.9.2.custom.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.th.js"></script>
    <script type='text/javascript'src='timepicker/js/timepicki.js'></script>
    <script type="text/javascript" language="javascript" src="datatable/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.tableTools.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.bootstrap.js"></script>
    <script type='text/javascript'src='js/jquery.isloading.js'></script>
    <script type="text/javascript" language="javascript" src="js/gridviewScroll.min.js"></script>
</head>

<body>
<input type="hidden" id="hidden_user_company" value="<?=trim($personDetail['CompanyCode']);?>">
<input type="hidden" id="hidden_user_company_id" value="<?=trim($company_id);?>">
<input type="hidden" id="hidden_user_id" value="<?=trim($personDetail['UserID']);?>">
<input type="hidden" id="hidden_user_email" value="<?=trim($personDetail['ExtEmail']);?>">
    <!-- Navigation -->
    <nav role="navigation" style="background-color:#FFFFFF;border-bottom:2px solid;padding:5px;margin-bottom:10px;height:60px;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">  
                <a class="navbar-brand" href="#">
                    <img class="img_ja" src="img/LOGO_ART_PRECISE.png" width="220" height="40" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div style="float:right;" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                   $class_general = new general_function();
                   echo $class_general->get_menu(basename(__FILE__));
                ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Total 70px -->
    <!-- Page Content -->
    <div class="container" style="background-color:#FFFFFF;width:100%;">
        
        <div class="filter_header" style="height:90px;">
            <table width="100%" class="table_filter">
            <tr>
                    <td><div class="header_form">View Compare Group Revenue</div></td>
            </tr> 
            </table>
            <table width="95%" class="table_filter">                 
                <tr>
                    <td width="4%" style="display:none;"><input type="radio"  name="check_type_filter" value="1"   checked></td>
                    <td width="10%" align="right">Month * : </td>
                    <td width="30%" >
                        <select id="filter_fiscal_month_start" class="form-control" style="width:48%;float:left;">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <span style="width:4%;float:left;vertical-align:middle;" align="center">-</span>
                        <select id="filter_fiscal_month_end" class="form-control" style="width:48%;float:left;">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </td>
                    <td width="10%" align="right" rowspan="2">Year * : </td>
                    <td width="15%" rowspan="2">
                        <select id="filter_fiscal_year" class="form-control" style="width:220px;">
                        </select>
                    </td>
                    <td width="10%" align="right">Company : </td>
                    <td width="15%">
                    <select id="rev" class="form-control" style="width:200px;">
                    <?php
                        $group=array('','','PEM','PSP','PPP','PDE','PSL');
                        foreach($group as $key=>$value){
                            if($key>1){
                                echo "<option value=".$key.">$value</option>";
                            }
                        }


                    ?>
                    </select>
                    </td>
                    <td width="10%" rowspan="2">
                        <input type="button" id="buttton_search" onclick="show_view_gl_compare();" value="Search" class="btn btn-primary btn-sm">
                    </td>
                </tr>
            </table>
            
        </div><br><br>
        <!-- style="overflow-x:scroll;" -->
        <iframe id="txtArea1" style="display:none"></iframe>
        <div id="show_view_gl">
            no data
        </div>
        <div>
            <span id="query_time" style="font-size:2px;height:5px;display:none;"></span>
            <span id="view_menu_extras" style="display:none;">
            <img id="img_export_excel" title="Export Excel" style="float:right;cursor:pointer;margin-right:2%;margin-top:5px;" width="15" height="15" src="img/excel.png" onclick="fnExcelReport('table_show_compare1');">
            </span>
        </div>  
        <!--<br>
        <div align="center"><span id="back_to_top" style="font-size:2px;cursor:pointer;">back tp top</span></div>
        <br>-->
        <div id="dialog_all">
                    <div id="dialog_zoom_trans" title="Transaction : ">
                        <div id="dialog_html_zoom_trans" ></div>
                    </div>
        </div>
       <form id="form_excel" action="" method="POST">
            <input type="hidden" id="hidden_year_excel" name="hidden_year_excel">
            <input type="hidden" id="hidden_month_excel" name="hidden_month_excel">
            <input type="hidden" id="hidden_company_excel" name="hidden_company_excel">
            <input type="hidden" id="hidden_companycode_excel" name="hidden_companycode_excel">
       </form>
    </div>
    <!-- /.container -->
    
</body>
</html>
    
<script type="text/javascript">
var win_width = window.innerWidth;
var win_height = window.innerHeight;
var ajax_request = null;
var date_obj = new Date();
var date = date_obj.getDate(); var month = (date_obj.getMonth())+1; var year = date_obj.getFullYear();
//var year = 2017;
//var month = 1;
$( "#dialog_zoom_trans" ).dialog({
            autoOpen: false,
            width: ((win_width*95)/100),
            height: ((win_height*80)/100),
            position: [((win_width/2)-(((win_width*95)/100)/2)),((win_height/2)-(((win_height*80)/100)/2))],
            resizable: true,
            buttons: [   
            {
                text: "EXCEL",
                click: function(){
                    fnExcelReport('table_trans_inner_view_by_com');
                }
            }
            ,
            {
                text: "CLOSE",
                click: function() {      
                   $("#dialog_zoom_trans").dialog("close");
              }
            }
          ]
});

$(document).ready(function(){
    $("body").fadeIn(2000);
    push_year_filter(); push_month_filter($("#filter_fiscal_year").val()); 
});

$("#filter_fiscal_year").change(function(){
    push_month_filter(this.value); 
    clear_view();
});
$("#filter_rev,#filter_fiscal_month_start,#filter_fiscal_month_end").change(function(){
    clear_view();
});

function push_year_filter(){
    var  start_year = 2017;
    while(start_year<=year){
        $("#filter_fiscal_year").append('<option value="'+start_year+'">'+start_year+'</option>');
        start_year++;
    }
    $("#filter_fiscal_year").val(year);
}
function push_month_filter(p_year){
    $("#filter_fiscal_month_start,#filter_fiscal_month_end").val(month);
}
function formatNumber(num,dec,thou,pnt,curr1,curr2,n1,n2) {var x = Math.round(num * Math.pow(10,dec));if (x >= 0) n1=n2='';var y = (''+Math.abs(x)).split('');var z = y.length - dec; if (z<0) z--; for(var i = z; i < 0; i++) y.unshift('0'); if (z<0) z = 1; y.splice(z, 0, pnt); if(y[0] == pnt) y.unshift('0'); while (z > 3) {z-=3; y.splice(z,0,thou);}var r = curr1+n1+y.join('')+n2+curr2;return r;}
function count_dot(s1, letter) {
    return ( s1.match( RegExp(letter,'g') ) || [] ).length;
}
function ajax_abort(){
    if(ajax_request!=null){
        ajax_request.abort();
    }
}
function clear_view(){
    $("ul.nav.nav-tabs,span#view_menu_extras,span#query_time").hide();
    $("#show_view_gl").html('no data');
}

function show_view_gl_compare(){
    ajax_abort();
    if($("input[name=check_type_filter]:checked").val()=="2"){if($("#date_start").val()==""||$("#date_end").val()==""){alert("กรุณาเลือกช่วงเดือน"); return false;}}
    var timestamp_start = null;
    var timestamp_stop = null;
    $("#view_menu_extras,ul.nav.nav-tabs").show(); //set_lock_hidden_view();
    ajax_request = $.ajax({
            url: "get_data_other_report/get_html_view_by_revenue.php",
            async: true,
            dataType: "text",
            type: "post",
            data: {"year":$("#filter_fiscal_year").val(),"check_type_filter":$("input[name=check_type_filter]:checked").val(),"month_start":$("#filter_fiscal_month_start option:selected").val(),"month_end":$("#filter_fiscal_month_end option:selected").val(),"rev":$("#rev option:selected").val()},
            beforeSend: function(){
                timestamp_start = new Date().getTime();
                $("#show_view_gl,#query_time").html(''); 
                $("#show_view_gl").isLoading({ text:"Loading View",position:"overlay"});
                $("table.table_filter select,table.table_filter input").attr("disabled","disabled"); 
            },
            success: function (result) {
                timestamp_stop = new Date().getTime();
                $("#show_view_gl").html(result);
                $("#show_view_gl").isLoading("hide");
                $("table.table_filter select,table.table_filter input").removeAttr("disabled"); 
                $("#query_time").html('Query Time : '+((timestamp_stop-timestamp_start)/1000)+' sec').show();
                // $("span.span_amount").click(function(){
                //     if($.trim($(this).html())!=""){
                //         get_html_zoom_trans_by_com($(this).attr("head-com"),$(this).attr("head-com-code"),$(this).attr("gl-id"),$(this).attr("this-com"),$(this).attr("this-com-code"),$(this).attr("deb-crd-type"));
                //     }
                    
                // });
                //$('#table_show_compare').gridviewScroll({width: ((98*win_width)/100),height: get_height_of_view("compare"),freezesize: 0, headerrowcount: 1  }); 
            }
        }); 
}
function get_html_zoom_trans_by_com(head_com,head_com_code,gl_id,this_com,this_com_code,deb_crd_type){
    console.log(this_com);
    $.ajax({
            url: "get_data_other_report/get_html_view_transaction_apd_pem_pem1.php",
            async: true,
            dataType: "text",
            type: "post",
            data: {"head_com":head_com,"head_com_code":head_com_code,"gl_id":gl_id,"this_com":this_com,"this_com_code":this_com_code,"year":$("#filter_fiscal_year").val(),"month":$("#filter_fiscal_month").val(),"deb_crd_type":deb_crd_type,"check_type_filter":$("input[name=check_type_filter]:checked").val(),"month_start":$("#filter_fiscal_month_start option:selected").val(),"month_end":$("#filter_fiscal_month_end option:selected").val()},
            beforeSend: function(){
                $("#dialog_html_zoom_trans").html('');
                $("#dialog_zoom_trans").dialog("open");
                $("#dialog_html_zoom_trans").isLoading({ text: "Loading Transaction",position:"overlay"}); 
            },
            success: function (result) {
                console.log(result);
                $("#dialog_html_zoom_trans").isLoading("hide");  
                $("#dialog_html_zoom_trans").html(result);
                
            }
    }); 
}


function get_height_of_view(type_view){
        return ((win_height-(211+64)));
}

function fnExcelReport(table_id)
{
    var tab_text="<table border='2px'><tr>";
    var textRange; var j=0;
    tab = document.getElementById(table_id); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
        
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, "");
   

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"export.xls");
    }  
    else                 //other browser not tested on IE 11
            var  link = new Blob([tab_text], { type: 'application/vnd.ms-excel' });
            var linkUrl = URL.createObjectURL(link);
            window.location.href=linkUrl
       // sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

   // return (sa);
}


var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()



</script>
<style type="text/css">
.disable_a_link{
    pointer-events: none;
    cursor: default;
}

</style>
