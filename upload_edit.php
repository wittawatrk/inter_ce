<?php
require_once 'SSO/SSO.php'; // นำเข้าไฟล์ Library
require_once 'function.php'; 

$APP_ID = 1610281337; // ไอดีของแอพพลิเคชั่น

// ตรวจสอบการล็อกอิน
$sso = new SSO($APP_ID);
$ssoResponse = $sso->getAuthentication();

$personDetail = $ssoResponse['personDetail']; // ข้อมูลพนักงาน
$panelLogout = $ssoResponse['panelLogout']; // html code แสดงปุ่มออกจากระบบ

// แสดงข้อมูล
echo $panelLogout;
//echo "<hr>";
//echo "<br>";
//var_dump($personDetail);
//echo $personDetail['CompanyID'];
$class_q_local = new Query_local();

//$personDetail['CompanyCode'] = "CI";
$arr_com_id=$class_q_local->query_table("select company_id from company where company_code='".$personDetail['CompanyCode']."'");

if(!is_array($arr_com_id)){exit();}else{
   $company_id = $arr_com_id[0]['company_id'];
   if($company_id==0||$company_id==NULL){exit();}
}

?>
<?php
	//if(trim($personDetail['CompanyCode'])=="PC"){
		?>
		<script type="text/javascript">
			//window.location = "index_mc_confirm.php";
		</script>
		<?php
	//}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Intercompany Eliminations System</title>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/small-business.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="datatable/datatable.bootstrap.css">
    <link type="text/css" rel="stylesheet" href="datepicker/datepicker3.css" media="screen" />
    <link href="dialog/css/black-tie/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="css/isloading.css" rel="stylesheet">
    <link rel='stylesheet' type='text/css'href='timepicker/css/timepicki.css'/>
    <link rel='stylesheet' type='text/css'href='css/GridviewScroll.css'/>

    <script src="js/jquery.js"></script> 
    <script src="js/bootstrap.min.js"></script>
    <script src="dialog/js/jquery-ui-1.9.2.custom.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.th.js"></script>
    <script type='text/javascript'src='timepicker/js/timepicki.js'></script>
    <script type="text/javascript" language="javascript" src="datatable/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.tableTools.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.bootstrap.js"></script>
    <script type='text/javascript'src='js/jquery.isloading.js'></script>
    <script type="text/javascript" language="javascript" src="js/gridviewScroll.min.js"></script>
</head>

<body>
<input type="hidden" id="hidden_user_company" value="<?=trim($personDetail['CompanyCode']);?>">
<input type="hidden" id="hidden_user_company_id" value="<?=trim($company_id);?>">
<input type="hidden" id="hidden_user_id" value="<?=trim($personDetail['UserID']);?>">
<input type="hidden" id="hidden_user_email" value="<?=trim($personDetail['ExtEmail']);?>">
    <!-- Navigation -->
    <nav role="navigation" style="background-color:#FFFFFF;border-bottom:2px solid;padding:5px;margin-bottom:10px;height:60px;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">  
                <a class="navbar-brand" href="#">
                    <img class="img_ja" src="img/LOGO_ART_PRECISE.png" width="220" height="40" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div style="float:right;" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                   $class_general = new general_function();
                   echo $class_general->get_menu(basename(__FILE__));
                ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Total 70px -->
    <!-- Page Content -->
    <div class="container" style="background-color:#FFFFFF;width:98%;">
        
        <div class="filter_header" style="height:90px;">
            <table width="100%" class="table_filter">
            <tr>
                    <td><div class="header_form">EDITABLE FROM LOCAL DATABASE</div></td>
            </tr> 
            </table>
            <table width="70%" class="table_filter">
                              
                <tr>
                    <td width="10%" align="right">Year * : </td>
                    <td width="20%" >
                        <select id="filter_fiscal_year" class="form-control" style="width:200px;">
                        </select>
                    </td>
                    <td width="10%" align="right">Month * : </td>
                    <td width="20%" >
                        <select id="filter_fiscal_month" class="form-control" style="width:200px;">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </td>
                    <td width="10%" align="right">Company : </td>
                    <td width="20%" >
                        <?php 
                            $arr_table_com=$class_q_local->query_table("select * from company where db_type = 'none' order by order_no ");
                            if(is_array($arr_table_com)&&sizeof($arr_table_com)>0){
                                $i=0; $option = '<option value="">Choose Company</option>';
                                while($i<sizeof($arr_table_com)){ 
                                    $com_code_option = trim($arr_table_com[$i]["company_code"]); $com_id_option = trim($arr_table_com[$i]["company_id"]);
                                    if(array_search($com_code_option,$personDetail['CompanyAllowed'])!==FALSE){
                                        if(sizeof($personDetail['CompanyAllowed'])==1){
                                            if(trim($com_code_option)==trim($personDetail['CompanyCode'])){$selected="selected";}else{$selected=NULL;}
                                        }else{
                                            $selected=NULL;
                                        }
                                        $option.='<option value="'.$com_id_option.'" '.$selected.'>'.$com_code_option.'</option>';
                                    }
                                    $i++;
                                }
                            }else{
                                $option = '<option value="">Choose Company</option>';
                            }   
                        ?>
                        <select id="filter_company" class="form-control" style="width:200px;">
                            <?php
                                echo $option;
                            ?>
                        </select>
                    </td>
                    <td width="10%"><input type="button" class="btn btn-primary btn-sm" value="SEARCH" onclick="show_view_editable();"></td>
                </tr>
            </table>
            
        </div>
        <!-- style="overflow-x:scroll;" -->
        <div id="show_view_gl">
            no data
        </div>
        <div>
            <span id="query_time" style="font-size:2px;height:5px;"></span>
            <span id="view_menu_extras">
            </span>
        </div>  
        
        <div class="div_confirm_button" style="display:none;padding:5px;">
            <div class="row">
                <div class="col-md-6"><button type="button" role="button_save_0" style="width:100%;" onclick="save_confirm('editable','0');" class="btn btn-primary btn-lg">Save</button></div>
                <div class="col-md-6"><button type="button" role="button_save_1" style="width:100%;" onclick="save_confirm('editable','1');" class="btn btn-primary btn-lg">Save(CONFIRM)</button></div>
            </div>      
        </div>
        <!--<br>
        <div align="center"><span id="back_to_top" style="font-size:2px;cursor:pointer;">back tp top</span></div>
        <br>-->
        
       
    </div>
    <!-- /.container -->
    
    
</body>
</html>
    
<script type="text/javascript">
var win_width = window.innerWidth;
var win_height = window.innerHeight;
var ajax_request = null;
var date_obj = new Date();
var date = date_obj.getDate(); var month = (date_obj.getMonth())+1; var year = date_obj.getFullYear();


$(document).ready(function(){
    
    $("body").fadeIn(2000);
    push_year_filter(); push_month_filter($("#filter_fiscal_year").val());
    

});
$("#filter_fiscal_month").change(function(){
    clear_table();
});
$("#filter_fiscal_year").change(function(){
    push_month_filter(this.value); 
    clear_table();
});
$("#filter_company").change(function(){
    clear_table();
});
function clear_table(){
    $("#show_view_gl").html('no data');
    $("#query_time").html('');
    $("div.div_confirm_button").hide();
}
function push_year_filter(){

    var  start_year = 2017;
    while(start_year<=year){
        $("#filter_fiscal_year").append('<option value="'+start_year+'">'+start_year+'</option>');
        start_year++;
    }
    $("#filter_fiscal_year").val(year);
}
function push_month_filter(p_year){

    /*if(p_year==2016){
        var start_month = 10;
        $("#filter_fiscal_month").find("option").each(function(){
            if(this.value<start_month){
                $(this).attr("disabled","disabled");
            }
        });
        if(month>=10){$("#filter_fiscal_month").val(month);}else{$("#filter_fiscal_month").val(10);}     
    }else{
        $("#filter_fiscal_month").find("option").removeAttr("disabled");
    */
        $("#filter_fiscal_month").val(month);
    //}
}
function formatNumber(num,dec,thou,pnt,curr1,curr2,n1,n2) {var x = Math.round(num * Math.pow(10,dec));if (x >= 0) n1=n2='';var y = (''+Math.abs(x)).split('');var z = y.length - dec; if (z<0) z--; for(var i = z; i < 0; i++) y.unshift('0'); if (z<0) z = 1; y.splice(z, 0, pnt); if(y[0] == pnt) y.unshift('0'); while (z > 3) {z-=3; y.splice(z,0,thou);}var r = curr1+n1+y.join('')+n2+curr2;return r;}
function save_confirm(upload_or_editable,status_last){
    var arr_to_save = [];
    var r=confirm("Confirm to saving data");
    if(r==true){
        var confirm_save = true;
        $("#table_show_gl tr").each(function(){
            var tr_role=$(this).attr("tr-role");
            if(tr_role!="data"){}
            else{ /// tr-role = data;
                //var count_tbody_td = 0;
                $(this).find('td[role-td=td_to_save]').each(function(){
                    //if(count_tbody_td>1){
                        var head_com = $(this).attr("head-com");
                        var gl_id    = $(this).attr("gl-id");
                        var this_com = $(this).attr("this-com");
                        var this_deb_crd = $(this).attr("deb-crd-type");
                        var type_input = $(this).attr("type-input");
                        var old_id = $(this).attr("old-id");
                       
                        if(upload_or_editable=="editable"){var revision = $(this).attr("revision");}else{var revision = null;}

                        var check_input = $(this).find(".input_balance");
                        if(check_input.length > 0) {
                            if(upload_or_editable=="editable"){
                                var html_input = $.trim($(check_input).val());
                            }else{var html_input = $.trim($(check_input).html());}
                           
                            if(html_input!=""){
                                var dot = count_dot(html_input,'\\.');
                                if(dot>1){confirm_save=false;}
                            }
                        }else{
                            var html_input=null;
                        }
                        arr_to_push = [head_com,this_com,$("#filter_fiscal_month").val(),$("#filter_fiscal_year").val(),$("#hidden_user_id").val(),gl_id,html_input,this_deb_crd,type_input,old_id,revision];
                             arr_to_save.push(arr_to_push);
                    //}
                    //count_tbody_td++;
                });
            }
        });
        if(confirm_save==false)
            {alert("Error Numneric");}
        else{
            console.log(arr_to_save.length);
            console.log(arr_to_save);
            $.ajax({
                url: "save/save_confirm_view_gl_by_com.php",
                async: true,
                dataType: "text",
                type: "post",
                data: {"upload_or_editable":upload_or_editable,"status_last":status_last,"user_company_code":$("#filter_company option:selected").text(),"user_company_id":$("#filter_company option:selected").val(),"year":$("#filter_fiscal_year").val(),"month":$("#filter_fiscal_month").val(),"arr_to_save":arr_to_save},
                beforeSend: function(){
                    $("select").attr("disabled","disabled");
                    $("button[role=button_save_0],button[role=button_save_1]").attr("disabled","disabled");
                    $.isLoading({ text:"Saving Data : Please Wait",position:"overlay"});
                    
                },
                success: function (result) {
                   if(result==""){
                    alert("Saveing Successful");     
                   }else{
                    alert(result);
                   }
                    $.isLoading("hide");
                    $("select,button[role=button_save_0],button[role=button_save_1]").removeAttr("disabled");  
                    show_view_editable();
                }
            }); 
        
        }      
    }
}

function count_dot(s1, letter) {
    return ( s1.match( RegExp(letter,'g') ) || [] ).length;
}
function ajax_abort(){
    if(ajax_request!=null){
        ajax_request.abort();
    }
}
function show_view_editable(){
    ajax_abort();
    get_status_lock();
    var timestamp_start = null;
    var timestamp_stop = null;
    if($("#filter_company").val()==""){alert("กรุณาเลือกบริษัท"); return false;}
    ajax_request = $.ajax({
            url: "get_data/get_html_view_editable.php",
            async: true,
            dataType: "text",
            type: "post",
            data: {"user_company_code":$("#filter_company option:selected").text(),"user_company_id":$("#filter_company option:selected").val(),"year":$("#filter_fiscal_year").val(),"month":$("#filter_fiscal_month").val()},
            beforeSend: function(){
                $("div.div_confirm_button").hide();
                $("#query_time").html(''); 
                timestamp_start = new Date().getTime();
                $("#show_view_gl").html('');
                $.isLoading({ text:"Loading Editable Data",position:"overlay"});
                $("table.table_filter select").attr("disabled","disabled"); 
            },
            success: function (result) {
                $("#show_view_gl").html(result);
                $.isLoading("hide");
                $("table.table_filter select").removeAttr("disabled"); 
               
                timestamp_stop = new Date().getTime();
                $("#query_time").html('Query Time : '+((timestamp_stop-timestamp_start)/1000)+' sec');
                $('input.input_balance').bind('keypress',function(e){return (e.which!=45&&e.which!=8&&e.which!=0&&(e.which<46||e.which>57||e.which==47))?false:true;});
                $(".input_balance").change(function(){
                    
                    $(this).closest("td").attr("type-input","manual"); 
                    var this_com = $(this).attr("this-com"); 
                    var this_gl_group = $(this).attr("gl-group");
                    var sum_change_total = 0;
                    $("#table_show_gl tr td[this-com="+this_com+"][gl-group="+this_gl_group+"]").each(function(){
                        var check_input = $(this).find("input.input_balance");
                        if(check_input.length > 0) {
                            if($.trim($(check_input).val())!=""){
                            var input_number = ($(check_input).val()).replace(",","");
                            var input_without_comma=parseFloat(input_number);
                            sum_change_total+=input_without_comma;
                            }
                        }
                    });
                    $("span.span_total[gl-group="+this_gl_group+"][com-id="+this_com+"]").html(formatNumber(sum_change_total,2,',','.','','','-',''));    
                });
                $('#table_show_gl').gridviewScroll({width: ((98*win_width)/100),height: get_height_of_view("upload"),freezesize: 2}); 
                
                if($("#hidden_user_id").val()=="987"&&$("#filter_company option:selected").val()=="10"){
                    console.log('hide button');
                    $("div.div_confirm_button").hide();
                }else{
                    console.log('show button');
                    $("div.div_confirm_button").show();
                }
            }
        }); 
}
function get_height_of_view(type_view){
    return ((win_height-(211+40)));
}
function get_status_lock(){
    var status_lock = null;
    $.ajax({
            url: "get_data/get_lock_status.php",
            async: false,
            dataType: "text",
            type: "post",
            data: {'year':$("#filter_fiscal_year option:selected").val(),'month':$("#filter_fiscal_month option:selected").val(),'com_id':$("#filter_company").val(),'com_code':$("#filter_company option:selected").text()},
            beforeSend: function(){
               
            },
            success: function (result) {
                 status_lock = result;
            }
    }); 
    console.log(status_lock);
    if(status_lock=="1"){
        alert("การคอนเฟิร์มถูกล็อคโดย PC");
        $("button[role=button_save_0],button[role=button_save_1],button[role=button_upload_0],button[role=button_upload_1]").attr("disabled","disabled").css("background-color","#CCCCCC").css("color","#000");
    }else{
        $("button[role=button_save_0],button[role=button_save_1],button[role=button_upload_0],button[role=button_upload_1]").removeAttr("disabled").css("background-color","#337ab7").css("color","#FFF");
    }
}
</script>
<style type="text/css">
.disable_a_link{
    pointer-events: none;
    cursor: default;
}

</style>
