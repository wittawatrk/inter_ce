<?php
require_once 'SSO/SSO.php'; // นำเข้าไฟล์ Library
require_once 'function.php'; 

$APP_ID = 1610281337; // ไอดีของแอพพลิเคชั่น

// ตรวจสอบการล็อกอิน
$sso = new SSO($APP_ID);
$ssoResponse = $sso->getAuthentication();

$personDetail = $ssoResponse['personDetail']; // ข้อมูลพนักงาน
$panelLogout = $ssoResponse['panelLogout']; // html code แสดงปุ่มออกจากระบบ

// แสดงข้อมูล
echo $panelLogout;
//echo "<hr>";
//echo "<br>";
//var_dump($personDetail);
//echo $personDetail['CompanyID'];
$class_q_local = new Query_local();

//$personDetail['CompanyCode'] = "CI";
$arr_com_id=$class_q_local->query_table("select company_id from company where company_code='".$personDetail['CompanyCode']."'");

if(!is_array($arr_com_id)){exit();}else{
   $company_id = $arr_com_id[0]['company_id'];
   if($company_id==0||$company_id==NULL){exit();}
}

?>
<?php
    //if(trim($personDetail['CompanyCode'])=="PC"){
        ?>
        <script type="text/javascript">
            //window.location = "index_mc_confirm.php";
        </script>
        <?php
    //}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Intercompany Eliminations System</title>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/small-business.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="datatable/datatable.bootstrap.css">
    <link type="text/css" rel="stylesheet" href="datepicker/datepicker3.css" media="screen" />
    <link href="dialog/css/black-tie/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="css/isloading.css" rel="stylesheet">
    <link rel='stylesheet' type='text/css'href='timepicker/css/timepicki.css'/>
    <link rel='stylesheet' type='text/css'href='css/GridviewScroll.css'/>

    <script src="js/jquery.js"></script> 
    <script src="js/bootstrap.min.js"></script>
    <script src="dialog/js/jquery-ui-1.9.2.custom.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.th.js"></script>
    <script type='text/javascript'src='timepicker/js/timepicki.js'></script>
    <script type="text/javascript" language="javascript" src="datatable/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.tableTools.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.bootstrap.js"></script>
    <script type='text/javascript'src='js/jquery.isloading.js'></script>
    <script type="text/javascript" language="javascript" src="js/gridviewScroll.min.js"></script>
</head>

<body>
<input type="hidden" id="hidden_user_company" value="<?=trim($personDetail['CompanyCode']);?>">
<input type="hidden" id="hidden_user_company_id" value="<?=trim($company_id);?>">
<input type="hidden" id="hidden_user_id" value="<?=trim($personDetail['UserID']);?>">
<input type="hidden" id="hidden_user_email" value="<?=trim($personDetail['ExtEmail']);?>">
    <!-- Navigation -->
    <nav role="navigation" style="background-color:#FFFFFF;border-bottom:2px solid;padding:5px;margin-bottom:10px;height:60px;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">  
                <a class="navbar-brand" href="#">
                    <img class="img_ja" src="img/LOGO_ART_PRECISE.png" width="220" height="40" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div style="float:right;" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                   $class_general = new general_function();
                   echo $class_general->get_menu(basename(__FILE__));
                ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Total 70px -->
    <!-- Page Content -->
    <div class="container" style="background-color:#FFFFFF;width:100%;">
        
        <div class="filter_header" style="height:90px;">
            <table width="100%" class="table_filter">
            <tr>
                    <td><div class="header_form">AR/AP Balance List</div></td>
            </tr> 
            </table>
            <table width="85%" class="table_filter">                 
                <tr>
                    <td width="4%" style="display:none;"><input type="radio"  name="check_type_filter" value="1"   checked></td>
                    <td width="10%" align="right">Month * : </td>
                    <td width="20%" >
                        <select id="filter_fiscal_month" class="form-control" style="width:220px;">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </td>
                    <td width="10%" align="right" rowspan="2">Year * : </td>
                    <td width="20%" rowspan="2">
                        <select id="filter_fiscal_year" class="form-control" style="width:220px;">
                        </select>
                    </td>
                    <td width="10%" align="right" rowspan="2">Company : </td>
                    <td width="20%" rowspan="2">
                        <?php 
                            $arr_table_com=$class_q_local->query_table("select * from company order by order_no");
                            if(is_array($arr_table_com)&&sizeof($arr_table_com)>0){
                                $i=0; $option = '<option value="">Choose Company</option>';
                                while($i<sizeof($arr_table_com)){ 
                                    $com_code_option = trim($arr_table_com[$i]["company_code"]); $com_id_option = trim($arr_table_com[$i]["company_id"]);
                                    if(array_search($com_code_option,$personDetail['CompanyAllowed'])!==FALSE){
                                        
                                        if(sizeof($personDetail['CompanyAllowed'])==1){
                                            if(trim($com_code_option)==trim($personDetail['CompanyCode'])){$selected="selected";}else{$selected=NULL;}
                                        }else{
                                            $selected=NULL;
                                        }

                                        $option.='<option value="'.$com_id_option.'" '.$selected.'>'.$com_code_option.'</option>';
                                    }
                                    $i++;
                                }
                            }else{
                                $option = '<option value="">Choose Company</option>';
                            }   
                        ?>
                        <select id="filter_company" class="form-control" style="width:200px;">
                            <?php
                                echo $option;
                            ?>
                        </select>
                    </td>
                    <td width="10%" rowspan="2">
                        <input type="button" id="buttton_search" onclick="show_view_gl_compare();" value="Search" class="btn btn-primary btn-sm">
                    </td>
                </tr>
                <tr style="display:none;">
                    <td width="4%"><input type="radio" name="check_type_filter" value="2"></td>
                    <td width="10%" align="right">From-To * : </td>
                    <td width="20%" >
                        <select id="filter_fiscal_month_start" class="form-control" style="width:110px;float:left;">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        
                        <select id="filter_fiscal_month_end" class="form-control" style="width:110px;float:left;">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </td>
                </tr>
            </table>
            
        </div><br><br>
        <!-- style="overflow-x:scroll;" -->
        <div id="show_view_gl">
            no data
        </div>
        <div>
            <span id="query_time" style="font-size:2px;height:5px;display:none;"></span>
            <span id="view_menu_extras" style="display:none;">
            <img id="img_export_excel" title="Export Excel" style="float:right;cursor:pointer;margin-right:2%;margin-top:5px;" width="15" height="15" src="img/excel.png" onclick="fnExcelReport('table_show_compare');">
            <img id="hidden_company_set" title="Show/Hide Columns" style="float:right;cursor:pointer;margin-right:2%;margin-top:5px;" width="15" height="15" src="img/hidden.png" onclick="hidden_company_set_open_dialog();">
            </span>
        </div>  
        <!--<br>
        <div align="center"><span id="back_to_top" style="font-size:2px;cursor:pointer;">back tp top</span></div>
        <br>-->
        <div id="dialog_all">
                    <div id="dialog_zoom_trans" title="Transaction : ">
                        <div id="dialog_html_zoom_trans" ></div>
                    </div>
                    <div id="dialog_company_hidden_set" title="Show Column">
                        <div id="dialog_html_company_hidden_set" style="display:none;">
                            <table border="0"  width="100%" style="font-size:12px;">
                            <tr>
                                <td align="center"><input class="hidden_company_checkbox_all" type="checkbox" checked></td><td align="center">All</td>
                            </tr>
                            <?php
                            $i=0;
                            while($i<sizeof($arr_table_com)){ 
                                    $com_id_option = trim($arr_table_com[$i]["company_id"]); $com_code_option = trim($arr_table_com[$i]["company_code"]); 
                                    echo '<tr>';
                                        echo '<td align="center">'; 
                                            echo '<input class="hidden_company_checkbox" type="checkbox" com-id="'.$com_id_option.'" checked>'; 
                                        echo '</td>';
                                        echo '<td align="center">';  
                                            echo $com_code_option; 
                                        echo '</td>';
                                    echo '</tr>';
                                    $i++;
                            }
                            ?>
                            </table>
                        </div>
                    </div>
                    <div id="dialog_company_lock" title="Company Lock/Unlock">
                        <div id="dialog_html_company_lock" ></div>
                    </div>
        </div>
       <form id="form_excel" action="" method="POST">
            <input type="hidden" id="hidden_year_excel" name="hidden_year_excel">
            <input type="hidden" id="hidden_month_excel" name="hidden_month_excel">
            <input type="hidden" id="hidden_company_excel" name="hidden_company_excel">
            <input type="hidden" id="hidden_companycode_excel" name="hidden_companycode_excel">
       </form>
    </div>
    <!-- /.container -->
    
</body>
</html>
    
<script type="text/javascript">
var win_width = window.innerWidth;
var win_height = window.innerHeight;
var ajax_request = null;
var date_obj = new Date();
var date = date_obj.getDate(); var month = (date_obj.getMonth())+1; var year = date_obj.getFullYear();
//var year = 2017;
//var month = 1;
$( "#dialog_zoom_trans" ).dialog({
            autoOpen: false,
            width: ((win_width*95)/100),
            height: ((win_height*80)/100),
            position: [((win_width/2)-(((win_width*95)/100)/2)),((win_height/2)-(((win_height*80)/100)/2))],
            resizable: true,
            buttons: [   
            {
                text: "EXCEL",
                click: function(){
                    fnExcelReport('table_trans_inner_view_by_com');
                }
            }
            ,
            {
                text: "CLOSE",
                click: function() {      
                   $("#dialog_zoom_trans").dialog("close");
              }
            }
          ]
});
$( "#dialog_company_hidden_set" ).dialog({
            autoOpen: false,
            width: ((win_width*30)/100),
            height: ((win_height*80)/100),
            position: [((win_width/2)-(((win_width*30)/100)/2)),((win_height/2)-(((win_height*80)/100)/2))],
            resizable: true,
            buttons: [  
            {
                text: "OK",
                click: function() {      
                   hidden_company_set_reset_view();
                   $( "#dialog_company_hidden_set" ).dialog("close");
              }
            }, 
            {
                text: "CLOSE",
                click: function() {      
                   $( "#dialog_company_hidden_set" ).dialog("close");
              }
            }
          ]
});
$(document).ready(function(){
    $("body").fadeIn(2000);
    push_year_filter(); push_month_filter($("#filter_fiscal_year").val()); 
});

$("#filter_fiscal_month").change(function(){
    clear_view();
});
$("#filter_fiscal_year").change(function(){
    push_month_filter(this.value); 

    clear_view();
});
$("#filter_company,#filter_fiscal_month_start,#filter_fiscal_month_end").change(function(){
    clear_view();
});
$("input.hidden_company_checkbox_all").click(function(){
    if($(this).prop("checked")==true){
        $("input.hidden_company_checkbox").prop("checked",true);
    }else{
        $("input.hidden_company_checkbox").not("input.hidden_company_checkbox[com-id="+$("#filter_company option:selected").val()+"]").prop("checked",false);
    }
});
$("input.hidden_company_checkbox").click(function(){
    if($(this).prop("checked")==true){
        var all_check = true;
        $("input.hidden_company_checkbox").each(function(){
                if($(this).prop("checked")==false){all_check = false;}
        });
        $("input.hidden_company_checkbox_all").prop("checked",all_check);
    }else{
        $("input.hidden_company_checkbox_all").prop("checked",false);
    }
});
function push_year_filter(){
    var  start_year = 2017;
    while(start_year<=year){
        $("#filter_fiscal_year").append('<option value="'+start_year+'">'+start_year+'</option>');
        start_year++;
    }
    $("#filter_fiscal_year").val(year);
}
function push_month_filter(p_year){
    $("#filter_fiscal_month,#filter_fiscal_month_start,#filter_fiscal_month_end").val(month);
}
function formatNumber(num,dec,thou,pnt,curr1,curr2,n1,n2) {var x = Math.round(num * Math.pow(10,dec));if (x >= 0) n1=n2='';var y = (''+Math.abs(x)).split('');var z = y.length - dec; if (z<0) z--; for(var i = z; i < 0; i++) y.unshift('0'); if (z<0) z = 1; y.splice(z, 0, pnt); if(y[0] == pnt) y.unshift('0'); while (z > 3) {z-=3; y.splice(z,0,thou);}var r = curr1+n1+y.join('')+n2+curr2;return r;}

function count_dot(s1, letter) {
    return ( s1.match( RegExp(letter,'g') ) || [] ).length;
}
function ajax_abort(){
    if(ajax_request!=null){
        ajax_request.abort();
    }
}
function clear_view(){
    $("ul.nav.nav-tabs,span#view_menu_extras,span#query_time").hide();
    $("#show_view_gl").html('no data');
}
function set_lock_hidden_view(){
    $("#dialog_company_hidden_set").find("input.hidden_company_checkbox").each(function(){
        $(this).prop("checked",true).removeAttr("disabled");    
    });
    $("#dialog_company_hidden_set").find("input.hidden_company_checkbox[com-id="+$("#filter_company option:selected").val()+"]").attr("checked","checked").attr("disabled","disabled");
}
function show_view_gl_compare(){
    ajax_abort();
    if($("input[name=check_type_filter]:checked").val()=="2"){if($("#date_start").val()==""||$("#date_end").val()==""){alert("กรุณาเลือกช่วงเดือน"); return false;}}
    var timestamp_start = null;
    var timestamp_stop = null;
    $("#view_menu_extras,ul.nav.nav-tabs").show(); //set_lock_hidden_view();
    ajax_request = $.ajax({
            url: "get_data_other_report/get_html_view_ar_balance_list.php",
            async: true,
            dataType: "text",
            type: "post",
            data: {"user_company_code":$("#filter_company option:selected").text(),"user_company_id":$("#filter_company option:selected").val(),"year":$("#filter_fiscal_year").val(),"month":$("#filter_fiscal_month").val(),"array_company_hidden":get_check_box_hidden(),"check_type_filter":$("input[name=check_type_filter]:checked").val(),"month_start":$("#filter_fiscal_month_start option:selected").val(),"month_end":$("#filter_fiscal_month_end option:selected").val()},
            beforeSend: function(){
                timestamp_start = new Date().getTime();
                $("#show_view_gl,#query_time").html(''); 
                $("#show_view_gl").isLoading({ text:"Loading View",position:"overlay"});
                $("table.table_filter select,table.table_filter input").attr("disabled","disabled"); 
            },
            success: function (result) {
                timestamp_stop = new Date().getTime();
                $("#show_view_gl").html(result);
                $("#show_view_gl").isLoading("hide");
                $("table.table_filter select,table.table_filter input").removeAttr("disabled"); 
                $("#query_time").html('Query Time : '+((timestamp_stop-timestamp_start)/1000)+' sec').show();
                // $("span[status=normal],span[status=blink]").click(function(){
                //     if($.trim($(this).html())!=""){
                //         get_html_zoom_trans_by_com($(this).attr("span-side"),$(this).attr("head-com"),$(this).attr("head-com-code"),$(this).attr("gl-id"),$(this).attr("this-com"),$(this).attr("this-com-code"),$(this).attr("deb-crd-type"));
                //     }
                    
                // });
                $('#table_show_compare').gridviewScroll({width: ((98*win_width)/100),height: get_height_of_view("compare"),freezesize: 2, headerrowcount: 2  }); 
            }
        }); 
}
function get_html_zoom_trans_by_com(role_td,head_com,head_com_code,gl_id,this_com,this_com_code,deb_crd_type){
    console.log(this_com);
    $.ajax({
            url: "get_data_other_report/get_html_view_transaction_ar_balance_list.php",
            async: true,
            dataType: "text",
            type: "post",
            data: {"role_td":role_td,"head_com":head_com,"head_com_code":head_com_code,"gl_id":gl_id,"this_com":this_com,"this_com_code":this_com_code,"year":$("#filter_fiscal_year").val(),"month":$("#filter_fiscal_month").val(),"deb_crd_type":deb_crd_type,"check_type_filter":$("input[name=check_type_filter]:checked").val(),"month_start":$("#filter_fiscal_month_start option:selected").val(),"month_end":$("#filter_fiscal_month_end option:selected").val()},
            beforeSend: function(){
                $("#dialog_html_zoom_trans").html('');
                $("#dialog_zoom_trans").dialog("open");
                $("#dialog_html_zoom_trans").isLoading({ text: "Loading Transaction",position:"overlay"}); 
            },
            success: function (result) {
                console.log(result);
                $("#dialog_html_zoom_trans").isLoading("hide");  
                $("#dialog_html_zoom_trans").html(result);
                
            }
    }); 
}

function get_height_of_view(type_view){
        return ((win_height-(211+64)));
}

function hidden_company_set_open_dialog(){
    $( "#dialog_html_company_hidden_set" ).fadeIn(300);
    $( "#dialog_company_hidden_set" ).dialog("open");
}
function hidden_company_set_reset_view(){
    if($('#nav-bycom').attr('class')=="active"){show_view_gl();}
    else if($('#nav-compare-accum').attr('class')=="active"){show_view_gl_compare_accum();}
    else if($('#nav-editable').attr('class')=="active"){show_view_editable();}
    else{show_view_gl_compare();}
}
function get_check_box_hidden(){
    var array_check_hidden = [];
    $("#dialog_company_hidden_set").find("input.hidden_company_checkbox").each(function(){
        if($(this).prop("checked")==true){
            array_check_hidden.push($(this).attr("com-id"));
        }
    });
    return array_check_hidden;
}

function fnExcelReport(table_id)
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById(table_id); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}



</script>
<style type="text/css">
.disable_a_link{
    pointer-events: none;
    cursor: default;
}

</style>
