<?php
require_once 'SSO/SSO.php'; // นำเข้าไฟล์ Library
require_once 'function.php'; 

$APP_ID = 1610281337; // ไอดีของแอพพลิเคชั่น

// ตรวจสอบการล็อกอิน
$sso = new SSO($APP_ID);
$ssoResponse = $sso->getAuthentication();

$personDetail = $ssoResponse['personDetail']; // ข้อมูลพนักงาน
$panelLogout = $ssoResponse['panelLogout']; // html code แสดงปุ่มออกจากระบบ

// แสดงข้อมูล
echo $panelLogout;
//echo "<hr>";
//echo "<br>";
//var_dump($personDetail);
//echo $personDetail['CompanyID'];
$class_q_local = new Query_local();

//$personDetail['CompanyCode'] = "CI";
$arr_com_id=$class_q_local->query_table("select company_id from company where company_code='".$personDetail['CompanyCode']."'");

if(!is_array($arr_com_id)){exit();}else{
   $company_id = $arr_com_id[0]['company_id'];
   if($company_id==0||$company_id==NULL){exit();}
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Summary By GL </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="http://oss.sheetjs.com/js-xlsx/xlsx.full.min.js"></script>
    <script type="text/javascript" src="./select/jquery.sumoselect.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="./node_modules/jquery.tabulator/dist/js/tabulator.min.js"></script>
    <script type="text/javascript" src="./js/datePicker.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link href="./node_modules/jquery.tabulator/dist/css/bootstrap/tabulator_bootstrap4.min.css" rel="stylesheet">
    <link href="./select/sumoselect.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>
</head>

<body>
    <input type="hidden" id="hidden_user_company" value="<?=trim($personDetail['CompanyCode']);?>">
    <input type="hidden" id="hidden_user_company_id" value="<?=trim($company_id);?>">
    <input type="hidden" id="hidden_user_id" value="<?=trim($personDetail['UserID']);?>">
    <input type="hidden" id="hidden_user_email" value="<?=trim($personDetail['ExtEmail']);?>">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img class="img_ja" src="img/LOGO_ART_PRECISE.png" width="220" height="40" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">View</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Upload
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="upload_erp.php">
                            <i class="icon-list"></i> Upload From ERP</a>
                        <a class="dropdown-item" href="upload_edit.php">
                            <i class="icon-list"></i> Editable</a>
                </li>
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Others Report
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="view_report.php">
                            <i class="icon-list"></i>Consolidate Report for PC</a>
                        <a class="dropdown-item" href="other_report_get_by_revenue.php">
                            <i class="icon-list"></i>Consolidate Report for RS</a>
                        <a class="dropdown-item active" href="summary_by_gl_code.php">
                            <i class="icon-list"></i>Summary by GL </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Setting
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="setting_gl.php">
                            <i class="icon-list"></i> Setting GL</a>
                        <a class="dropdown-item" href="setting_customer.php">
                            <i class="icon-list"></i> Setting Customer/Supplier</a>
                        <a class="dropdown-item" href="setting_bs.php">
                            <i class="icon-list"></i> Setting Starting Balance</a>
                </li>
            </ul>
            </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-8 mt-3" style="margin: auto">

                <div class="row">
                    <div class="card mb-2">
                        <div class="card-header">
                            ค้นหา
                        </div>
                        <div class="card-body">
                            <table width="100%" class="table_filter">

                                <tr>
                                    <th>From - To:</th>
                                    <td>
                                        <select id="filter_fiscal_month_start" class="form-control">
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="filter_fiscal_month_end" class="form-control">
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </td>
                                    <th width="10%" align="right">Year * : </th>
                                    <td>
                                        <select id="filter_fiscal_year" class="form-control" style="width:220px;">
                                        </select>
                                    </td>
                                </tr>
                                <tr>

                                    <th>Company * :  </th>
                                    <td colspan="2">
                                        <?php
                           $arr_table_com=$class_q_local->query_table("select * from company order by order_no");
                           if(is_array($arr_table_com)&&sizeof($arr_table_com)>0){
                               $i=0; $option = '';
                               while($i<sizeof($arr_table_com)){
                                   $com_code_option = trim($arr_table_com[$i]["company_code"]); $com_id_option = trim($arr_table_com[$i]["company_id"]);
                                   if(array_search($com_code_option,$personDetail['CompanyAllowed'])!==FALSE){

                                       if(sizeof($personDetail['CompanyAllowed'])==1){
                                           if(trim($com_code_option)==trim($personDetail['CompanyCode'])){$selected="selected";}else{$selected=NULL;}
                                       }else{
                                           $selected=NULL;
                                       }

                                       $option.='<option value="'.$com_id_option.'" '.$selected.'>'.$com_code_option.'</option>';
                                   }
                                   $i++;
                               }
                           }else{
                             
                           }
                       ?>
                                            <select id="filter_company" multiple="multiple">
                                                <?php  echo $option;  ?>
                                            </select>
                                    </td>
                                    <td colspan="2">
                                        <button type="button" class="btn btn-primary btn-sm" style="float:right" onclick="search();">Search</button>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-md-10 mt-3" style="margin: auto">
                <div class="row">
                    <button type="button" class="btn btn-success mb-3" id="export">Export Excel</button>
                </div>
                <div class="row">

                    <div id="example-table"></div>
                </div>

            </div>
        </div>



    </div>



</body>

</html>

    <script>
        var date_obj = new Date();
        var date = date_obj.getDate();
        var month = (date_obj.getMonth()) + 1;
        var year = date_obj.getFullYear();
        $(document).ready(function () {
            $('#filter_company').SumoSelect({placeholder: 'Select Company',selectAll:true});
            push_year_filter();

           

        })

        function push_year_filter() {
            var start_year = 2017;
            while (start_year <= year) {
                $("#filter_fiscal_year").append('<option value="' + start_year + '">' + start_year + '</option>');
                start_year++;
            }
            $("#filter_fiscal_year").val(year);
        }

        function search() {
            if($('#filter_company').val()=='')
            {
                        alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
                        console.log("error")
                        return false;
                    }
            $.ajax({
                url: './get_data_other_report/get_data_summary_by_gl_code.php',
                type: 'POST',
                data: {
                    "year": $('#filter_fiscal_year').val(),
                    "smonth": $('#filter_fiscal_month_start').val(),
                    "emonth": $('#filter_fiscal_month_end').val(),
                    "cmp": $('#filter_company').val()
                },
                dataType: 'JSON',
                success: function (data) {
                    if(!data){
                        alert(ไม่พบข้อมูล)
                    }
                    $("#example-table").tabulator({
                height: 600, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
                layout: "fitColumns", //fit columns to width of table (optional)
                groupBy: ["company_code","group_name","gl_des"],
                columns: [ //Define Table Columns
                    {
                        title: "Group Gl",
                        field: "group_name",
                        align: "left",
                        headerFilter: true,
                        headerFilterPlaceholder:"Group Gl filter"
                    },
                    {
                        title: "Company",
                        field: "company_code",
                        align: "center",
                        headerFilter: true,
                        headerFilterPlaceholder:"Company filter"
                    },
                    {
                        title: "Partner",
                        field: "partner",
                        align: "center",
                        headerFilter: true,
                        headerFilterPlaceholder:"Company filter"
                    },
                    {
                        title: "GL_Code",
                        field: "glcode",
                        width: 150,
                        headerFilter: true,
                        headerFilterPlaceholder:"Gl_Code filter"
                    },
                    {
                        title: "GL_Name",
                        field: "gl_des",
                        align: "left",
                        headerFilter: true,
                        headerFilterPlaceholder:"Gl Name filter"
                    },
                    {
                        title: "Type",
                        field: "type",
                        align: "center",
                        headerFilter: "select",
                        headerFilterParams: {
                            "P/L": "P/L",
                            "B/S": "B/S",
                            "": "All"
                        },
                    },
                    {
                        title: "Amount",
                        field: "amount",
                        formatter:"money",
                        align: "right",
                    },

                   
                ]
               
            })
                    $("#example-table").tabulator("setData", data);
                }

            })
        }

        $("#export").click(function(){
        
            $("#example-table").tabulator("download", "xlsx", "dataGL .xlsx", {sheetName:"My Data"});
        })
    </script>
