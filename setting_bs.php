﻿<?php
require_once 'SSO/SSO.php'; // นำเข้าไฟล์ Library
require_once 'function.php';

$APP_ID = 1610281337; // ไอดีของแอพพลิเคชั่น

// ตรวจสอบการล็อกอิน
$sso = new SSO($APP_ID);
$ssoResponse = $sso->getAuthentication();

$personDetail = $ssoResponse['personDetail']; // ข้อมูลพนักงาน
$panelLogout = $ssoResponse['panelLogout']; // html code แสดงปุ่มออกจากระบบ

// แสดงข้อมูล
echo $panelLogout;
//echo "<hr>";
//echo "<br>";
//var_dump($personDetail);
//echo $personDetail['CompanyID'];
$class_q_local = new Query_local();

//$personDetail['CompanyCode'] = "CI";
$arr_com_id=$class_q_local->query_table("select company_id from company where company_code='".$personDetail['CompanyCode']."'");

if (!is_array($arr_com_id)) {
    exit();
} else {
    $company_id = $arr_com_id[0]['company_id'];
    if ($company_id==0||$company_id==null) {
        exit();
    }
}

?>
<?php
    //if(trim($personDetail['CompanyCode'])=="PC"){
        ?>
        <script type="text/javascript">
            //window.location = "index_mc_confirm.php";
        </script>
        <?php
    //}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Intercompany Eliminations System</title>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/small-business.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="datatable/datatable.bootstrap.css">
    <link type="text/css" rel="stylesheet" href="datepicker/datepicker3.css" media="screen" />
    <link href="dialog/css/black-tie/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="css/isloading.css" rel="stylesheet">
    <link rel='stylesheet' type='text/css'href='timepicker/css/timepicki.css'/>
    <link rel='stylesheet' type='text/css'href='css/GridviewScroll.css'/>

    <script src="js/jquery.js"></script> 
    <script src="js/bootstrap.min.js"></script>
    <script src="dialog/js/jquery-ui-1.9.2.custom.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.th.js"></script>
    <script type='text/javascript'src='timepicker/js/timepicki.js'></script>
    <script type="text/javascript" language="javascript" src="datatable/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.tableTools.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.bootstrap.js"></script>
    <script type='text/javascript'src='js/jquery.isloading.js'></script>
    <script type="text/javascript" language="javascript" src="js/gridviewScroll.min.js"></script>
</head>

<body>
<input type="hidden" id="hidden_user_company" value="<?=trim($personDetail['CompanyCode']);?>">
<input type="hidden" id="hidden_user_company_id" value="<?=trim($company_id);?>">
<input type="hidden" id="hidden_user_id" value="<?=trim($personDetail['UserID']);?>">
<input type="hidden" id="hidden_user_email" value="<?=trim($personDetail['ExtEmail']);?>">
    <!-- Navigation -->
    <nav role="navigation" style="background-color:#FFFFFF;border-bottom:2px solid;padding:5px;margin-bottom:10px;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">  
                <a class="navbar-brand" href="#">
                    <img class="img_ja" src="img/LOGO_ART_PRECISE.png" width="220" height="40" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div style="float:right;" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                   $class_general = new general_function();
                   echo $class_general->get_menu(basename(__FILE__));
                ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container" style="background-color:#FFFFFF;border:1px solid;border-radius:5px;min-height:500px;width:100%;">
    <?php if (array_search('PC', $personDetail['CompanyAllowed'])==false) {
                    echo "You Don't have permission to access this page";
                } else {
            ?>
        <div class="filter_header">
            <table width="100%" class="table_filter">
                <tr>
                    <td colspan="2" ><div class="header_form">Setting Ending Balace  AT 12/2017</div></td>
                </tr>               
                <!--<tr>
                    <td width="25%" align="right">Fiscal Year * : </td>
                    <td width="75%" >
                        <select id="filter_fiscal_year" class="form-control" style="width:200px;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="right">Select month of this Fiscal year * : </td>
                    <td>
                        <select id="filter_fiscal_month" class="form-control" style="width:200px;">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </td>
                </tr>-->
                <tr>
                    <td width="10%" align="right">Company : </td>
                    <td width="90%">
                    <?php 
                            $arr_table_com=$class_q_local->query_table("select * from company ");
            if (is_array($arr_table_com)&&sizeof($arr_table_com)>0) {
                $i=0;
                $option="";
                while ($i<sizeof($arr_table_com)) {
                    $com_code_option = trim($arr_table_com[$i]["company_code"]);
                    $com_id_option = trim($arr_table_com[$i]["company_id"]);
                    if (array_search($com_code_option, $personDetail['CompanyAllowed'])!==false) {
                        if (trim($com_code_option)==trim($personDetail['CompanyCode'])) {
                            $selected="selected";
                        } else {
                            $selected=null;
                        }
                        $option.='<option value="'.$com_id_option.'" '.$selected.'>'.$com_code_option.'</option>';
                    }
                    $i++;
                }
            } else {
                $option = '<option value="">Choose Company</option>';
            } ?>
                        <select id="filter_company" class="form-control" style="width:200px;">
                            <?php
                                echo $option; ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="div_confirm_button">
            <button type="button" id="confirm_save_start_bs" style="width:100%;" onclick="save_start_bs();" class="btn btn-primary btn-lg">SAVE</button>    
        </div>
        <br>
        <div id="show_starting_balance">
        
        </div>
        <img id="img_export_excel" title="Export Excel" style="float:right;cursor:pointer;margin-right:2%;margin-top:5px;" width="15" height="15" src="img/excel.png" onclick="export_excel_div();">
        <div><span id="query_time" style="font-size:2px;"></span></div>  
        <form id="form_excel" action="" method="POST">
            <input type="hidden" id="hidden_year_excel" name="hidden_year_excel">
            <input type="hidden" id="hidden_month_excel" name="hidden_month_excel">
            <input type="hidden" id="hidden_company_excel" name="hidden_company_excel">
            <input type="hidden" id="hidden_companycode_excel" name="hidden_companycode_excel">
       </form>
       <?php
        } ?>
    </div>
    <!-- /.container -->
    
</body>
</html>
    
<script type="text/javascript">
var win_width = window.innerWidth;
var win_height = window.innerHeight;
var date_obj = new Date();
var date = date_obj.getDate(); var month = (date_obj.getMonth())+1; var year = date_obj.getFullYear();
$(document).ready(function(){  
    $("body").fadeIn(2000);
    push_year_filter(); push_month_filter(); show_setting_balance();
});
$("#filter_fiscal_month").change(function(){
    show_setting_balance();
});
$("#filter_fiscal_year").change(function(){
    push_month_filter(); show_setting_balance();
});
$("#filter_company").change(function(){
    show_setting_balance();
});
$("#back_to_top").click(function(){
    $('html, body').animate({ scrollTop: 0 }, 'fast');
});
function formatNumber(num,dec,thou,pnt,curr1,curr2,n1,n2) {var x = Math.round(num * Math.pow(10,dec));if (x >= 0) n1=n2='';var y = (''+Math.abs(x)).split('');var z = y.length - dec; if (z<0) z--; for(var i = z; i < 0; i++) y.unshift('0'); if (z<0) z = 1; y.splice(z, 0, pnt); if(y[0] == pnt) y.unshift('0'); while (z > 3) {z-=3; y.splice(z,0,thou);}var r = curr1+n1+y.join('')+n2+curr2;return r;}
function push_year_filter(){
    var i = 0; var back_year = 5;
    while(i<back_year){
        var show_year = year-i;
        $("#filter_fiscal_year").append('<option value="'+show_year+'">'+show_year+'</option>');
        i++;
    }
}
function push_month_filter(){
    if($("#filter_fiscal_year").val()<year){ // ปีก่อนหน้า
        $("#filter_fiscal_month").find("option").removeAttr("disabled");
    }else{
        $("#filter_fiscal_month").find("option").each(function(){
            if(this.value>month){
                $(this).attr("disabled","disabled");
            }
        });
        $("#filter_fiscal_month").val(month);
    }
}
function show_setting_balance(){
    var timestamp_start = null;
    var timestamp_stop = null;
   
    $.ajax({
            url: "get_data/setting_get_start_balance_table.php",
            async: true,
            dataType: "text",
            type: "post",
            data: {"user_company_code":$("#filter_company option:selected").text(),"user_company_id":$("#filter_company option:selected").val()},
            beforeSend: function(){
                $("#query_time").html(''); 
                timestamp_start = new Date().getTime();
                $("#show_starting_balance").html('');
                $("#show_starting_balance").isLoading({ text:"Loading View",position:"overlay"});
                $("table.table_filter select").attr("disabled","disabled"); 
                $("#back_to_top").hide();
            },
            success: function (result) {
                $("#show_starting_balance").html(result);
                $("#show_starting_balance").isLoading("hide");
                $("table.table_filter select").removeAttr("disabled"); 
                $("#back_to_top").show();
                timestamp_stop = new Date().getTime();
                $("#query_time").html('Query Time : '+((timestamp_stop-timestamp_start)/1000)+' sec');
                //$('input.input_balance').bind('keypress',function(e){return (e.which!=45&&e.which!=44&&e.which!=8&&e.which!=0&&(e.which<46||e.which>57||e.which==47))?false:true;});
                
                
                /*var tableOffset = $("#table_show_start_bs").offset().top;
                var $header = $("#table_show_start_bs > thead").clone();
                var count_td = 0;
                var $fixedHeader = $("#header-fixed").append($header);
                $("#table_show_start_bs thead tr").find("td").each(function(){
                    var td_width = this.getBoundingClientRect().width;
                    $("#header-fixed thead tr td:eq("+count_td+")").css('width', td_width+'px');
                    count_td++;
                });

                $(window).bind("scroll", function() {var offset = $(this).scrollTop();
                    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {$fixedHeader.show();}
                    else if (offset < tableOffset) {$fixedHeader.hide();}
                });
                $(window).bind('resize', function() {
                    $("#header-fixed").html('');
                    $("#header-fixed").append($header);
                    var count_td = 0;
                    $("#table_show_start_bs thead tr").find("td").each(function(){
                        var td_width = this.getBoundingClientRect().width;
                        $("#header-fixed thead tr td:eq("+count_td+")").css('width', td_width+'px');
                        count_td++;
                    });
                });*/
                $('#table_show_start_bs').gridviewScroll({width: ((98*win_width)/100),height: get_height_of_view("upload"),freezesize: 2}); 
                $('input.input_balance').bind('keypress',function(e){return (e.which!=45&&e.which!=8&&e.which!=0&&(e.which<46||e.which>57||e.which==47))?false:true;});
            }
        }); 
}
function save_start_bs(){
    var array_save = [];
    $("#table_show_start_bs tbody tr").each(function(){
        $(this).find("td").each(function(){
                var check_balance=$(this).find("input.input_balance");
                if(check_balance.length>0){
                    var head_com = $(check_balance).attr("head-com-id");
                    var this_com = $(check_balance).attr("this-com-id");
                    var gl_id = $(check_balance).attr("gl-id");
                    var value_input=$.trim($(check_balance).val());
                    var old_id = $(check_balance).attr("old-id-balance");
                    //if($.trim(value_input)!=""){
                    var array_to_push = [head_com,this_com,gl_id,value_input,old_id];
                    array_save.push(array_to_push);
                    //}
                }            
        });
    });
    var r = confirm("Confirm to edit starting balance");
    if(r==true){
        $.ajax({
                url: "save/save_starting_balance_new.php",
                async: true,
                dataType: "text",
                type: "post",
                data: {"user_id":$("#hidden_user_id").val(),"array_save":array_save},
                beforeSend: function(){
                     $("select").attr("disabled","disabled");
                     $(".div_confirm_button").isLoading({ text:"Saving Data : Please Wait",position:"overlay"});
                },
                success: function (result) {
                     console.log(result);
                     alert("Saveing Successful");
                     $(".div_confirm_button").isLoading("hide");
                     $("select").removeAttr("disabled");     
                     show_setting_balance();  
                }
        });     
    }  
}
function get_height_of_view(type_view){
    //total 211
    if(type_view=="upload"){
        return ((win_height-(211+80)));
    }
}
function export_excel_div(){
    $("#hidden_year_excel").val($("#filter_fiscal_year").val());
    $("#hidden_month_excel").val($("#filter_fiscal_month").val());
    $("#hidden_company_excel").val($("#filter_company").val());
    $("#hidden_companycode_excel").val($("#filter_company option:selected").text());
    
    document.getElementById('form_excel').action = "excel/excel_export_start_balance.php";
    
    document.getElementById('form_excel').submit();
}
</script>
<style type="text/css">
.disable_a_link{
    pointer-events: none;
    cursor: default;
}

</style>

