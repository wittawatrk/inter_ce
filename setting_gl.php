<?php
require_once 'SSO/SSO.php'; // นำเข้าไฟล์ Library
require_once 'function.php'; 

$APP_ID = 1610281337; // ไอดีของแอพพลิเคชั่น

// ตรวจสอบการล็อกอิน
$sso = new SSO($APP_ID);
$ssoResponse = $sso->getAuthentication();

$personDetail = $ssoResponse['personDetail']; // ข้อมูลพนักงาน
$panelLogout = $ssoResponse['panelLogout']; // html code แสดงปุ่มออกจากระบบ

// แสดงข้อมูล
echo $panelLogout;
//echo "<hr>";
//echo "<br>";
//var_dump($personDetail);
//echo $personDetail['CompanyID'];
$class_q_local = new Query_local();
//$arr_table=$class_db->query_table("select * from datasource_mapping");
//print_r($arr_table);
/*
$stmt_query_insert=$conn_other_db->prepare("select * from Absences");
$stmt_query_insert->execute();
$arr_q = $stmt_query_insert->fetch( PDO::FETCH_ASSOC );

print_r($arr_q); //UserID 1078=chol // 1006 = dai //  1007 = pae
*/
//$personDetail['CompanyCode'] = "CI";
$arr_com_id=$class_q_local->query_table("select company_id from company where company_code='".$personDetail['CompanyCode']."'");

if(!is_array($arr_com_id)){exit();}else{
   $company_id = $arr_com_id[0]['company_id'];
   if($company_id==0||$company_id==NULL){exit();}
}




?>
<?php
	//if(trim($personDetail['CompanyCode'])=="PC"){
		?>
		<script type="text/javascript">
			//window.location = "index_mc_confirm.php";
		</script>
		<?php
	//}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Intercompany Eliminations System</title>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<input type="hidden" id="hidden_user_company" value="<?=trim($personDetail['CompanyCode']);?>">
<input type="hidden" id="hidden_user_company_id" value="<?=trim($company_id);?>">
<input type="hidden" id="hidden_user_id" value="<?=trim($personDetail['UserID']);?>">
<input type="hidden" id="hidden_user_email" value="<?=trim($personDetail['ExtEmail']);?>">
    <!-- Navigation -->
    <nav role="navigation" style="background-color:#FFFFFF;border-bottom:2px solid;padding:5px;margin-bottom:10px;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">  
                <a class="navbar-brand" href="#">
                    <img class="img_ja" src="img/LOGO_ART_PRECISE.png" width="220" height="40" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div style="float:right;" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            	<?php
                   $class_general = new general_function();
                   echo $class_general->get_menu(basename(__FILE__));
                ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container" style="background-color:#FFFFFF;border:1px solid;border-radius:5px;min-height:500px;padding:5px;">
    <?php
        $arr_table_gl=$class_q_local->query_table("select * from ie_grouping_gl");
    ?>
    <?php if (array_search('PC',$personDetail['CompanyAllowed'])==FALSE){
        echo "You Don't have permission to access this page"; }
        else {?>
    <table width="100%">
        <tr>
            <td><div class="header_form">Setting GL</div></td>
        </tr>  
    </table><br>
        <div class="filter_header">
            <table width="100%" class="table_filter">
                              
                <tr>
                    <td width="10%" align="left">Company : </td>
                    <td width="90%" >
                   <?php 
                            $arr_table_com=$class_q_local->query_table("select * from company ");
                            if(is_array($arr_table_com)&&sizeof($arr_table_com)>0){
                                $i=0; $option="";
                                while($i<sizeof($arr_table_com)){ 
                                    $com_code_option = trim($arr_table_com[$i]["company_code"]); $com_id_option = trim($arr_table_com[$i]["company_id"]);
                                    if(array_search($com_code_option,$personDetail['CompanyAllowed'])!==FALSE){
                                        if(trim($com_code_option)==trim($personDetail['CompanyCode'])){$selected="selected";}else{$selected=NULL;}
                                        $option.='<option value="'.$com_id_option.'" '.$selected.'>'.$com_code_option.'</option>';
                                    }
                                    $i++;
                                }
                            }else{
                                $option = '<option value="">Choose Company</option>';
                            }   
                    ?>
                        <select id="filter_company" class="form-control" style="width:200px;">
                            <?php
                                echo $option;
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table id="table_setting_gl" width="100%">
                <thead>
                    <tr>
                        <td>GL_ID</td>
                        <td width="5%">Group No.</td>
                        <td width="35%">GL</td>
                        <td width="5%">Type</td>
                        <td>Account ID/Description</td>
                        <td width="5%">Setting<br>Map Account</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <?php } ?>
        </div>
        <div id="dialog_all">
                    <div id="dialog_setting_gl" title="Setting Account">
                        <div id="dialog_html_setting_gl" ></div>
                    </div>
                    
        </div>
    </div>
    <!-- /.container -->

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/small-business.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="datatable/datatable.bootstrap.css">
    <link type="text/css" rel="stylesheet" href="datepicker/datepicker3.css" media="screen" />
    <link href="dialog/css/black-tie/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="css/isloading.css" rel="stylesheet">
   

    <script src="js/jquery.js"></script> 
    <script src="js/bootstrap.min.js"></script>
    <script src="dialog/js/jquery-ui-1.9.2.custom.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="datepicker/bootstrap-datepicker.th.js"></script>
    
    <script type="text/javascript" language="javascript" src="datatable/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.tableTools.js"></script>
    <script type="text/javascript" language="javascript" src="datatable/dataTables.bootstrap.js"></script>
    <script type='text/javascript'src='js/jquery.isloading.js'></script>
<script type="text/javascript">

    var win_width = window.innerWidth;
    var win_height = window.innerHeight;
    var table_setting_gl=$('#table_setting_gl').DataTable({"dom":'<<t>p>',"aaSorting":[0],"columnDefs": [{"targets": [0],"visible": false,"searchable": false}],"bPaginate": false,"bSort":false,"createdRow":function(row,data,index){/*$(row).find("td").first().addClass("td_head");$(row).find("td:not(:first)").addClass("td_data");*/}});
    $( "#dialog_setting_gl" ).dialog({
            autoOpen: false,
            width: ((win_width*95)/100),
            height: ((win_height*80)/100),
            position: [((win_width/2)-(((win_width*95)/100)/2)),((win_height/2)-(((win_height*80)/100)/2))],
            resizable: true,
            buttons: [
            {
                text: "SAVE",
                click: function() {
                    confirm_setting_account();
              }
            },
            {
                text: "CLOSE",
                click: function() {      
                   $("#dialog_setting_gl").dialog("close");
              }
            }
          ]
    });
    
$(document).ready(function(){
    show_table_gl();
});
$("#filter_company").change(function(){
    show_table_gl();
});

function show_table_gl(){ 
    $(".ui-dialog-content").dialog("close");
    $.ajax({
            url: "get_data/setting_get_table_gl_match_account.php",
            async: false,
            dataType: "json",
            type: "post",
            data: {"company_id":$("#filter_company option:selected").val()},
            beforeSend: function(){
                //$.isLoading({ text: "Loading",position:"overlay"}); 
            },
            success: function (result) {
                console.log(result);
                table_setting_gl.clear().draw();
                if(result.length!=undefined&&result[0]==true){
                    var arr_gl_all=result[1]; var i=0;
                    while(i<arr_gl_all.length){
                        var arr_row = arr_gl_all[i];
                        row_node=table_setting_gl.row.add([arr_row['atid'],arr_row['group_no'],arr_row['name'],arr_row['type'],arr_row['all_acc'],"<div align='center'><img class='setting_gl' gl-atid='"+arr_row['atid']+"' src='img/setting.png' width='25' height='25'></div>"]).order( [[ 0, 'asc' ]] ).draw().node(); 
                        i++;
                    }
     
                }else{
                   // console.log(result[1]);
                }
                $("img.setting_gl").click(function(){
                    var atid_gl = $(this).attr("gl-atid");
                    var des_title = $(this).closest('tr').find('td:eq(1)').html();
                    $('#dialog_setting_gl').dialog('option', 'title', 'Setting Account : '+des_title);
                    get_html_setting_gl(atid_gl);
                });
               // $.isLoading("hide");  
            }
        }); 
}
function get_html_setting_gl(gl_atid){
   
    $.ajax({
            url: "get_data/setting_get_form_match_account.php",
            async: true,
            dataType: "text",
            type: "post",
            data: {"company_id":$("#filter_company option:selected").val(),"company_text":$("#filter_company option:selected").text(),"gl_atid":gl_atid},
            beforeSend: function(){
                $("#dialog_html_setting_gl").html('');
                $("#dialog_setting_gl").dialog("open");
                $("#dialog_html_setting_gl").isLoading({ text: "Loading",position:"overlay"}); 
            },
            success: function (result) {
               // console.log(result);
                $("#dialog_html_setting_gl").isLoading("hide");  
                $("#dialog_html_setting_gl").html(result);
                
            }
    }); 
    
}
function confirm_setting_account(){
    var r=confirm("Confirm to save ?");
    if(r==true){
        var rowCount=table_add_match_gl.column(0).data().length; 
        var i=0; var arr_save = [];
        while(i<rowCount){
            var data_tr = table_add_match_gl.row(i).data();
            data_tr[3] = null;
            arr_save.push(data_tr);
            i++;
        }
        $.ajax({
                url: "save/save_match_account.php",
                async: true,
                dataType: "text",
                type: "post",
                data: {"arr_account":arr_save,"gl_atid":$("#hidden_gl_atid_to_match").val(),"company_id":$("#filter_company option:selected").val()},
                beforeSend: function(){
                    $.isLoading({ text: "Loading",position:"overlay"}); 
                },
                success: function (result) {
                    console.log(result);
                    $.isLoading("hide");  
                    $("#dialog_setting_gl").dialog("close");
                    show_table_gl();
                }
        }); 
        
    }
}
</script>
</body>
</html>
<style type="text/css">
#table_setting_gl{
    border:1px grey solid;
}
#table_setting_gl thead td{
    padding: 12px;
    background-color: #f6f6f6;
    border-bottom: 1px #adaead solid;
    /*font-weight: bold;*/
}
#table_setting_gl tbody td{
    padding: 8px;
    /*cursor:pointer;*/
    border-right:1px grey solid;
    border-bottom:1px grey solid;
}
img.setting_gl{
    cursor: pointer;
    border:2px solid #fff;
        -moz-box-shadow: 10px 10px 5px #ccc;
        -webkit-box-shadow: 10px 10px 5px #ccc;
        box-shadow: 2px 2px 5px #ccc;
        -moz-border-radius:4px;
        -webkit-border-radius:2px;
        border-radius:4px;
}
</style>