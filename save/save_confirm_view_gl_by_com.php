<?php
require_once('../function.php');
$q_local = new Query_local();
$q_domain = new Query_domain($_POST['user_company_code']);
$today = $class_date->get_date_today();
$year = $_POST['year'];
$month= $_POST['month'];
$com_user = $_POST['user_company_id'];
$erp_type =  $q_local->query_table("select top(1) db_type from company where company_id = $com_user");

$status_last = $_POST['status_last']; //// confirm หรือไม่
if($_POST['upload_or_editable']=="upload"){

		///////////////////////////////////////////// เช็คว่ามีการกรอกข้อมูลเดือนที่แล้วไปหรือยัง
		if(($year==2018&&$month==1)||($year==2017&&$month==12)){}
		else if($year!=2018&&$month==1){
			$check_lastest = $q_local->query_table("select * from ie_summary_lastest where year='".($year-1)."' and month='12' and company_id_user='".$com_user."'");
			if(!is_array($check_lastest)||(is_array($check_lastest)&&sizeof($check_lastest)==0)){echo 'กรุณาอย่ากรอกข้ามเดือน'; exit();}
		}else{
			$check_lastest = $q_local->query_table("select * from ie_summary_lastest where year='".$year."' and month='".($month-1)."' and company_id_user='".$com_user."'");
			if(!is_array($check_lastest)||(is_array($check_lastest)&&sizeof($check_lastest)==0)){echo 'กรุณาอย่ากรอกข้ามเดือน'; exit();}
		}
		////////////////////////////////////////////////////////////////////////////////////////

		$check_month_year_confirm = $q_local->query_table("select top 1 revision from ie_summary_lastest where year='".$year."' and month='".$month."' and company_id_user='".$com_user."'");
		if(is_array($check_month_year_confirm)){
			if(sizeof($check_month_year_confirm)==0){$new_revision=0;}
			else{
				$new_revision=($check_month_year_confirm[0]['revision'])+1;
			}
			$i=0;
			while($i<sizeof($_POST['arr_to_save'])){
				$arr_row = $_POST['arr_to_save'][$i];
				$date_start = $arr_row[3]."-".str_pad($arr_row[2],2,"0",STR_PAD_LEFT)."-01";
				$date_last =  $arr_row[3]."-".str_pad($arr_row[2],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($date_start)),2,"0",STR_PAD_LEFT);
				$this_deb_crd=$arr_row[7]; if($this_deb_crd=="debnr"){$table_deb_crd = "ie_debtor";}else{$table_deb_crd = "ie_creditor";}
				$head_com=$arr_row[0];
				$this_com=$arr_row[1];
				$month=$arr_row[2];
				$year=$arr_row[3];
				$createuser=$arr_row[4];
				$gl_id=$arr_row[5];
				$amount=$arr_row[6];
				$head_com_code=$arr_row[11];

				$type_credit_debit=$arr_row[7];
				$open_start_date = str_pad($arr_row[2],2,'0',STR_PAD_LEFT)."/".$arr_row[3];

				if($amount!=""&&$amount!=NULL){
						$query2 = "insert into ie_summary_lastest ([company_id_user]
				      ,[company_id_mapping]
				      ,[month]
				      ,[year]
				      ,[ie_grouping_gl_id]
				      ,[amount]
				      ,[created_date],[type_edit],[revision],[status_last]) OUTPUT Inserted.atid as id values (?,?,?,?,?,?,?,?,?,?)";
						$params2 = array($head_com,$this_com,$month,$year,$gl_id,($amount=="")?NULL:$class_general->remove_comma($amount),$today,"erp",$new_revision,$status_last);
						$arr_return_lastest = $q_local->query_insert($query2,$params2);

						if(is_array($arr_return_lastest)&&sizeof($arr_return_lastest)>0)
						{
							$query = "insert into ie_summary_log ([company_id_user]
					      ,[company_id_mapping]
					      ,[month]
					      ,[year]
					      ,[create_date]
					      ,[createuser]
					      ,[ie_grouping_gl_id]
					      ,[amount],[revision],[type_edit],[id_summary_lastest]) OUTPUT Inserted.atid as id values (?,?,?,?,?,?,?,?,?,?,?)";
							$params = array($head_com,$this_com,$month,$year,$today,$createuser,$gl_id,($amount=="")?NULL:$class_general->remove_comma($amount),$new_revision,"erp",$arr_return_lastest[0]['id']);
							$arr_return_sum = $q_local->query_insert($query,$params);
							if(is_array($arr_return_sum)&&sizeof($arr_return_sum)>0&&($amount!=""&&$amount!=NULL))
							{
								$summery_id=$arr_return_lastest[0]['id'];
								$summery_log_id=$arr_return_sum[0]['id'];
							 if($erp_type[0]['db_type']=='exact'){
								$q_trans_inner = "select g.reknr as glcode,g.bkstnr as entry_no,g.faktuurnr as ourref,g.docnumber as yourref,g.oms25 as des,g.artcode as itemcode
									,i.Description as item_des
									,g.aantal as qty,g.bdr_hfl as Amount
									,c1.debnr,c1.cmp_name as deb_name
									,c2.crdnr,c2.cmp_name as crd_name
									,g.bkstnr_sub as so
									from gbkmut g with(nolock) 
									LEFT OUTER JOIN items i ON i.ItemCode = g.artcode
									LEFT OUTER JOIN cicmpy c1 ON g.debnr = c1.debnr
									LEFT OUTER JOIN cicmpy c2 ON g.crdnr = c2.crdnr

									WHERE
									g.transtype IN ('N', 'C', 'P') 
									 AND g.bud_vers is NULL  
									 AND g.reknr IN (SELECT [gl_code] FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_map] WHERE [company_id] = ".$head_com." AND [ie_grouping_id] = ".$gl_id." ) AND
									 ltrim(rtrim(g.[".$this_deb_crd."])) IN (SELECT ltrim(rtrim(".$this_deb_crd.")) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[".$table_deb_crd."] WHERE [company_id_user] = ".$head_com." AND [company_id_mapping] = ".$this_com.")
									  AND ISNULL(g.transsubtype,'') <> 'X'  AND g.datum >='".$date_start."'   AND g.datum <= '".$date_last."'
									AND g.oorsprong <> 'S'  AND g.remindercount <=13 AND g.bkstnr IS NOT NULL";
								}
									else{
									$q_trans_inner = " select op.gl_no as glcode
										,entry_no
										,ourref
										,yourref
										,des
										,itemcode
										,item_des
										,(op.debit-op.credit) as Amount
										, CASE op.customer  
												 WHEN '0' THEN ''  
												 WHEN '1' THEN op.ref  
												 ELSE ''  
											  END as debnr
										, CASE op.customer  
												 WHEN '0' THEN ''  
												 WHEN '1' THEN op.name  
												 ELSE ''  
											  END as deb_name
										
										
										, CASE op.supplier  
												 WHEN '0' THEN ''  
												 WHEN '1' THEN op.ref  
												 ELSE ''  
											  END as crdnr
										, CASE op.supplier  
												 WHEN '0' THEN ''  
												 WHEN '1' THEN op.name  
												 ELSE ''  
											  END as crd_name
										,'' as so
										
										 from openquery([OP_DB_".$head_com_code."_CLEAN],'select
										aa.code as gl_no,
										
										am.name as entry_no,
										am.ref as ourref,
										'''' as yourref,
										am.description as des,
										aaa.code as itemcode,
										'''' as item_des,
										
										rp.ref ,
										rp.name,
										rp.customer,
										rp.supplier,
										'''' as so,
										
										al.credit,al.debit
										from account_move_line as al
										left outer join account_move  as am on al.move_id=am.id
										left outer join account_account as aa on al.account_id=aa.id
										left outer join account_period as ap on am.period_id=ap.id
										left outer join res_partner as rp on al.partner_id=rp.id
										left outer join account_analytic_account aaa on al.analytic_account_id=aaa.id
										where
					
										ap.name =''".$open_start_date."'' and al.partner_id is not null;') as op
										WHERE
										LTRIM(RTRIM(op.gl_no)) IN (SELECT LTRIM(RTRIM([gl_code])) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[ie_grouping_gl_map] WHERE [company_id] = '".$head_com."' AND [ie_grouping_id] = '".$gl_id."' )
										AND LTRIM(RTRIM(op.ref)) IN (SELECT RTRIM(LTRIM([".$this_deb_crd."])) FROM [192.168.66.22].[SynergyExternal_cloud_2018_new].[dbo].[".$table_deb_crd."] WHERE [company_id_user] = '".$head_com."' AND [company_id_mapping] = '".$this_com."')

										";
									}
								$arr_table_trans=$q_domain->query_table($q_trans_inner);
								
								$j=0;
								while($j<sizeof($arr_table_trans)){
									$arr_insert = $arr_table_trans[$j];
									$query_trans = "insert into ie_zoom ([ie_summary_id],[glcode],[entry_no],[ourref],[yourref],[des],[itemcode],[item_des],[qty],[debnr],[deb_name],[crdnr],[crd_name],[so],[ie_summary_log_id]) OUTPUT Inserted.atid as id values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
									$params_trans = array($summery_id,$arr_insert['glcode'],$arr_insert['entry_no'],$arr_insert['ourref'],$arr_insert['yourref'],$arr_insert['des'],$arr_insert['itemcode'],$arr_insert['item_des'],$class_general->remove_comma($arr_insert['Amount']),$arr_insert['debnr'],$arr_insert['deb_name'],$arr_insert['crdnr'],$arr_insert['crd_name'],$arr_insert['so'],$summery_log_id);
									
									// print_r($params_trans);
									 $arr_return_trans = $q_local->query_insert($query_trans,$params_trans);
									// if(!is_array($arr_return_trans)){echo $arr_return_trans."\n";}
									$j++;
								} // end while J++	
							} // end arr return_sum
						}
					}
					$i++;
		}

		$clear_laste_sum = "delete from ie_summary_lastest where created_date<'".$today."' and year='".$year."' and month='".$month."' and company_id_user='".$com_user."'";
		$q_local->query_table($clear_laste_sum);
	}

}else{ ////////////// editable
		$i=0;
		while($i<sizeof($_POST['arr_to_save'])){
			$arr_row = $_POST['arr_to_save'][$i];
			//print_r($arr_row);
			$date_start = $arr_row[3]."-".str_pad($arr_row[2],2,"0",STR_PAD_LEFT)."-01";
			$date_last =  $arr_row[3]."-".str_pad($arr_row[2],2,"0",STR_PAD_LEFT)."-".str_pad(date("t",strtotime($date_start)),2,"0",STR_PAD_LEFT);
			$this_deb_crd=$arr_row[7]; if($this_deb_crd=="debnr"){$table_deb_crd = "ie_debtor";}else{$table_deb_crd = "ie_creditor";}
			$head_com=$arr_row[0];
			$this_com=$arr_row[1];
			$month=$arr_row[2];
			$year=$arr_row[3];
			$createuser=$arr_row[4];
			$gl_id=$arr_row[5];
			$amount=$arr_row[6];
			$type_credit_debit=$arr_row[7];
			$type_input=$arr_row[8];
			$old_id=$arr_row[9];
			$revision=$arr_row[10];
			
			if(trim($old_id)!=""&&$old_id!=NULL){ // update
				$query_up = "update ie_summary_lastest set [amount]=?,[type_edit]=? OUTPUT Inserted.atid as id where atid='".$old_id."'";
				$params_up = array(($amount=="")?NULL:$class_general->remove_comma($amount),$type_input);
				$arr_update_sum = $q_local->query_insert($query_up,$params_up);

				$query_insert_log = "insert into ie_editable_log ([ie_editable_user_id],[ie_editable_datetime],[ie_editable_amount],[ie_editable_sum_id]) OUTPUT Inserted.ie_editable_id as id values (?,?,?,?)";
				$params_insert_log = array($createuser,$today,($amount=="")?NULL:$class_general->remove_comma($amount),$old_id);
				$array_re=$q_local->query_insert($query_insert_log,$params_insert_log);
			}
			else
			{ // insert into sum and sum lastest
				if($amount!=""&&$amount!=NULL){
					$query2 = "insert into ie_summary_lastest ([company_id_user]
				      ,[company_id_mapping]
				      ,[month]
				      ,[year]
				      ,[ie_grouping_gl_id]
				      ,[amount]
				      ,[created_date],[type_edit],[revision]) OUTPUT Inserted.atid as id values (?,?,?,?,?,?,?,?,?)";
					$params2 = array($head_com,$this_com,$month,$year,$gl_id,($amount=="")?NULL:$class_general->remove_comma($amount),$today,"manual",$revision);
					$arr_return_lastest = $q_local->query_insert($query2,$params2);

					if(is_array($arr_return_lastest)&&sizeof($arr_return_lastest)>0){
						$query = "insert into ie_summary_log ([company_id_user]
			      ,[company_id_mapping]
			      ,[month]
			      ,[year]
			      ,[create_date]
			      ,[createuser]
			      ,[ie_grouping_gl_id]
			      ,[amount],[revision],[type_edit]) OUTPUT Inserted.atid as id values (?,?,?,?,?,?,?,?,?,?)";
						$params = array($head_com,$this_com,$month,$year,$today,$createuser,$gl_id,($amount=="")?NULL:$class_general->remove_comma($amount),$revision,"manual");
						$arr_return_sum = $q_local->query_insert($query,$params);	
					}	
				}
			}
			$i++;
		}
		//////////////////// update_status_last 
		$q_update_status_last = "update ie_summary_lastest set [status_last]=?  where year='".$year."' and month='".$month."' and company_id_user='".$com_user."'";
		$params_update_status_last = array($status_last);
		$arr_return_status_last = $q_local->query_insert($q_update_status_last,$params_update_status_last);	
		///////////////////
}
?>